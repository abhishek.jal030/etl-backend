package com.datasync.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class JobRequest {

	private String jobId;
	private String JobName;
	private String sourceConnection;
	private String destinationConnection;
	private String sourceConnectionName;
	private String destinationConnectionName;
	private String sourceCollection;
	private String sourceCollectionType;
	private String destinationCollection;
	private String destinationConnectionUrl;
	private String mapping;
	private String jobFilter;
	private boolean isStopped;
	private String status;
	private String lastRun;
	private String incrementalParameter;
	private String incrementalParameterValue;
	private String createdBy;
	private String updatedBy;

}
