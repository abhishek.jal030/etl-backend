package com.datasync.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BulkJobRequest {

	private String id;
	private String name;
	private String sourceConnection;
	private String destinationConnection;
	private String sourceConnectionName;
	private String destinationConnectionName;
	private String CollectionNames;
	private String prefix;
	private String scheduleDateTime;
	private String createdBy;
	private String updatedBy;
	
	

}
