package com.datasync.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ConnectionTestRequest {

	private String connectionType;
	private String url;
	private String tokenUrl;
	private String testUrl;
	private String userName;
	private String password;
	private String host;
	private String port;
	private String serviceName;
	private String schemaName;

}
