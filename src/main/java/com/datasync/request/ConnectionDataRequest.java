package com.datasync.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ConnectionDataRequest {

	List<ConnectionConfigRequest> connectionConfigRequestList;
	private String connectionId;
	private String type;
	private String name;
	private String aliasName;
	private String createdDate;
	private String updatedDate;
	private String createdBy;
	private String updatedBy;

}
