package com.datasync.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datasync.manager.ConnectorManager;
import com.datasync.request.ConnectionTestRequest;
import com.datasync.request.ConnectorTypeRequest;
import com.datasync.request.PreviewFilterRequest;
import com.datasync.request.TableMetaDataRequest;
import com.datasync.request.TestFilterRequest;
import com.datasync.response.responsemanager.ApiResponse;

@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class ConnectorController {

	@Autowired
	ConnectorManager connectorManager;

	@GetMapping(path = "/collections/{connectionId}/{collectionName}/metadata", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getMetadata(@PathVariable("connectionId") String connectionId,
			@PathVariable("collectionName") String collectionName,
			@RequestParam(value = "type", required = false) String name) {
		return connectorManager.getMetadata(connectionId, collectionName, name);
	}

	@PostMapping(path = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> testConnection(@RequestBody ConnectionTestRequest connectionTestRequest) {
		return connectorManager.testConnection(connectionTestRequest);
	}

	@GetMapping(path = "collections/{connectionId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getAllEntitiesName(@PathVariable("connectionId") String connectionId) {
		return connectorManager.getAllEntitiesName(connectionId);
	}

	@PostMapping(path = "collections/{connectionId}/{collectionName}/data", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getDataForPreview(@PathVariable("connectionId") String connectionId,
			@PathVariable("collectionName") String collectionName,
			@RequestBody PreviewFilterRequest previewFilterRequest,
			@RequestParam(value = "collectionType", required = false) String collectionType,
			@RequestParam(value = "startRecord", required = false) Integer startRecord,
			@RequestParam(value = "recordCount", required = false) Integer recordCount) {
		if (startRecord == null) {
			startRecord = 0;
		}
		if (recordCount == null) {
			recordCount = 10;
		}
		return connectorManager.getDataForPreview(connectionId, collectionName,collectionType, previewFilterRequest.getPreviewFilter(),
				startRecord, recordCount);
	}

	@GetMapping(path = "collections/odata/{connectionId}/refreshMetadata", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> refreshMetadata(@PathVariable("connectionId") String connectionId) {
		return connectorManager.refreshMetadata(connectionId);
	}

	@GetMapping(path = "collections/odata/{connectionId}/refreshCollectionCounts", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> refreshCollectionCounts(@PathVariable("connectionId") String connectionId) {
		return connectorManager.refreshCollectionCounts(connectionId);
	}

	@GetMapping(path = "collections/odata/{connectionId}/tableMetadata", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getTableMetadata(@PathVariable("connectionId") String connectionId,
			@RequestParam(value = "collectionName") String collectionName) {
		return connectorManager.getTableMetadata(connectionId, collectionName);
	}

	@GetMapping(path = "collections/odata/{jobId}/updatedTableMetadata", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getUpdatedTableMetadata(@PathVariable("jobId") String jobId) {
		return connectorManager.getUpdatedTableMetadata(jobId);
	}

	@PostMapping(path = "collections/odata/{connectionId}/tableMetadata", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> createTable(@PathVariable("connectionId") String connectionId,
			@RequestBody TableMetaDataRequest tableMetaDataRequest) {
		return connectorManager.createTable(connectionId, tableMetaDataRequest);
	}

	@PostMapping(path = "collections/odata/{connectionId}/alterTableMetadata", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> alterTable(@PathVariable("connectionId") String connectionId,
			@RequestBody TableMetaDataRequest tableMetaDataRequest) {
		return connectorManager.alterTable(connectionId, tableMetaDataRequest);
	}

	@GetMapping(path = "/connectorType", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getAllConnectorType() {
		return connectorManager.getAllConnectorType();
	}

	@PostMapping(path = "/connectorType", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> createConnectorType(@RequestBody ConnectorTypeRequest connectorTypeRequest) {
		return connectorManager.createConnectorType(connectorTypeRequest);
	}

	/*
	 * @PostMapping(path = "collections/odata/{connectionId}/testFilterQuery",
	 * produces = MediaType.APPLICATION_JSON_VALUE) public
	 * ResponseEntity<ApiResponse> testFilterQuery(@PathVariable("connectionId")
	 * String connectionId,
	 * 
	 * @RequestParam(value = "collectionName") String collectionName,
	 * 
	 * @RequestBody TestFilterRequest testFilterRequest) {
	 * testFilterRequest.setCollectionName(collectionName);
	 * testFilterRequest.setConnectionId(connectionId); return
	 * connectorManager.testFilterQuery(testFilterRequest); }
	 */
}
