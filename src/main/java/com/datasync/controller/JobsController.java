package com.datasync.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.datasync.manager.JobsManager;
import com.datasync.request.BulkJobRequest;
import com.datasync.request.JobRequest;
import com.datasync.request.ScheduledJobRequest;
import com.datasync.request.TempJobRequest;
import com.datasync.response.responsemanager.ApiResponse;

@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class JobsController {

	@Autowired
	JobsManager jobsManager;

	@GetMapping(path = "/jobs/{jobId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getJob(@PathVariable("jobId") String jobId) {
		return jobsManager.getJob(jobId);
	}

	@PostMapping(path = "/job", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getAllJobs(@RequestBody JobRequest jobRequest,
			@RequestParam(value = "sortableColumn", required = false) String sortableColumn,
			@RequestParam(value = "sortingType", required = false) String sortingType,
			@RequestParam(value = "startRecord", required = false) Integer startRecord,
			@RequestParam(value = "recordCount", required = false) Integer recordCount) {
		if (startRecord == null) {
			startRecord = 0;
		}
		if (recordCount == null) {
			recordCount = 10;
		}
		return jobsManager.getAllJobs(jobRequest, sortableColumn, sortingType, startRecord, recordCount);
	}

	@PostMapping(path = "/jobs", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> createJobs(@RequestBody JobRequest jobRequest) {
		return jobsManager.saveOrUpdateJob(jobRequest);
	}

	@PutMapping(path = "/jobs/{jobId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> updateJob(@PathVariable("jobId") String jobId,
			@RequestBody JobRequest jobRequest) {
		jobRequest.setJobId(jobId);
		return jobsManager.saveOrUpdateJob(jobRequest);
	}

	@DeleteMapping(path = "/jobs", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> deleteJobs(@RequestBody String[] jobIds) {
		List<String> jobIdList = new ArrayList<String>();
		if (jobIds != null && jobIds.length != 0) {
			jobIdList = Arrays.asList(jobIds);
		}
		return jobsManager.deleteJobs(jobIdList);
	}

	@GetMapping(path = "/jobs/{jobId}/run", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> runJob(@PathVariable("jobId") String jobId) {
		return jobsManager.runJob(jobId);
	}

	@GetMapping(path = "/jobs/{jobId}/run/export", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<InputStreamResource> runJobExport(@PathVariable("jobId") String jobId) {
		return jobsManager.runJobExport(jobId);
	}

	@PostMapping(path = "/jobs/{jobId}/run/upload", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> runJobUpload(@PathVariable("jobId") String jobId,
			@RequestParam(value = "file", required = true) MultipartFile file) {
		return jobsManager.runJobUpload(jobId, file);
	}

	@GetMapping(path = "/jobs/{jobId}/progress", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> jobProgress(@PathVariable("jobId") String jobId) {
		return jobsManager.jobProgress(jobId);
	}

	@GetMapping(path = "/jobs/InProgress", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> jobInProgress() {
		return jobsManager.jobInProgress();
	}

	@GetMapping(path = "/jobs/{connectionId}/details", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> connectionJobDetails(@PathVariable("connectionId") String connectionId) {
		return jobsManager.connectionJobDetails(connectionId);
	}

	@PostMapping(path = "/jobs/{jobId}/schedule", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> updateScheduledJobs(@PathVariable("jobId") String jobId,
			@RequestBody ScheduledJobRequest scheduledJobRequest) {
		scheduledJobRequest.setJobId(jobId);
		return jobsManager.saveOrUpdatescheduledJob(scheduledJobRequest);
	}

	@DeleteMapping(path = "/jobs/{jobId}/schedule", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> deleteScheduledJobs(@PathVariable("jobId") String jobId) {

		return jobsManager.deleteScheduledJobs(jobId);
	}

	@GetMapping(path = "/jobs/{jobId}/logs", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> viewLogs(@PathVariable("jobId") String jobId) {
		return jobsManager.viewLogs(jobId);
	}

	@GetMapping(path = "/jobs/{jobId}/logs/{jobStatusId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> viewLogDetails(@PathVariable("jobId") String jobId,
			@PathVariable("jobStatusId") String jobStatusId) {
		return jobsManager.viewLogDetails(jobId, jobStatusId);
	}

	@PostMapping(path = "/jobs/difference", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> viewJobsDifference(@RequestBody JobRequest jobRequest) {
		return jobsManager.viewJobsDifference(jobRequest);
	}

	@GetMapping(path = "/jobs/{jobId}/stop", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> stopJob(@PathVariable("jobId") String jobId) {
		return jobsManager.stopJob(jobId);
	}

	@PostMapping(path = "/jobs/{jobId}/difference", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> saveJobsDifference(@PathVariable("jobId") String jobId,
			@RequestBody TempJobRequest tempJobRequest) {
		tempJobRequest.setJobId(jobId);
		return jobsManager.saveJobsDifference(tempJobRequest);
	}

	@PostMapping(path = "/jobs/bulkJobs", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> createBulkJob(@RequestBody BulkJobRequest bulkJobRequest) {
		return jobsManager.saveOrUpdateBulkJob(bulkJobRequest);
	}
}
