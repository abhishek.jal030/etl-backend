package com.datasync.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

import com.datasync.manager.ConnectionManager;
import com.datasync.request.ConnectionDataRequest;
import com.datasync.response.responsemanager.ApiResponse;

@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class ConnectionController {

	@Autowired
	ConnectionManager connectionManager;

	@PostMapping(path = "/connection", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getAllConnections(@RequestBody ConnectionDataRequest connectionDataRequest,
			@RequestParam(value = "sortableColumn", required = false) String sortableColumn,
			@RequestParam(value = "sortingType", required = false) String sortingType,
			@RequestParam(value = "startRecord", required = false) Integer startRecord,
			@RequestParam(value = "recordCount", required = false) Integer recordCount) {
		if (startRecord == null) {
			startRecord = 0;
		}
		if (recordCount == null) {
			recordCount = 100;
		}
		return connectionManager.getAllConnections(connectionDataRequest, startRecord, recordCount, sortableColumn,
				sortingType);
	}

	@GetMapping(path = "/connections/{connectionId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getConnection(@PathVariable("connectionId") String connectionId) {
		return connectionManager.getConnection(connectionId);
	}

	@PostMapping(path = "/connections", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> createConnection(@RequestBody ConnectionDataRequest connectionDataRequest) {
		return connectionManager.saveOrUpdateConnection(connectionDataRequest);
	}

	@PutMapping(path = "/connections/{connectionId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> saveConnection(@PathVariable("connectionId") String connectionId,
			@RequestBody ConnectionDataRequest connectionDataRequest) {
		connectionDataRequest.setConnectionId(connectionId);
		return connectionManager.saveOrUpdateConnection(connectionDataRequest);
	}

	@DeleteMapping(path = "/connections", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> deleteConnections(@RequestBody String[] connectionIds) {
		List<String> connectionIdList = new ArrayList<String>();
		if (connectionIds != null && connectionIds.length != 0) {
			connectionIdList = Arrays.asList(connectionIds);
		}
		return connectionManager.deleteConnections(connectionIdList);
	}
}
