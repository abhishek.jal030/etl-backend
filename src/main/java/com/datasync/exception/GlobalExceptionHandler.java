package com.datasync.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.datasync.response.responsemanager.ApiError;
import com.datasync.response.responsemanager.ApiResponse;
import com.datasync.util.ResponseUtil;

public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		BindingResult bindingResult = ex.getBindingResult();
		List<ApiError> apiFieldErrors = null;
		if (bindingResult.hasErrors()) {
			apiFieldErrors = bindingResult
					.getFieldErrors().stream().map(fieldError -> new ApiError(fieldError.getCode(),
							fieldError.getDefaultMessage(), fieldError.getField(), fieldError.getRejectedValue()))
					.collect(Collectors.toList());
		}
		ApiResponse apiResponse = ResponseUtil
				.buildErrorResponse(apiFieldErrors.toArray(new ApiError[apiFieldErrors.size()]));
		return ResponseEntity.ok(apiResponse);
	}

}
