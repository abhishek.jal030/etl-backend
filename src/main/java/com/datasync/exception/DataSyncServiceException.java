package com.datasync.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal server error")
public class DataSyncServiceException extends RuntimeException {

	private static final long serialVersionUID = 3227742142529729435L;

	public DataSyncServiceException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public DataSyncServiceException(String msg) {
		super(msg);
	}
}
