package com.datasync.exception;

public class ResourceNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2058087522406388051L;

	public ResourceNotFoundException(String msg) {
		super(msg);
	}

	public ResourceNotFoundException() {
		super();
	}

	public ResourceNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}
}