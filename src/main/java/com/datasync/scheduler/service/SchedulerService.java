package com.datasync.scheduler.service;

/**
 * @author Shubham
 *
 */
public interface SchedulerService {
	
	public void refreshCollectionsCount();
	
	public void scheduleJobs();
}