package com.datasync.scheduler.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.datasync.entity.ConnectionEntity;
import com.datasync.entity.MetaDataEntity;
import com.datasync.repository.ConnectionEntityRepository;
import com.datasync.repository.MetaDataEntityRepository;
import com.datasync.scheduler.service.SchedulerService;
import com.datasync.serviceEngine.JobsService;
import com.datasync.serviceEngine.odataConnector.MetadataService;

/**
 * @author Shubham
 *
 */

@Service
public class SchedulerServiceImpl implements SchedulerService {
	private static Logger logger = LogManager.getLogger(SchedulerServiceImpl.class);

	@Autowired
	MetaDataEntityRepository metaDataEntityRepository;

	@Autowired
	ConnectionEntityRepository connectionEntityRepository;

	@Autowired
	MetadataService metadataService;

	@Autowired
	JobsService jobsService;

	// All Scheduler Expression is Defining Below :: <second> <minute> <hour>
	// <day-of-month> <month> <day-of-week>
	@Scheduled(cron = "0 0 0/4 * * ?")
	public void refreshCollectionsCount() {
		logger.debug("Entering to com.netapp.scheduler.service.impl.SchedulerServiceImpl.processRequestOnStatus()");
		try {
			List<MetaDataEntity> metaDataEntityList = metaDataEntityRepository.findAll();
			if (metaDataEntityList != null && metaDataEntityList.size() != 0) {
				for (int i = 0; i < metaDataEntityList.size(); i++) {
					ConnectionEntity connectionEntity = new ConnectionEntity();
					Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository
							.findById(metaDataEntityList.get(i).getConnectionId());
					if (connectionEntityOptional.isPresent()) {
						connectionEntity = connectionEntityOptional.get();
					}
					metadataService.refreshCollectionCounts(connectionEntity);
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
	}

	//@Scheduled(fixedRate = 1000)
	public void scheduleJobs() {
		jobsService.scheduledJobs();
		// TODO Auto-generated method stub
	}
}
