package com.datasync.serviceEngine;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.datasync.exception.DataSyncServiceException;
import com.datasync.request.BulkJobRequest;
import com.datasync.request.JobRequest;
import com.datasync.request.ScheduledJobRequest;
import com.datasync.request.TempJobRequest;
import com.datasync.response.BulkJobResponse;
import com.datasync.response.JobDetailsResponse;
import com.datasync.response.JobProgressResponse;
import com.datasync.response.JobResponse;
import com.datasync.response.TempJobResponse;
import com.datasync.response.ViewJobDifferenceResponse;
import com.datasync.response.ViewLogDetailsResponse;
import com.datasync.response.ViewLogResponse;

public interface JobsService {

	public JobResponse getJob(String jobId);

	public JobDetailsResponse getAllJobs(JobRequest jobRequest, String sortableColumn, String sortingType,
			Integer startRecord, Integer recordCount);

	public JobResponse saveOrUpdateJob(JobRequest jobRequest);

	public boolean deleteJobs(List<String> jobIds);

	public boolean runJob(String jobId);

	public boolean runJobUpload(String jobId, MultipartFile file);

	public Map<String, Object> runJobExport(String jobId);

	public JobProgressResponse jobProgress(String jobId);

	public JobProgressResponse[] jobInProgress();

	public JobDetailsResponse connectionJobDetails(String connectionId);

	public boolean saveOrUpdatescheduledJob(ScheduledJobRequest scheduledJobRequest);

	public boolean scheduledJobs();

	public boolean deleteScheduledJobs(String jobId);

	public ViewLogResponse[] viewLogs(String jobId);

	public ViewLogDetailsResponse[] viewLogDetails(String jobId, String jobStatusId);

	public ViewJobDifferenceResponse[] viewJobsDifference(JobRequest jobRequest);

	public JobResponse stopJob(String jobId);

	public TempJobResponse saveJobsDifference(TempJobRequest tempJobRequest);

	public BulkJobResponse saveOrUpdateBulkJob(BulkJobRequest bulkJobRequest) throws DataSyncServiceException;

}
