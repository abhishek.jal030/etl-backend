package com.datasync.serviceEngine.rdbmsConnectors.OracleConnector;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.datasync.request.ConnectionTestRequest;
import com.datasync.response.ConnectionTestResponse;
import com.datasync.response.MetadataResponse;
import com.datasync.serviceEngine.rdbmsConnectors.RDBMSConfiguration;
import com.datasync.util.ConnectorUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class MySqlService extends RDBMSConfiguration {

	private static Logger logger = LogManager.getLogger(MySqlService.class);

	public ConnectionTestResponse testConnection(ConnectionTestRequest connectionTestRequest) {
		logger.debug("Entering to com.datasync.serviceEngine.rdbmsConnectors.OracleConnector.testConnection()");

		ConnectionTestResponse connectionTestResponse = null;
		try {
			Connection conn = getConnection(connectionTestRequest);
			connectionTestResponse = ConnectorUtil.buildTestConnectorResponse(conn);
		} catch (Exception e) {
			connectionTestResponse = new ConnectionTestResponse();
			connectionTestResponse.setMessage(e.getMessage());
			connectionTestResponse.setStatus(false);
		}
		logger.debug("Exiting to com.datasync.serviceEngine.rdbmsConnectors.OracleConnector.testConnection()");
		return connectionTestResponse;
	}

	
}
