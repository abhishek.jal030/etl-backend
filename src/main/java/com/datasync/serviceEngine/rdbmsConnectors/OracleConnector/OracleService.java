package com.datasync.serviceEngine.rdbmsConnectors.OracleConnector;

import java.sql.Connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.datasync.entity.ConnectionEntity;
import com.datasync.request.ConnectionTestRequest;
import com.datasync.request.TableMetaDataRequest;
import com.datasync.response.ConnectionTestResponse;
import com.datasync.serviceEngine.rdbmsConnectors.RDBMSConfiguration;
import com.datasync.util.ConnectorUtil;
import com.datasync.util.DataSyncUtilityMessage;

@Service
public class OracleService extends RDBMSConfiguration {

	private static Logger logger = LogManager.getLogger(RDBMSConfiguration.class);

	public ConnectionTestResponse testConnection(ConnectionTestRequest connectionTestRequest) {
		logger.debug("Entering to com.datasync.serviceEngine.rdbmsConnectors.OracleConnector.testConnection()");

		ConnectionTestResponse connectionTestResponse = null;
		try {
			Connection conn = getConnection(connectionTestRequest);
			connectionTestResponse = ConnectorUtil.buildTestConnectorResponse(conn);
		} catch (Exception e) {
			connectionTestResponse = new ConnectionTestResponse();
			connectionTestResponse.setMessage(e.getMessage());
			connectionTestResponse.setStatus(false);
		}
		logger.debug("Exiting to com.datasync.serviceEngine.rdbmsConnectors.OracleConnector.testConnection()");
		return connectionTestResponse;
	}

	public boolean createTable(ConnectionEntity connectionEntity, TableMetaDataRequest tableMetaDataRequest)
			throws Exception {
		String CREATE_TABLE_STAT = "CREATE TABLE " + tableMetaDataRequest.getTableName() + "( ";

		for (int i = 0; i < tableMetaDataRequest.getTableMetadataList().size(); i++) {
			String columnname = tableMetaDataRequest.getTableMetadataList().get(i).getName();
			String type = tableMetaDataRequest.getTableMetadataList().get(i).getType();
			CREATE_TABLE_STAT = CREATE_TABLE_STAT + columnname + " " + type + ", ";

		}
		CREATE_TABLE_STAT = CREATE_TABLE_STAT.substring(0, CREATE_TABLE_STAT.length() - 2);
		CREATE_TABLE_STAT = CREATE_TABLE_STAT + " )";
		System.out.print("CREATE_TABLE_STAT.." + CREATE_TABLE_STAT);
		ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
		connectionTestRequest
				.setServiceName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.serviceName));
		connectionTestRequest.setPort(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
		connectionTestRequest.setHost(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
		connectionTestRequest.setUserName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
		connectionTestRequest.setPassword(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
		connectionTestRequest.setConnectionType(connectionEntity.getType());
		return createTable(connectionTestRequest, CREATE_TABLE_STAT);

	}

	public boolean alterTable(ConnectionEntity connectionEntity, TableMetaDataRequest tableMetaDataRequest)
			throws Exception {
		String ALTER_TABLE_STAT = "ALTER TABLE " + tableMetaDataRequest.getTableName() + " ADD ( ";

		for (int i = 0; i < tableMetaDataRequest.getTableMetadataList().size(); i++) {
			String columnname = tableMetaDataRequest.getTableMetadataList().get(i).getName();
			String type = tableMetaDataRequest.getTableMetadataList().get(i).getType();
			ALTER_TABLE_STAT = ALTER_TABLE_STAT + columnname + " " + type + ", ";

		}
		ALTER_TABLE_STAT = ALTER_TABLE_STAT.substring(0, ALTER_TABLE_STAT.length() - 2);
		ALTER_TABLE_STAT = ALTER_TABLE_STAT + " )";
		System.out.print("ALTER_TABLE_STAT.." + ALTER_TABLE_STAT);
		ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
		connectionTestRequest
				.setServiceName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.serviceName));
		connectionTestRequest.setPort(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
		connectionTestRequest.setHost(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
		connectionTestRequest.setUserName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
		connectionTestRequest.setPassword(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
		return createTable(connectionTestRequest, ALTER_TABLE_STAT);

	}

	public static String getOracleDataType(String edmType, int length) {
		if (length <= 0) {
			length = 4000;
		}
		switch (edmType) {
		case "Int64":
			return "NUMBER(19,0)";
		case "Binary":
			return "BFILE";
		case "Boolean":
			return "CHAR(10)";
		case "Decimal":
			return "FLOAT";
		case "Double":
			return "BINARY_DOUBLE";
		case "Single":
			return "BINARY_FLOAT";
		case "Int32":
			return "NUMBER(6,0)";
		case "DateTime":
			return "TIMESTAMP";
		case "DateTimeOffset":
			return "TIMESTAMP WITH TIME ZONE";
		case "String":
			return "VARCHAR2(" + length + ")";
		case "Guid":
			return "VARCHAR2(48)";
		default:
			return "VARCHAR2(" + length + ")";
		}

	}
}
