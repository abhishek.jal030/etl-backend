package com.datasync.serviceEngine.rdbmsConnectors;

import java.io.FileReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.PreparedStatement;

import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datasync.entity.ConnectionEntity;
import com.datasync.request.ConnectionTestRequest;
import com.datasync.request.MappingRequest;
import com.datasync.response.CollectionMetadataResponse;
import com.datasync.response.DataPreviewResponse;
import com.datasync.response.InResponse;
import com.datasync.response.MetadataResponse;
import com.datasync.response.MetadataResponseDetails;
import com.datasync.serviceEngine.ConnectorFactory;
import com.datasync.serviceEngine.apiConnector.ApiConnectorService;
import com.datasync.serviceEngine.odataConnector.MetadataService;
import com.datasync.util.DataSyncUtilityMessage;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class RDBMSConfiguration {

	@Autowired
	ApiConnectorService apiConnectorService;

	private static Logger logger = LogManager.getLogger(RDBMSConfiguration.class);

	Connection conn = null;

	public Connection getConnection(ConnectionTestRequest connectionTestRequest) throws Exception {

		logger.debug("Entering to com.datasync.serviceEngine.rdbmsConnectors.getConnection()");
		if (connectionTestRequest.getConnectionType().toUpperCase().equals(DataSyncUtilityMessage.oracle)) {
			// String JDBC_DRIVER = "oracle.jdbc.OracleDriver";
			String DB_URL = "jdbc:oracle:thin:@//" + connectionTestRequest.getHost() + ":"
					+ connectionTestRequest.getPort() + "/" + connectionTestRequest.getServiceName();
			String USER = connectionTestRequest.getUserName();
			String PASS = connectionTestRequest.getPassword();
			System.out.print("DB_URL..." + DB_URL);
			try {
				Class.forName(DataSyncUtilityMessage.ORACLE_DRIVER);
				conn = DriverManager.getConnection(DB_URL, USER, PASS);
				System.out.println("Database created successfully...");
				logger.debug("Exiting to com.datasync.serviceEngine.rdbmsConnectors.getConnection()");
				return conn;
			} catch (SQLException se) {
				throw se;
				// se.printStackTrace();
			} catch (Exception e) {
				throw e;
				// e.printStackTrace();
			}

		}
		if (connectionTestRequest.getConnectionType().toUpperCase().equals(DataSyncUtilityMessage.mysql)) {
			// String JDBC_DRIVER = "oracle.jdbc.OracleDriver";
			String DB_URL = "jdbc:mysql://" + connectionTestRequest.getHost() + ":" + connectionTestRequest.getPort()
					+ "/" + connectionTestRequest.getSchemaName() + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			String USER = connectionTestRequest.getUserName();
			String PASS = connectionTestRequest.getPassword();
			System.out.print("DB_URL..." + DB_URL);
			try {
				Class.forName(DataSyncUtilityMessage.MYSQL_DRIVER);
				conn = DriverManager.getConnection(DB_URL, USER, PASS);
				System.out.println("Database created successfully...");
				logger.debug("Exiting to com.datasync.serviceEngine.rdbmsConnectors.getConnection()");
				return conn;
			} catch (SQLException se) {
				throw se;
				// se.printStackTrace();
			} catch (Exception e) {
				throw e;
				// e.printStackTrace();
			}

		}
		return conn;

	}

	public MetadataResponseDetails getEntitiesName(ConnectionEntity connectionEntity) throws Exception {
		if (connectionEntity.getType().equals(DataSyncUtilityMessage.oracle)) {
			logger.debug("Entering to com.datasync.serviceEngine.rdbmsConnectors.getEntitiesName()");
			ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
			MetadataResponseDetails metadataResponseDetails = new MetadataResponseDetails();
			List<MetadataResponse> entityNameList = new ArrayList<MetadataResponse>();
			connectionTestRequest.setConnectionType(connectionEntity.getType());
			connectionTestRequest
					.setServiceName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.serviceName));
			connectionTestRequest.setPort(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
			connectionTestRequest.setHost(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
			connectionTestRequest
					.setUserName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
			connectionTestRequest
					.setPassword(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
			Connection conn = getConnection(connectionTestRequest);
			Statement stmt = conn.createStatement();
			String query = "SELECT table_name FROM user_tables";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				MetadataResponse metadataResponse = new MetadataResponse();
				metadataResponse.setCollectionName(rs.getString("TABLE_NAME"));
				entityNameList.add(metadataResponse);
			}
			conn.close();
			metadataResponseDetails.setMetadataTableResponse(entityNameList);
			;
			logger.debug("Exiting to com.datasync.serviceEngine.rdbmsConnectors.getEntitiesName()");
			return metadataResponseDetails;
		}
		if (connectionEntity.getType().equals(DataSyncUtilityMessage.mysql)) {
			logger.debug("Entering to com.datasync.serviceEngine.rdbmsConnectors.getEntitiesName()");
			ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
			MetadataResponseDetails metadataResponseDetails = new MetadataResponseDetails();
			List<MetadataResponse> entityNameList = new ArrayList<MetadataResponse>();
			List<MetadataResponse> procedureNameList = new ArrayList<MetadataResponse>();
			connectionTestRequest.setConnectionType(connectionEntity.getType());
			connectionTestRequest
					.setSchemaName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.schemaName));
			connectionTestRequest.setPort(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
			connectionTestRequest.setHost(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
			connectionTestRequest
					.setUserName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
			connectionTestRequest
					.setPassword(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
			Connection conn = getConnection(connectionTestRequest);

			Statement stmt = conn.createStatement();
			String queryForTable = "SHOW TABLES";
			String queryForProcedure = "SHOW PROCEDURE STATUS WHERE Db = '"
					+ connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.schemaName) + "'";
			ResultSet rsForTable = stmt.executeQuery(queryForTable);
			while (rsForTable.next()) {
				System.out.println(rsForTable.getString(1));
				MetadataResponse metadataResponse = new MetadataResponse();
				metadataResponse.setCollectionName(rsForTable.getString(1));
				entityNameList.add(metadataResponse);

			}
			ResultSet rsForProcedure = stmt.executeQuery(queryForProcedure);
			while (rsForProcedure.next()) {
				System.out.println(rsForProcedure.getString(2));
				MetadataResponse metadataResponse = new MetadataResponse();
				metadataResponse.setCollectionName(rsForProcedure.getString(2));
				procedureNameList.add(metadataResponse);
			}
			conn.close();
			metadataResponseDetails.setMetadataTableResponse(entityNameList);
			metadataResponseDetails.setMetadataProcedureResponse(procedureNameList);
			logger.debug("Exiting to com.datasync.serviceEngine.rdbmsConnectors.getEntitiesName()");
			return metadataResponseDetails;
		}
		return null;

	}

	/**
	 * @param collectionName
	 * @param connectionEntity
	 * @param proTabName
	 * @return
	 * @throws Exception
	 */
	public MetadataResponse getMetaData(String collectionName, ConnectionEntity connectionEntity, String proTabName)
			throws Exception {
		logger.debug("Entering to com.datasync.serviceEngine.rdbmsConnectors.getMetaData()");
		ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
		if (connectionEntity.getType().equals(DataSyncUtilityMessage.oracle)) {
			connectionTestRequest.setConnectionType(connectionEntity.getType());
			connectionTestRequest
					.setServiceName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.serviceName));
			connectionTestRequest.setPort(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
			connectionTestRequest.setHost(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
			connectionTestRequest
					.setUserName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
			connectionTestRequest
					.setPassword(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
			Connection conn = getConnection(connectionTestRequest);
			Statement stmt = conn.createStatement();
			String query = "SELECT COLUMN_NAME,DATA_TYPE,DATA_LENGTH " + "FROM user_tab_columns "
					+ "WHERE upper(table_name) = '" + collectionName.toUpperCase() + "' ORDER BY column_id";
			System.out.println(query);
			ResultSet rs = stmt.executeQuery(query);
			MetadataResponse metadataResponse = new MetadataResponse();
			List<InResponse> inResponseList = new ArrayList<InResponse>();
			metadataResponse.setCollectionName(collectionName.toUpperCase());
			List<CollectionMetadataResponse> collectionMetadataResponseList = new ArrayList<CollectionMetadataResponse>();
			while (rs.next()) {
				CollectionMetadataResponse collectionMetadataResponse = new CollectionMetadataResponse();
				collectionMetadataResponse.setName(rs.getString("COLUMN_NAME"));
				collectionMetadataResponse.setType(rs.getString("DATA_TYPE"));
				collectionMetadataResponse.setMaxLength(rs.getString("DATA_LENGTH"));
				collectionMetadataResponseList.add(collectionMetadataResponse);
			}
			conn.close();
			metadataResponse.setCollectionMetadataResponseList(collectionMetadataResponseList);
			metadataResponse.setInResponseList(inResponseList);
			logger.debug("Exiting to com.datasync.serviceEngine.rdbmsConnectors.getMetaData()");
			return metadataResponse;
		}
		if (connectionEntity.getType().equals(DataSyncUtilityMessage.mysql)) {
			connectionTestRequest.setConnectionType(connectionEntity.getType());
			connectionTestRequest
					.setSchemaName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.schemaName));
			connectionTestRequest.setPort(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
			connectionTestRequest.setHost(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
			connectionTestRequest
					.setUserName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
			connectionTestRequest
					.setPassword(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
			MetadataResponse metadataResponse = new MetadataResponse();
			List<CollectionMetadataResponse> collectionMetadataResponseList = new ArrayList<CollectionMetadataResponse>();
			List<InResponse> inResponseList = new ArrayList<InResponse>();
			if (proTabName.equals("procedure") || proTabName == "procedure") {
				collectionMetadataResponseList = apiConnectorService.getMetaData(collectionName);
				inResponseList = getInParam(collectionName, connectionTestRequest);
				metadataResponse.setCollectionName(collectionName);
			} else {
				Connection conn = getConnection(connectionTestRequest);
				Statement stmt = conn.createStatement();
				String query = "SHOW COLUMNS FROM " + collectionName;
				System.out.println(query);
				ResultSet rs = stmt.executeQuery(query);
				metadataResponse.setCollectionName(collectionName);
				while (rs.next()) {
					CollectionMetadataResponse collectionMetadataResponse = new CollectionMetadataResponse();
					collectionMetadataResponse.setName(rs.getString(1));
					// System.out.println(collectionName);
					collectionMetadataResponse.setType(rs.getString(2));
					collectionMetadataResponseList.add(collectionMetadataResponse);
				}
				conn.close();
			}

			metadataResponse.setCollectionMetadataResponseList(collectionMetadataResponseList);
			metadataResponse.setInResponseList(inResponseList);
			logger.debug("Exiting to com.datasync.serviceEngine.rdbmsConnectors.getMetaData()");
			return metadataResponse;
		}
		return null;

	}

	private List<InResponse> getInParam(String collectionName, ConnectionTestRequest connectionTestRequest)
			throws Exception {
		List<InResponse> inResponseList = new ArrayList<InResponse>();
		Connection conn = getConnection(connectionTestRequest);
		Statement stmt = conn.createStatement();
		String query = "select * FROM information_schema.parameters WHERE SPECIFIC_name = '" + collectionName + "'";
		System.out.println(query);
		ResultSet rsForINParam = stmt.executeQuery(query);
		while (rsForINParam.next()) {
			InResponse inMetadataResponse = new InResponse();
			inMetadataResponse.setName(rsForINParam.getString(6));
			inMetadataResponse.setType(rsForINParam.getString(7));
			inMetadataResponse.setOrdinalPosition(rsForINParam.getString(4));
			inMetadataResponse.setMaxLength(rsForINParam.getString(8));
			inResponseList.add(inMetadataResponse);
		}
		conn.close();
		return inResponseList;
	}

	public DataPreviewResponse getDataForPreview(ConnectionEntity connectionEntity, String collectionName,
			String collectionType, String previewFilter, Integer startRecord, Integer recordCount) throws Exception {
		if (connectionEntity.getType().equals(DataSyncUtilityMessage.oracle)) {
			logger.debug("Entering to com.datasync.serviceEngine.rdbmsConnectors.getDataForPreview()");
			ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
			DataPreviewResponse dataPreviewResponse = new DataPreviewResponse();
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
			Map<String, Object> row = null;
			int totalRecordCount = 0;
			connectionTestRequest.setConnectionType(connectionEntity.getType());
			connectionTestRequest
					.setServiceName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.serviceName));
			connectionTestRequest.setPort(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
			connectionTestRequest.setHost(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
			connectionTestRequest
					.setUserName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
			connectionTestRequest
					.setPassword(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
			try {
				String filterQuery = "";
				if (!previewFilter.equals("")) {
					filterQuery = " where " + getPreviewFilterQuery(previewFilter, connectionTestRequest);

				}
				Connection conn = getConnection(connectionTestRequest);
				Statement stmt1 = conn.createStatement();
				Statement stmt2 = conn.createStatement();
				int count = recordCount + startRecord;
				String query = "SELECT * FROM (SELECT A.*, rownum rn FROM (SELECT * FROM " + collectionName
						+ filterQuery + ") A WHERE rownum <= " + count + ") WHERE rn > " + startRecord;
				String query1 = "SELECT COUNT(*) as rowcount FROM " + collectionName + filterQuery;
				ResultSet rs = stmt1.executeQuery(query);
				ResultSetMetaData metaData = rs.getMetaData();
				ResultSet rs1 = stmt2.executeQuery(query1);
				Integer columnCount = metaData.getColumnCount();
				rs1.next();
				totalRecordCount = rs1.getInt("rowcount");
				while (rs.next()) {
					row = new HashMap<>();
					for (int i = 1; i <= columnCount; i++) {
						row.put(metaData.getColumnName(i), rs.getObject(i));
					}
					resultList.add(row);
					dataPreviewResponse.setReviewList(resultList);

				}
				dataPreviewResponse.setTotalRecordCount(totalRecordCount);
				conn.close();
			} catch (

			Exception e) {
				logger.error(e);
			}
			logger.debug("Exiting to com.datasync.serviceEngine.rdbmsConnectors.getDataForPreview()");
			return dataPreviewResponse;

		}
		if (connectionEntity.getType().equals(DataSyncUtilityMessage.mysql)) {
			logger.debug("Entering to com.datasync.serviceEngine.rdbmsConnectors.getDataForPreview()");
			ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
			DataPreviewResponse dataPreviewResponse = new DataPreviewResponse();
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
			Map<String, Object> row = null;
			int totalRecordCount = 0;

			connectionTestRequest.setConnectionType(connectionEntity.getType());
			connectionTestRequest
					.setSchemaName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.schemaName));
			connectionTestRequest.setPort(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
			connectionTestRequest.setHost(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
			connectionTestRequest
					.setUserName(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
			connectionTestRequest
					.setPassword(connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
			Connection conn = getConnection(connectionTestRequest);
			if (collectionType.equalsIgnoreCase("PROCEDURE")) {
				try {
					ResultSet rs = null;
					MetadataResponse metadataResponse = getMetaData(collectionName, connectionEntity, "procedure");
					rs = createProcedureQuery(metadataResponse, conn, previewFilter);
					ResultSetMetaData metaData = rs.getMetaData();

					Integer columnCount = metaData.getColumnCount();
					while (rs.next()) {
						row = new HashMap<>();
						for (int i = 1; i <= columnCount; i++) {
							row.put(metaData.getColumnName(i), rs.getObject(i));
						}
						resultList.add(row);
						dataPreviewResponse.setReviewList(resultList);

					}

					dataPreviewResponse.setTotalRecordCount(dataPreviewResponse.getReviewList().size());
					conn.close();
				} catch (

				Exception e) {
					logger.error(e);
				}
				logger.debug("Exiting to com.datasync.serviceEngine.rdbmsConnectors.getDataForPreview()");
				return dataPreviewResponse;

			}
			if (collectionType.equalsIgnoreCase("TABLE")) {
				try {
					String filterQuery = "";
					if (!previewFilter.equals("")) {
						filterQuery = " where " + getPreviewFilterQuery(previewFilter, connectionTestRequest);

					}
					Statement stmt1 = conn.createStatement();
					Statement stmt2 = conn.createStatement();
					int count = recordCount + startRecord;
					String query = "select * from " + collectionName + filterQuery + " limit " + count;
					String query1 = "SELECT COUNT(*) as rowcount from " + collectionName + filterQuery;
					ResultSet rs = stmt1.executeQuery(query);
					ResultSetMetaData metaData = rs.getMetaData();
					ResultSet rs1 = stmt2.executeQuery(query1);
					Integer columnCount = metaData.getColumnCount();
					rs1.next();

					totalRecordCount = rs1.getInt("rowcount");
					while (rs.next()) {
						row = new HashMap<>();
						for (int i = 1; i <= columnCount; i++) {
							row.put(metaData.getColumnName(i), rs.getObject(i));
						}
						resultList.add(row);
						dataPreviewResponse.setReviewList(resultList);

					}
					dataPreviewResponse.setTotalRecordCount(totalRecordCount);
					conn.close();
				} catch (

				Exception e) {
					logger.error(e);
				}
				logger.debug("Exiting to com.datasync.serviceEngine.rdbmsConnectors.getDataForPreview()");
				return dataPreviewResponse;

			}

		}

		return null;

	}

	public boolean createTable(ConnectionTestRequest connectionTestRequest, String CREATE_TABLE_STAT) throws Exception {
		Connection conn = getConnection(connectionTestRequest);
		PreparedStatement ps = conn.prepareStatement(CREATE_TABLE_STAT);
		ps.execute();
		conn.close();
		return true;
	}

	public int[] insert(PreparedStatement ps) throws Exception {
		int[] rows = ps.executeBatch();
		return rows;
	}

	public MappingRequest createInsertSQL(String tableName, String mapping, String sourceEntity) {
		MappingRequest mappingRequest = new MappingRequest();
		Map<String, String> mappingObject = new HashMap<String, String>();
		String insertQuery = "INSERT INTO " + tableName + "(";
		String valuesQuery = "VALUES (";
		String updateQuery = "UPDATE " + tableName + " SET ";
		JSONObject jo = new JSONObject(mapping);
		jo.keySet().stream().forEach((key) -> {
			mappingObject.put(key, jo.get(key).toString());
		});
		Map<String, String> treeMap = new TreeMap<String, String>(mappingObject);
		for (Map.Entry<String, String> entry : treeMap.entrySet()) {
			updateQuery = updateQuery + entry.getKey() + "= " + "? " + ", ";
			insertQuery = insertQuery + entry.getKey() + ", ";
			valuesQuery = valuesQuery + "?" + ", ";
		}
		String objectIdMapping = DataSyncUtilityMessage.primaryKey;
		for (Entry<String, String> entry : treeMap.entrySet()) {
			if (entry.getValue().equals(sourceEntity + "." + DataSyncUtilityMessage.primaryKey)) {
				objectIdMapping = entry.getKey();
			}
		}
		if (!insertQuery.equalsIgnoreCase("INSERT INTO (")) {
			insertQuery = insertQuery.substring(0, insertQuery.length() - 2);
			valuesQuery = valuesQuery.substring(0, valuesQuery.length() - 2);
			insertQuery = insertQuery + ") " + valuesQuery + ") ";
			updateQuery = updateQuery.substring(0, updateQuery.length() - 2) + " WHERE " + objectIdMapping + " = ?";
		}
		System.out.println("insertQuery : " + insertQuery);
		System.out.println("updateQuery : " + updateQuery);
		mappingRequest.setInsertSQL(insertQuery);
		mappingRequest.setUpdateSQL(updateQuery);
		mappingRequest.setObjectIdMapping(objectIdMapping);
		mappingRequest.setMapping(treeMap);
		return mappingRequest;
	}

	public String getPreviewFilterQuery(String filterJson, ConnectionTestRequest connectionTestRequest) {
		String filterQuery = "";
		if (connectionTestRequest.getConnectionType().equals(DataSyncUtilityMessage.oracle)) {
			List<Map<String, String>> mappingObjectList = new ArrayList<Map<String, String>>();
			JSONArray jo = new JSONArray(filterJson);
			for (int i = 0; i < jo.length(); i++) {
				JSONObject jo1 = jo.getJSONObject(i);
				Map<String, String> mappingObject = new HashMap<String, String>();
				jo1.keySet().stream().forEach((key) -> {
					mappingObject.put(key, jo1.get(key).toString());
				});
				mappingObjectList.add(mappingObject);
			}
			for (int i = 0; i < mappingObjectList.size(); i++) {
				if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.logicalOperator).isEmpty()) {
					filterQuery = filterQuery + " "
							+ mappingObjectList.get(i).get(DataSyncUtilityMessage.logicalOperator).toLowerCase() + " ";
				}
				filterQuery = filterQuery + mappingObjectList.get(i).get(DataSyncUtilityMessage.field) + " "
						+ getComparisonOperator(
								mappingObjectList.get(i).get(DataSyncUtilityMessage.comparisonOperator));
				if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType).isEmpty() && mappingObjectList.get(i)
						.get(DataSyncUtilityMessage.dataType).equalsIgnoreCase(DataSyncUtilityMessage.datetimeoffset)) {
					filterQuery = filterQuery + " datetimeoffset'"
							+ mappingObjectList.get(i).get(DataSyncUtilityMessage.value).replace(" ", " ") + "'";
				} else if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType).isEmpty()
						&& mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType)
								.equalsIgnoreCase(DataSyncUtilityMessage.datetime)) {
					filterQuery = filterQuery + " datetime'"
							+ mappingObjectList.get(i).get(DataSyncUtilityMessage.value).replace(" ", " ") + "'";
				} else if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType).isEmpty() && mappingObjectList
						.get(i).get(DataSyncUtilityMessage.dataType).equalsIgnoreCase(DataSyncUtilityMessage.bool)) {
					filterQuery = filterQuery + " " + mappingObjectList.get(i).get(DataSyncUtilityMessage.value);

				} else {
					filterQuery = filterQuery + " '" + mappingObjectList.get(i).get("value").replace(" ", " ") + "'";
				}
			}
			System.out.println("filterQuery ::" + filterQuery);

		}
		if (connectionTestRequest.getConnectionType().equals(DataSyncUtilityMessage.mysql)) {
			List<Map<String, String>> mappingObjectList = new ArrayList<Map<String, String>>();
			JSONArray jo = new JSONArray(filterJson);
			for (int i = 0; i < jo.length(); i++) {
				JSONObject jo1 = jo.getJSONObject(i);
				Map<String, String> mappingObject = new HashMap<String, String>();
				jo1.keySet().stream().forEach((key) -> {
					mappingObject.put(key, jo1.get(key).toString());
				});
				mappingObjectList.add(mappingObject);
			}
			for (int i = 0; i < mappingObjectList.size(); i++) {
				if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.logicalOperator).isEmpty()) {
					filterQuery = filterQuery + " "
							+ mappingObjectList.get(i).get(DataSyncUtilityMessage.logicalOperator).toLowerCase() + " ";
				}
				filterQuery = filterQuery + mappingObjectList.get(i).get(DataSyncUtilityMessage.field) + " "
						+ getComparisonOperator(
								mappingObjectList.get(i).get(DataSyncUtilityMessage.comparisonOperator));
				if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType).isEmpty() && mappingObjectList.get(i)
						.get(DataSyncUtilityMessage.dataType).equalsIgnoreCase(DataSyncUtilityMessage.datetimeoffset)) {
					filterQuery = filterQuery + " datetimeoffset'"
							+ mappingObjectList.get(i).get(DataSyncUtilityMessage.value).replace(" ", " ") + "'";
				} else if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType).isEmpty()
						&& mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType)
								.equalsIgnoreCase(DataSyncUtilityMessage.datetime)) {
					filterQuery = filterQuery + " datetime'"
							+ mappingObjectList.get(i).get(DataSyncUtilityMessage.value).replace(" ", " ") + "'";
				} else if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType).isEmpty() && mappingObjectList
						.get(i).get(DataSyncUtilityMessage.dataType).equalsIgnoreCase(DataSyncUtilityMessage.bool)) {
					filterQuery = filterQuery + " " + mappingObjectList.get(i).get(DataSyncUtilityMessage.value);

				} else {
					filterQuery = filterQuery + " '" + mappingObjectList.get(i).get("value").replace(" ", " ") + "'";
				}
			}
			System.out.println("filterQuery ::" + filterQuery);
		}
		return filterQuery;
	}

	private String getComparisonOperator(String comparisonOperator) {
		switch (comparisonOperator) {
		case "equal":
			return "=";
		case "not equal":
			return "!=";
		case "less than":
			return "<";
		case "less than or equal":
			return "<=";
		case "greater than":
			return ">";
		case "Greater than or equal":
			return ">=";
		default:
			return "=";
		}
	}

	public ResultSet createProcedureQuery(MetadataResponse metadataResponse, Connection conn, String jobFilter)
			throws JsonProcessingException, ParseException, SQLException {
		// String runProcedure = "{ call GetAllPartner() }";
		/*
		 * String Date1="01/01/1800"; Date nullDate=new
		 * SimpleDateFormat("dd/MM/yyyy").parse(Date1);
		 */
		List<Map<String, String>> mappingObjectList = new ArrayList<Map<String, String>>();
		if (jobFilter != null && !jobFilter.isEmpty()) {
			JSONArray jo = new JSONArray(jobFilter);
			for (int i = 0; i < jo.length(); i++) {
				JSONObject jo1 = jo.getJSONObject(i);
				Map<String, String> mappingObject = new HashMap<String, String>();
				jo1.keySet().stream().forEach((key) -> {
					mappingObject.put(key, jo1.get(key).toString());
				});
				mappingObjectList.add(mappingObject);
			}
		}
		String runProcedure = "{ call ";
		CallableStatement statement = null;
		if (mappingObjectList != null && !mappingObjectList.isEmpty()) {

			try {

				runProcedure = runProcedure + metadataResponse.getCollectionName() + "(";
				for (int i = 0; i < metadataResponse.getInResponseList().size(); i++) {
					runProcedure = runProcedure + "?,";
				}

				runProcedure = runProcedure.substring(0, runProcedure.length() - 1);
				runProcedure = runProcedure + ") }";
				System.out.println("Procedure Query.." + runProcedure);
				statement = conn.prepareCall(runProcedure);

				metadataResponse.getInResponseList().stream()
						.sorted((p1, p2) -> p1.getOrdinalPosition().compareTo(p2.getOrdinalPosition()));
				for (int i = 0; i < metadataResponse.getInResponseList().size(); i++) {
					if (mappingObjectList.get(i).get("dataType").equals("varchar")) {

						final int j = i;
						List<Map<String, String>> mappingObjectList1 = mappingObjectList.stream().filter(
								x -> (metadataResponse.getInResponseList().get(j).getName().equals(x.get("field"))))
								.collect(Collectors.toList());

						statement.setString(i + 1, mappingObjectList1.get(0).get("value"));
					} else if (mappingObjectList.get(i).get("dataType").equals("datetime")) {

						final int j = i;
						List<Map<String, String>> mappingObjectList1 = mappingObjectList.stream().filter(
								x -> (metadataResponse.getInResponseList().get(j).getName().equals(x.get("field"))))
								.collect(Collectors.toList());
						if (!mappingObjectList1.get(0).get("value").isEmpty()) {
							statement.setString(i + 1, mappingObjectList1.get(0).get("value"));
						} else {
							statement.setString(i + 1, "1800/01/01");
						}
					}
				}

				return statement.executeQuery();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		} else {
			runProcedure = runProcedure + metadataResponse.getCollectionName() + "(";
			runProcedure = runProcedure + ") }";
			statement = conn.prepareCall(runProcedure);
			return statement.executeQuery();
		}
	}

}
