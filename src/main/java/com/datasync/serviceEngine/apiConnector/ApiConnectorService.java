package com.datasync.serviceEngine.apiConnector;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.stereotype.Service;

import com.datasync.entity.ConnectionEntity;
import com.datasync.response.ApiMetaDataResponse;
import com.datasync.response.CollectionMetadataResponse;
import com.datasync.response.DataPreviewResponse;
import com.datasync.response.MetadataResponse;
import com.datasync.response.MetadataResponseDetails;
import com.datasync.serviceEngine.Impl.ConnectorFactoryImpl;
import com.datasync.serviceEngine.odataConnector.HttpClientService;
import com.datasync.util.DataSyncUtilityMessage;

@Service
public class ApiConnectorService {

	private static Logger logger = LogManager.getLogger(ApiConnectorService.class);

	@Autowired
	HttpClientService httpClientService;

	public MetadataResponse getMetaData(String collectionName, ConnectionEntity connectionEntity) {
		logger.debug("Entering to com.datasync.serviceEngine.apiConnector.getMetaData()");
		MetadataResponse metadataResponse = new MetadataResponse();
		if (connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.datahug)) {
			List<ApiMetaDataResponse> apiMetaDataResponseList = getAPIWithoutSwaggerMetaData(connectionEntity);
			metadataResponse.setCollectionName("Datahug Territory");
			System.out.println(collectionName);
			if (apiMetaDataResponseList != null && apiMetaDataResponseList.size() != 0) {
				metadataResponse.setCollectionMetadataResponseList(
						apiMetaDataResponseList.get(0).getCollectionMetadataResponse());
			}
		} else {
			List<ApiMetaDataResponse> apiMetaDataResponseList = getAPIMetaData();
			List<ApiMetaDataResponse> result = apiMetaDataResponseList.stream()
					.filter(x -> collectionName.equals(x.getApiName())).collect(Collectors.toList());
			metadataResponse.setCollectionName(collectionName);
			System.out.println(collectionName);
			if (result != null && result.size() != 0) {
				metadataResponse.setCollectionMetadataResponseList(result.get(0).getCollectionMetadataResponse());
			}
		}
		logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getMetaData()");
		return metadataResponse;
	}

	public MetadataResponseDetails getEntityNames(String connectionId, ConnectionEntity connectionEntity) {
		logger.debug("Entering to com.datasync.serviceEngine.apiConnector.getEntityNames()");
		MetadataResponseDetails metadataResponseDetails = new MetadataResponseDetails();
		List<MetadataResponse> metadataResponseList = new ArrayList<MetadataResponse>();
		if (connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.datahug)) {
			// List<ApiMetaDataResponse> apiMetaDataResponseList =
			// getAPIWithoutSwaggerMetaData();
			// for (int i = 0; i < apiMetaDataResponseList.size(); i++) {
			MetadataResponse metadataResponse = new MetadataResponse();
			metadataResponse.setCollectionName("Datahug Territory");
			metadataResponse.setApiUrl(connectionEntity.getConnectionConfig().get("url"));
			metadataResponseList.add(metadataResponse);
			// }
		} else {
			List<ApiMetaDataResponse> apiMetaDataResponseList = getAPIMetaData();
			for (int i = 0; i < apiMetaDataResponseList.size(); i++) {
				MetadataResponse metadataResponse = new MetadataResponse();
				metadataResponse.setCollectionName(apiMetaDataResponseList.get(i).getApiName());
				metadataResponse.setApiUrl(apiMetaDataResponseList.get(i).getApiUrl());
				metadataResponseList.add(metadataResponse);
			}
		}
		metadataResponseDetails.setMetadataTableResponse(metadataResponseList);
		logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getEntityNames()");
		return metadataResponseDetails;
	}

	public List<ApiMetaDataResponse> getAPIMetaData() {
		logger.debug("Entering to com.datasync.serviceEngine.apiConnector.getAPIMetaData()");
		List<ApiMetaDataResponse> apiMetaDataResponseList = new ArrayList<ApiMetaDataResponse>();
		Object obj = null;
		try {
			obj = new JSONParser().parse(new InputStreamReader(
					ApiConnectorService.class.getClassLoader().getResourceAsStream("apiDocs.json")));
			JSONObject doc = (JSONObject) obj;
			Map path = getJsonMapValue(doc, "paths");
			Iterator<Map.Entry> itrPath = path.entrySet().iterator();
			while (itrPath.hasNext()) {
				Map.Entry pair = itrPath.next();
				String apiUrl = (String) pair.getKey();
				// System.out.println(apiUrl);
				JSONObject apiValue = (JSONObject) pair.getValue();
				Iterator<Map.Entry> itrApiDetails = apiValue.entrySet().iterator();
				while (itrApiDetails.hasNext()) {
					Map.Entry pairApiDetails = itrApiDetails.next();
					String apiAction = (String) pairApiDetails.getKey();
					// System.out.println(apiAction);
					if (apiAction.equals("post") && apiUrl.contains("dataSync")) {
						ApiMetaDataResponse apiMetaDataResponse = new ApiMetaDataResponse();
						// System.out.println(apiUrl);
						// System.out.println(apiAction);
						JSONObject actionValue = (JSONObject) pairApiDetails.getValue();
						String apiName = (String) actionValue.get("summary");
						// System.out.println(apiName);
						String reqBodyName = getBodyName(actionValue);
						JSONObject reqParam = getReqParams(doc, reqBodyName);
						Map properties = (Map) reqParam.get("properties");
						Iterator<Map.Entry> itrproperties = properties.entrySet().iterator();
						List<CollectionMetadataResponse> apiMetaDataBodyList = new ArrayList<CollectionMetadataResponse>();
						while (itrproperties.hasNext()) {
							Map.Entry pairNameType = itrproperties.next();
							// String A
							// String paramName = (String) pairNameType.getKey();
							// System.out.println(paramName);
							JSONObject paramValue = (JSONObject) pairNameType.getValue();
							String paramType = (String) paramValue.get("type");
							String refBodyName = "";
							apiMetaDataResponse.setRequestBodyType("null");
							if (paramType.equals("array")) {
								JSONObject items = (JSONObject) paramValue.get("items");
								refBodyName = items.toString();
								// System.out.println(items);
							}
							// System.out.println(paramType);
							if (paramType.equals("array") && refBodyName.contains("ref")) {
								apiMetaDataResponse.setRequestBodyType("Array");
								String requestBodyName = getReqBodyName((JSONObject) paramValue);
								JSONObject reqBodyParam = getReqParams(doc, requestBodyName);
								Map reqBodyProperties = (Map) reqBodyParam.get("properties");
								Iterator<Map.Entry> itrReqBodyProperties = reqBodyProperties.entrySet().iterator();
								while (itrReqBodyProperties.hasNext()) {
									CollectionMetadataResponse collectionMetadataResponse = new CollectionMetadataResponse();
									Map.Entry ReqPairNameType = itrReqBodyProperties.next();
									// String A
									String reqParamName = (String) ReqPairNameType.getKey();
									// System.out.println(reqParamName);
									JSONObject reqParamValue = (JSONObject) ReqPairNameType.getValue();
									String reqParamType = (String) reqParamValue.get("type");
									// System.out.println(reqParamType);
									collectionMetadataResponse.setName(reqParamName);
									// System.out.println(reqParamName);
									collectionMetadataResponse.setType(reqParamType);
									apiMetaDataBodyList.add(collectionMetadataResponse);
								}
							} else {
								CollectionMetadataResponse collectionMetadataResponse = new CollectionMetadataResponse();
								String paramName = (String) pairNameType.getKey();
								// System.out.println(paramName);
								String paramsType = (String) paramValue.get("type");
								// System.out.println(paramsType);
								collectionMetadataResponse.setName(paramName);
								// System.out.println(paramName);
								collectionMetadataResponse.setType(paramsType);
								apiMetaDataBodyList.add(collectionMetadataResponse);
							}
						}
						apiMetaDataResponse.setApiName(apiName);
						apiMetaDataResponse.setApiUrl(apiUrl);
						apiMetaDataResponse.setApiAction(apiAction);
						apiMetaDataResponse.setCollectionMetadataResponse(apiMetaDataBodyList);
						apiMetaDataResponseList.add(apiMetaDataResponse);
					}
				}
			}
		} catch (FileNotFoundException e) {
			logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getAPIMetaData() with Exception"
					+ e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getAPIMetaData() with Exception"
					+ e.getMessage());
			e.printStackTrace();
		} catch (ParseException e) {
			logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getAPIMetaData() with Exception"
					+ e.getMessage());
			e.printStackTrace();
		}
		logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getAPIMetaData()");
		return apiMetaDataResponseList;
	}

	@SuppressWarnings("unchecked")
	public List<ApiMetaDataResponse> getAPIWithoutSwaggerMetaData(ConnectionEntity connectionEntity) {
		logger.debug("Entering to com.datasync.serviceEngine.apiConnector.getAPIWithoutSwaggerMetaData()");
		List<ApiMetaDataResponse> apiMetaDataResponseList = new ArrayList<ApiMetaDataResponse>();
		try {
			HttpURLConnection connectionAccessToken = httpClientService.getConnection(
					connectionEntity.getConnectionConfig().get("tokenUrl"), DataSyncUtilityMessage.APPLICATION_JSON,
					DataSyncUtilityMessage.HTTP_METHOD_GET, connectionEntity.getConnectionConfig().get("userName"),
					connectionEntity.getConnectionConfig().get("password"));
			InputStream content = connectionAccessToken.getInputStream();
			String responseAccessToken = IOUtils.toString(content);
			JsonParser springParser = JsonParserFactory.getJsonParser();
			System.out.println("response :" + responseAccessToken);
			Map<String, Object> mapAccessToken = springParser.parseMap(responseAccessToken);
			String accessToken = mapAccessToken.get("access_token").toString();
			HttpURLConnection connectionAPI = httpClientService.getConnectionAPIWithoutSwagger(
					connectionEntity.getConnectionConfig().get("url"), DataSyncUtilityMessage.APPLICATION_JSON,
					DataSyncUtilityMessage.HTTP_METHOD_GET, accessToken);
			content = connectionAPI.getInputStream();
			String responseAPI = IOUtils.toString(content);
			content.close();
			Map<String, Object> map = springParser.parseMap(responseAPI);
			ApiMetaDataResponse apiMetaDataResponse = new ApiMetaDataResponse();
			List<CollectionMetadataResponse> apiMetaDataBodyList = new ArrayList<CollectionMetadataResponse>();
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				if (entry.getValue() instanceof List<?>) {
					List<Map<String, Object>> objectList = (ArrayList<Map<String, Object>>) entry.getValue();
					for (Map<String, Object> object : objectList) {
						for (Map.Entry<String, Object> entry1 : object.entrySet()) {
							if (entry1.getValue() instanceof Map) {
								Map<String, Object> map2 = (Map<String, Object>) entry1.getValue();
								for (Map.Entry<String, Object> entry2 : map2.entrySet()) {
									CollectionMetadataResponse collectionMetadataResponse = new CollectionMetadataResponse();
									System.out.println("List Third Level Map::: " + entry1.getKey() + "_"
											+ entry2.getKey() + " = " + entry2.getValue());
									collectionMetadataResponse.setName(entry1.getKey() + "_" + entry2.getKey());
									if (entry2.getValue() instanceof Double) {
										collectionMetadataResponse.setType("NUMBER");
									} else if (entry2.getValue() instanceof Integer) {
										collectionMetadataResponse.setType("NUMBER");
									} else {
										collectionMetadataResponse.setType("VARCHAR2(200)");
									}
									apiMetaDataBodyList.add(collectionMetadataResponse);
								}
							} else {
								CollectionMetadataResponse collectionMetadataResponse = new CollectionMetadataResponse();
								System.out.println("List Second Level Key Value Pair::: " + entry1.getKey() + " = "
										+ entry1.getValue());
								collectionMetadataResponse.setName(entry1.getKey());
								if (entry1.getValue() instanceof Integer) {
									collectionMetadataResponse.setType("NUMBER");
								} else {
									collectionMetadataResponse.setType("VARCHAR2(200)");
								}
								apiMetaDataBodyList.add(collectionMetadataResponse);
							}

						}
						break;
					}
				}
			}
			apiMetaDataResponse.setCollectionMetadataResponse(apiMetaDataBodyList);
			apiMetaDataResponseList.add(apiMetaDataResponse);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getAPIWithoutSwaggerMetaData()");
		return apiMetaDataResponseList;
	}

	@SuppressWarnings("unchecked")
	public DataPreviewResponse getDataForPreview(String collectionName, ConnectionEntity connectionEntity) {
		logger.debug("Entering to com.datasync.serviceEngine.apiConnector.getAPIWithoutSwaggerMetaData()");
		DataPreviewResponse dataPreviewResponse = new DataPreviewResponse();
		List<Map<String, Object>> responseList = new ArrayList<Map<String, Object>>();
		try {
			HttpURLConnection connectionAccessToken = httpClientService.getConnection(
					connectionEntity.getConnectionConfig().get("tokenUrl"), DataSyncUtilityMessage.APPLICATION_JSON,
					DataSyncUtilityMessage.HTTP_METHOD_GET, connectionEntity.getConnectionConfig().get("userName"),
					connectionEntity.getConnectionConfig().get("password"));
			InputStream content = connectionAccessToken.getInputStream();
			String responseAccessToken = IOUtils.toString(content);
			JsonParser springParser = JsonParserFactory.getJsonParser();
			System.out.println("response :" + responseAccessToken);
			Map<String, Object> mapAccessToken = springParser.parseMap(responseAccessToken);
			String accessToken = mapAccessToken.get("access_token").toString();
			HttpURLConnection connectionAPI = httpClientService.getConnectionAPIWithoutSwagger(
					connectionEntity.getConnectionConfig().get("url"), DataSyncUtilityMessage.APPLICATION_JSON,
					DataSyncUtilityMessage.HTTP_METHOD_GET, accessToken);
			content = connectionAPI.getInputStream();
			String responseAPI = IOUtils.toString(content);
			content.close();
			Map<String, Object> map = springParser.parseMap(responseAPI);
			ApiMetaDataResponse apiMetaDataResponse = new ApiMetaDataResponse();
			List<CollectionMetadataResponse> apiMetaDataBodyList = new ArrayList<CollectionMetadataResponse>();
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				if (entry.getValue() instanceof List<?>) {
					List<Map<String, Object>> objectList = (ArrayList<Map<String, Object>>) entry.getValue();
					for (Map<String, Object> object : objectList) {
						Map<String, Object> responseMap = new HashMap<String, Object>();
						for (Map.Entry<String, Object> entry1 : object.entrySet()) {
							if (entry1.getValue() instanceof Map) {
								Map<String, Object> map2 = (Map<String, Object>) entry1.getValue();
								for (Map.Entry<String, Object> entry2 : map2.entrySet()) {
									System.out.println("List Third Level Map::: " + entry1.getKey() + "_"
											+ entry2.getKey() + " = " + entry2.getValue());
									responseMap.put(entry1.getKey() + "_" + entry2.getKey(), entry2.getValue());
								}
							} else {
								System.out.println("List Second Level Key Value Pair::: " + entry1.getKey() + " = "
										+ entry1.getValue());
								responseMap.put(entry1.getKey(), entry1.getValue());
							}

						}
						responseList.add(responseMap);
					}
				}
			}
			dataPreviewResponse.setReviewList(responseList);
			dataPreviewResponse.setTotalRecordCount(0);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getAPIWithoutSwaggerMetaData()");
		return dataPreviewResponse;
	}

	public JSONObject getReqParams(JSONObject src, String key) {
		logger.debug("Entering to com.datasync.serviceEngine.apiConnector.getReqParams()");
		Map definitions = (Map) src.get("definitions");
		JSONObject definitionsValue = null;
		Iterator<Map.Entry> itrDefinitions = definitions.entrySet().iterator();
		while (itrDefinitions.hasNext()) {
			Map.Entry pairDefinitions = itrDefinitions.next();
			if (pairDefinitions.getKey().toString().toLowerCase().equals(key.toLowerCase())) {

				definitionsValue = ((JSONObject) pairDefinitions.getValue());
			}
		}
		logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getReqParams()");
		return definitionsValue;
	}

	public Map getJsonMapValue(JSONObject src, String key) {
		logger.debug("Entering to com.datasync.serviceEngine.apiConnector.getJsonMapValue()");
		logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getJsonMapValue()");
		return (Map) src.get(key);
	}

	public String getBodyName(JSONObject src) {
		logger.debug("Entering to com.datasync.serviceEngine.apiConnector.getBodyName()");
		JSONArray paramArray = (JSONArray) src.get("parameters");
		Iterator itrParamArray = paramArray.iterator();
		String requestName = "";
		while (itrParamArray.hasNext()) {
			JSONObject paramObj = (JSONObject) itrParamArray.next();
			// System.out.println(paramObj);
			if (paramObj.get("in").equals("body")) {
				JSONObject schema = (JSONObject) paramObj.get("schema");
				if (schema.get("type") == null) {
					String ref = (String) schema.get("$ref");
					String[] refArray = ref.split("/");
					requestName = refArray[refArray.length - 1];
				} else {
					requestName = getReqBodyName(schema);
				}
				// System.out.println(requestName);
			}
		}
		logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getBodyName()");
		return requestName;
	}

	public String getReqBodyName(JSONObject src) {
		logger.debug("Entering to com.datasync.serviceEngine.apiConnector.getReqBodyName()");
		JSONObject items = (JSONObject) src.get("items");
		String ref = (String) items.get("$ref");
		String[] refArray = ref.split("/");
		String reqBody = refArray[refArray.length - 1];
		// System.out.println(reqBody);
		logger.debug("Exiting to com.datasync.serviceEngine.apiConnector.getReqBodyName()");
		return reqBody;
	}

	public List<CollectionMetadataResponse> getMetaData(String collectionName) {
		List<CollectionMetadataResponse> collectionMetadataResponseList = new ArrayList<CollectionMetadataResponse>();
		Object obj = null;
		try {
			obj = new JSONParser().parse(new InputStreamReader(
					ApiConnectorService.class.getClassLoader().getResourceAsStream("MySql.json")));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject doc = (JSONObject) obj;
		Map procedureName = (Map) doc.get(collectionName);
		Iterator<Map.Entry> itrprocedureName = procedureName.entrySet().iterator();
		while (itrprocedureName.hasNext()) {
			CollectionMetadataResponse collectionMetadataResponse = new CollectionMetadataResponse();
			Map.Entry pair = itrprocedureName.next();
			String name = (String) pair.getKey();
			collectionMetadataResponse.setName(name);
			String type = (String) pair.getValue();
			collectionMetadataResponse.setType(type);
			collectionMetadataResponseList.add(collectionMetadataResponse);
		}
		return collectionMetadataResponseList;

	}

}
