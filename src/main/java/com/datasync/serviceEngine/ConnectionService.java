package com.datasync.serviceEngine;

import java.util.List;

import com.datasync.exception.DataSyncServiceException;
import com.datasync.request.ConnectionDataRequest;
import com.datasync.response.ConnectionDetailsResponse;
import com.datasync.response.ConnectionResponse;

public interface ConnectionService {

	public ConnectionResponse getConnection(String connectionId);

	public ConnectionDetailsResponse getAllConnections(ConnectionDataRequest connectionDataRequest, Integer startRecord,
			Integer recordCount, String sortableColumn, String sortingType);

	public ConnectionResponse saveOrUpdateConnection(ConnectionDataRequest connectionDataRequest)
			throws DataSyncServiceException;;

	public boolean deleteConnections(List<String> connectionIds);
}
