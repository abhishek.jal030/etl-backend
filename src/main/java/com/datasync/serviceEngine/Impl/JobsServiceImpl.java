package com.datasync.serviceEngine.Impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.datasync.entity.BulkJobDetailsEntity;
import com.datasync.entity.BulkJobEntity;
import com.datasync.entity.JobAuditEntity;
import com.datasync.entity.JobEntity;
import com.datasync.entity.JobStatusEntity;
import com.datasync.entity.ScheduledJobEntity;
import com.datasync.entity.TempJobEntity;
import com.datasync.exception.DataSyncServiceException;
import com.datasync.mail.MailService;
import com.datasync.repository.BulkJobDetailsRepository;
import com.datasync.repository.BulkJobRepository;
import com.datasync.repository.CustomEntityRepository;
import com.datasync.repository.JobRepository;
import com.datasync.repository.JobStatusRepository;
import com.datasync.repository.ScheduledjobRepository;
import com.datasync.repository.TempJobRepository;
import com.datasync.request.BulkJobRequest;
import com.datasync.request.JobRequest;
import com.datasync.request.ScheduledJobRequest;
import com.datasync.request.TableMetaDataRequest;
import com.datasync.request.TableMetadata;
import com.datasync.request.TempJobRequest;
import com.datasync.response.BulkJobResponse;
import com.datasync.response.JobDetailsResponse;
import com.datasync.response.JobProgressResponse;
import com.datasync.response.JobResponse;
import com.datasync.response.MetadataResponse;
import com.datasync.response.TempJobResponse;
import com.datasync.response.ViewJobDifferenceDetailsResponse;
import com.datasync.response.ViewJobDifferenceResponse;
import com.datasync.response.ViewLogDetailsResponse;
import com.datasync.response.ViewLogResponse;
import com.datasync.serviceEngine.ConnectorFactory;
import com.datasync.serviceEngine.JobsService;
import com.datasync.serviceEngine.odataConnector.MetadataService;
import com.datasync.util.CommonUtil;
import com.datasync.util.DataSyncUtilityMessage;
import com.datasync.util.JobsUtil;

@Service
public class JobsServiceImpl implements JobsService {

	private static Logger logger = LogManager.getLogger(JobsServiceImpl.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private BulkJobRepository bulkJobRepository;

	@Autowired
	BulkJobDetailsRepository bulkJobDetailsRepository;

	@Autowired
	private ScheduledjobRepository scheduledjobRepository;

	@Autowired
	CustomEntityRepository customEntityRepository;

	@Autowired
	ConnectorFactory connectorFactory;

	@Autowired
	JobStatusRepository jobStatusRepository;

	@Autowired
	MetadataService metadataService;

	@Autowired
	TempJobRepository tempJobRepository;

	@Autowired
	MailService mailService;

	@Override
	public JobResponse getJob(String jobId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.getJob()");
		JobEntity jobEntity = new JobEntity();
		Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobId);
		if (jobEntityOptional.isPresent()) {
			jobEntity = jobEntityOptional.get();
		} else {
			return null;
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.getJob()");

		return JobsUtil.buildResponseAllMapsDetail(jobEntity);
	}

	@Override
	public JobDetailsResponse getAllJobs(JobRequest jobRequest, String sortableColumn, String sortingType,
			Integer startRecord, Integer recordCount) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.getAllJobs()");
		List<JobEntity> jobsEntityList = customEntityRepository.findAllJobs(jobRequest, startRecord, recordCount,
				sortableColumn, sortingType, false);

		Long totalRecordCount = customEntityRepository.countTotalJobsRecord(jobRequest, startRecord, recordCount,
				sortableColumn, sortingType);
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.getAllJobs()");
		return JobsUtil.buildResponseAllMapsDetail(jobsEntityList, totalRecordCount);
	}

	@Override
	public JobResponse saveOrUpdateJob(JobRequest jobRequest) throws DataSyncServiceException {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.saveOrUpdateJob()");
		JobResponse jobResponse = new JobResponse();
		try {
			List<JobEntity> jobEntityJobNameList = jobRepository.findByJobName(jobRequest.getJobName());
			if ((jobRequest.getJobId() == null || jobRequest.getJobId().isEmpty()) && jobEntityJobNameList != null
					&& jobEntityJobNameList.size() != 0) {
				if (jobEntityJobNameList.get(0).getJobName().equals(jobRequest.getJobName())) {
					throw new DataSyncServiceException("Name Already Exists");
				}
			}
			JobEntity jobEntity = new JobEntity();
			if (jobRequest.getJobId() != null && !jobRequest.getJobId().isEmpty()) {
				Optional<JobEntity> mapEntityOptional = jobRepository.findById(jobRequest.getJobId());
				if (!mapEntityOptional.isPresent()) {
					jobResponse = null;
				} else {
					jobEntity = mapEntityOptional.get();
				}
			}
			if (jobRequest.getJobId() == null || jobRequest.getJobId().isEmpty()) {
				jobEntity.setCreatedBy(jobRequest.getCreatedBy());
				jobEntity.setCreatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			}
			jobEntity.setSourceCollectionType(jobRequest.getSourceCollectionType());
			jobEntity.setJobName(jobRequest.getJobName());
			jobEntity.setDestinationConnectionName(jobRequest.getDestinationConnectionName());
			jobEntity.setDestinationConnection(jobRequest.getDestinationConnection());
			jobEntity.setSourceConnectionName(jobRequest.getSourceConnectionName());
			jobEntity.setSourceConnection(jobRequest.getSourceConnection());
			jobEntity.setSourceCollection(jobRequest.getSourceCollection());
			jobEntity.setDestinationCollection(jobRequest.getDestinationCollection());
			jobEntity.setMapping(jobRequest.getMapping());
			jobEntity.setJobFilter(jobRequest.getJobFilter());
			jobEntity.setIncrementalParameter(jobRequest.getIncrementalParameter());
			if (jobRequest.getIncrementalParameterValue() != null
					&& !jobRequest.getIncrementalParameterValue().isEmpty()) {
				SimpleDateFormat sm = new SimpleDateFormat(DataSyncUtilityMessage.TIME_STAMP);
				Date dat = new Date();
				try {
					dat = sm.parse(jobRequest.getIncrementalParameterValue());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				jobEntity.setIncrementalParameterValue(dat);
			} else {
				jobEntity.setIncrementalParameterValue(null);
			}
			jobEntity.setUpdatedBy(jobRequest.getUpdatedBy());
			jobEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			JobEntity map = jobRepository.save(jobEntity);

			jobResponse = JobsUtil.buildResponseAllMapsDetail(map);
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.saveOrUpdateJob()");
			return jobResponse;
		} catch (DataAccessException de) {
			logger.error(de.getMessage());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new DataSyncServiceException(ex.getMessage(), ex.getCause());
		}

		return jobResponse;
	}

	@Override
	public boolean deleteJobs(List<String> jobids) throws DataSyncServiceException {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.deleteJobs()");
		Query query = new Query();
		try {
			for (int i = 0; i < jobids.size(); i++) {
				List<JobStatusEntity> jobStatusEntityList = customEntityRepository.findByIdAndStatus(jobids.get(i));
				if (jobStatusEntityList != null && jobStatusEntityList.size() != 0) {
					throw new DataSyncServiceException("Job is currently in process");
				} else {
					query.addCriteria(Criteria.where("jobId").in(jobids.get(i)));
					mongoTemplate.remove(query, JobEntity.class);
					logger.debug("Exiting to com.datasync.serviceEngine.Impl.deleteJobs()");
				}
			}
			return true;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new DataSyncServiceException(ex.getMessage(), ex.getCause());
		}
	}

	public boolean runJob(String jobId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.runJob()");
		JobEntity jobEntity = new JobEntity();
		Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobId);
		if (jobEntityOptional.isPresent()) {
			jobEntity = jobEntityOptional.get();
		} else {
			return false;
		}
		TempJobEntity tempJobEntity = tempJobRepository.findByJobIdAndIsArchived(jobId, false);
		try {
			Optional<JobStatusEntity> jobStatusEntityOptional = null;
			if (tempJobEntity != null && tempJobEntity.getNewMapping() != null
					&& !tempJobEntity.getNewMapping().isEmpty()) {
				connectorFactory.runJobDifference(jobEntity, tempJobEntity.getNewMapping(), jobStatusEntityOptional);
			} else if (jobEntity.getIncrementalParameter() != null && !jobEntity.getIncrementalParameter().isEmpty()) {
				connectorFactory.runJobIncremental(jobEntity, jobStatusEntityOptional);
			} else {
				connectorFactory.runJob(jobEntity, jobStatusEntityOptional);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.runJob()");
		return true;
	}

	public boolean runJobUpload(String jobId, MultipartFile file) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.runJob()");
		JobEntity jobEntity = new JobEntity();
		Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobId);
		if (jobEntityOptional.isPresent()) {
			jobEntity = jobEntityOptional.get();
		} else {
			return false;
		}
		try {
			Optional<JobStatusEntity> jobStatusEntityOptional = null;
			connectorFactory.runJobUpload(jobEntity, jobStatusEntityOptional, file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.runJob()");
		return true;
	}

	public Map<String, Object> runJobExport(String jobId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.runJob()");
		JobEntity jobEntity = new JobEntity();
		Map<String, Object> response = new HashMap<String, Object>();
		Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobId);
		if (jobEntityOptional.isPresent()) {
			jobEntity = jobEntityOptional.get();
		} else {
			return null;
		}
		try {
			Optional<JobStatusEntity> jobStatusEntityOptional = null;
			response = connectorFactory.runJobExport(jobEntity, jobStatusEntityOptional);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.runJob()");
		return response;
	}

	public JobProgressResponse jobProgress(String jobId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.jobProgress()");
		JobStatusEntity jobStatusEntity = new JobStatusEntity();
		List<JobStatusEntity> jobStatusEntityList = customEntityRepository.findByJobId(jobId, false);
		if (jobStatusEntityList != null && jobStatusEntityList.size() != 0) {
			jobStatusEntity = jobStatusEntityList.get(0);
		} else {
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.jobProgress()");
			return new JobProgressResponse();
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.jobProgress()");
		return JobsUtil.buildResponseJobProgress(jobStatusEntity);
	}

	public JobProgressResponse[] jobInProgress() {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.jobInProgress()");
		List<JobStatusEntity> jobStatusEntityList = customEntityRepository.findByStatus();
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.jobInProgress()");
		return JobsUtil.buildAllResponseJobProgress(jobStatusEntityList);
	}

	@Override
	public JobDetailsResponse connectionJobDetails(String connectionId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.connectionJobDetails()");
		JobDetailsResponse jobDetailsResponse = new JobDetailsResponse();
		List<JobResponse> jobResponseList = new ArrayList<JobResponse>();
		List<JobEntity> jobsEntityList = customEntityRepository.findAllJobName(connectionId);
		Long totalRecordCount = customEntityRepository.countTotalRecord(connectionId);
		if (jobsEntityList != null && jobsEntityList.size() != 0) {
			for (int i = 0; i < jobsEntityList.size(); i++) {
				JobResponse jobResponse = new JobResponse();
				jobResponse.setJobName(jobsEntityList.get(i).getJobName());
				jobResponseList.add(jobResponse);
			}
		}
		jobDetailsResponse.setJobResponseList(jobResponseList);
		jobDetailsResponse.setTotalRecordCount(totalRecordCount);
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.connectionJobDetails()");
		return jobDetailsResponse;
	}

	@Override
	public boolean saveOrUpdatescheduledJob(ScheduledJobRequest scheduledJobRequest) throws DataSyncServiceException {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.saveOrUpdatescheduledJob()");
		boolean success = false;
		try {
			ScheduledJobEntity scheduledJobEntity = scheduledjobRepository.findByJobId(scheduledJobRequest.getJobId());
			if (scheduledJobEntity != null) {
				scheduledJobEntity.setUpdatedBy(scheduledJobRequest.getUpdatedBy());
				scheduledJobEntity.setCronExpression(scheduledJobRequest.getCronExpression());
			} else {
				scheduledJobEntity = new ScheduledJobEntity();
				scheduledJobEntity.setCronExpression(scheduledJobRequest.getCronExpression());
				scheduledJobEntity.setCreatedBy(scheduledJobRequest.getCreatedBy());
			}
			if (!scheduledJobRequest.getCronExpression().isEmpty()) {
				final String cronExpression = scheduledJobEntity.getCronExpression();
				final CronSequenceGenerator generator = new CronSequenceGenerator(cronExpression);
				scheduledJobEntity.setJobStartDate(generator.next(CommonUtil.getCurrentDateOnPSTTimeZone()));
			} else {
				scheduledJobEntity.setJobStartDate(scheduledJobRequest.getJobStartDate());
			}
			scheduledJobEntity.setJobId(scheduledJobRequest.getJobId());
			scheduledJobEntity.setJobEndDate(scheduledJobRequest.getJobEndDate());
			ScheduledJobEntity map = scheduledjobRepository.save(scheduledJobEntity);
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.saveOrUpdatescheduledJob()");
			if (map.getId() != null)
				success = true;
		} catch (DataAccessException de) {
			logger.error(de.getMessage());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new DataSyncServiceException(ex.getMessage(), ex.getCause());
		}
		return success;
	}

	@Override
	public boolean scheduledJobs() {
		try {
			Date currentDate = CommonUtil.getCurrentDateOnPSTTimeZone();
			System.out.println(currentDate.toString()); // 2016/11/16 12:08:43
			List<ScheduledJobEntity> scheduledJobEntityList = scheduledjobRepository
					.findJobIdByJobStartDate(currentDate);
			// scheduledJobEntityList = scheduledjobRepository.findAll();
			System.out.println("getJobStartDate..." + scheduledJobEntityList.size());
			if (scheduledJobEntityList.isEmpty()) {
				System.out.println("No Jobs found to scheduled start");
			} else {
				for (int i = 0; i < scheduledJobEntityList.size(); i++) {
					ScheduledJobEntity scheduledJobEntity = scheduledJobEntityList.get(i);
					runJob(scheduledJobEntity.getJobId());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteScheduledJobs(String jobId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.deleteScheduledJobs()");

		Query query = new Query();
		query.addCriteria(Criteria.where("jobId").in(jobId));
		mongoTemplate.remove(query, ScheduledJobEntity.class);
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.deleteScheduledJobs()");
		return true;
	}

	@Override
	public ViewLogResponse[] viewLogs(String jobId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.viewLogs()");
		List<JobStatusEntity> jobStatusEntityList = customEntityRepository.findByJobId(jobId, true);
		List<ViewLogResponse> viewLogResponseList = new ArrayList<ViewLogResponse>();
		JobStatusEntity jobStatusEntity = new JobStatusEntity();
		if (jobStatusEntityList != null && jobStatusEntityList.size() != 0) {
			for (int i = 0; i < jobStatusEntityList.size(); i++) {
				ViewLogResponse viewLogResponse = new ViewLogResponse();
				jobStatusEntity = jobStatusEntityList.get(i);
				List<JobAuditEntity> jobAuditEntityList = customEntityRepository
						.findByStatusId(jobStatusEntity.getId());
				if (jobAuditEntityList != null && jobAuditEntityList.size() != 0) {
					viewLogResponse.setJobId(jobStatusEntity.getJobId());
					viewLogResponse.setJobStatusId(jobStatusEntity.getId());
					viewLogResponse.setTotalRecordCount(jobStatusEntity.getTotalRecordCount());
					viewLogResponse.setTotalRecordProcessed(jobStatusEntity.getTotalRecordProcessed());
					if (!jobStatusEntity.getStatus().isEmpty()
							&& jobStatusEntity.getStatus().equalsIgnoreCase(DataSyncUtilityMessage.failed)) {
						viewLogResponse.setStatus(jobStatusEntity.getStatus());
					} else {
						viewLogResponse.setStatus(DataSyncUtilityMessage.completedWithErrors);
					}
				} else {
					viewLogResponse.setJobId(jobStatusEntity.getJobId());
					viewLogResponse.setJobStatusId(jobStatusEntity.getId());
					viewLogResponse.setTotalRecordCount(jobStatusEntity.getTotalRecordCount());
					viewLogResponse.setTotalRecordProcessed(jobStatusEntity.getTotalRecordProcessed());
					viewLogResponse.setStatus(jobStatusEntity.getStatus());
				}
				viewLogResponseList.add(viewLogResponse);
			}
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.viewLogs()");
		return viewLogResponseList.toArray(new ViewLogResponse[viewLogResponseList.size()]);
	}

	@Override
	public ViewLogDetailsResponse[] viewLogDetails(String jobId, String jobStatusId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.viewLogDetails()");
		List<ViewLogDetailsResponse> viewLogDetailsResponseList = new ArrayList<ViewLogDetailsResponse>();
		List<JobAuditEntity> jobAuditEntityList = customEntityRepository.findByStatusId(jobStatusId);
		JobAuditEntity jobAuditEntity = new JobAuditEntity();
		if (jobAuditEntityList != null && jobAuditEntityList.size() != 0) {
			for (int i = 0; i < jobAuditEntityList.size(); i++) {
				ViewLogDetailsResponse viewLogDetailsResponse = new ViewLogDetailsResponse();
				jobAuditEntity = jobAuditEntityList.get(i);
				viewLogDetailsResponse.setJobId(jobAuditEntity.getJobId());
				viewLogDetailsResponse.setJobStatusId(jobAuditEntity.getJobStatusId());
				viewLogDetailsResponse.setSkipRecordCount(jobAuditEntity.getSkipRecordCount());
				viewLogDetailsResponse.setMessage(jobAuditEntity.getMessage());
				viewLogDetailsResponseList.add(viewLogDetailsResponse);
			}
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.viewLogDetails()");
		return viewLogDetailsResponseList.toArray(new ViewLogDetailsResponse[viewLogDetailsResponseList.size()]);
	}

	public ViewJobDifferenceResponse[] viewJobsDifference(JobRequest jobRequest) {
		List<ViewJobDifferenceResponse> viewJobDifferenceResponseList = new ArrayList<ViewJobDifferenceResponse>();
		String sortableColumn = "";
		String sortingType = "";
		List<JobEntity> jobEntityList = customEntityRepository.findAllJobs(jobRequest, 0, 10, sortableColumn,
				sortingType, true);
		JobEntity jobEntity = new JobEntity();
		if (jobEntityList != null && jobEntityList.size() != 0) {
			for (int i = 0; i < jobEntityList.size(); i++) {
				ViewJobDifferenceResponse viewJobDifferenceResponse = new ViewJobDifferenceResponse();
				List<ViewJobDifferenceDetailsResponse> viewJobDifferenceDetailsResponseList = new ArrayList<ViewJobDifferenceDetailsResponse>();
				jobEntity = jobEntityList.get(i);
				viewJobDifferenceResponse.setJobId(jobEntity.getJobId());
				viewJobDifferenceResponse.setJobName(jobEntity.getJobName());
				Map<String, String> mappingObject = new HashMap<String, String>();
				MetadataResponse metadataResponse = metadataService.getMetadata(jobEntity.getSourceConnection(),
						jobEntity.getSourceCollection());
				if (metadataResponse != null && metadataResponse.getCollectionMetadataResponseList().size() != 0) {
					for (int j = 0; j < metadataResponse.getCollectionMetadataResponseList().size(); j++) {
						ViewJobDifferenceDetailsResponse viewJobDifferenceDetailsResponse = new ViewJobDifferenceDetailsResponse();
						String fieldName = metadataResponse.getCollectionMetadataResponseList().get(j).getName();
						if (jobEntity.getMapping() != null && !jobEntity.getMapping().isEmpty()) {
							JSONObject jo = new JSONObject(jobEntity.getMapping());
							jo.keySet().stream().forEach((key) -> {
								mappingObject.put(key, jo.get(key).toString());
							});
							int count = 0;
							for (Map.Entry<String, String> entry : mappingObject.entrySet()) {
								if (entry.getValue()
										.equalsIgnoreCase(jobEntity.getSourceCollection() + "." + fieldName)) {
									count = 0;
									break;
								} else {
									count++;
								}
							}
							if (count != 0) {
								viewJobDifferenceDetailsResponse.setFieldName(fieldName);
								viewJobDifferenceDetailsResponse.setStatus(DataSyncUtilityMessage.newString);
							}
						}
						if (viewJobDifferenceDetailsResponse != null
								&& viewJobDifferenceDetailsResponse.getFieldName() != null) {
							viewJobDifferenceDetailsResponseList.add(viewJobDifferenceDetailsResponse);
						}
					}
				}
				if (jobEntity.getMapping() != null && !jobEntity.getMapping().isEmpty()) {
					JSONObject jo = new JSONObject(jobEntity.getMapping());
					jo.keySet().stream().forEach((key) -> {
						mappingObject.put(key, jo.get(key).toString());
					});
					for (Map.Entry<String, String> entry : mappingObject.entrySet()) {
						int count = 0;
						String fieldName = "";
						ViewJobDifferenceDetailsResponse viewJobDifferenceDetailsResponse = new ViewJobDifferenceDetailsResponse();
						if (metadataResponse != null
								&& metadataResponse.getCollectionMetadataResponseList().size() != 0) {
							for (int j = 0; j < metadataResponse.getCollectionMetadataResponseList().size(); j++) {
								fieldName = metadataResponse.getCollectionMetadataResponseList().get(j).getName();
								if ((jobEntity.getSourceCollection() + "."
										+ metadataResponse.getCollectionMetadataResponseList().get(j).getName())
												.equalsIgnoreCase(entry.getValue())) {
									count = 0;
									break;
								} else {
									count++;
								}
							}
							if (count != 0) {
								viewJobDifferenceDetailsResponse.setFieldName(fieldName);
								viewJobDifferenceDetailsResponse.setStatus(DataSyncUtilityMessage.deleted);
							}
						}
						if (viewJobDifferenceDetailsResponse != null
								&& viewJobDifferenceDetailsResponse.getFieldName() != null) {
							viewJobDifferenceDetailsResponseList.add(viewJobDifferenceDetailsResponse);
						}
					}
				}
				viewJobDifferenceResponse.setViewJobDifferenceDetailsResponseList(viewJobDifferenceDetailsResponseList);
				viewJobDifferenceResponseList.add(viewJobDifferenceResponse);
			}
		}
		return viewJobDifferenceResponseList
				.toArray(new ViewJobDifferenceResponse[viewJobDifferenceResponseList.size()]);
	}

	@Override
	public JobResponse stopJob(String jobId) throws DataSyncServiceException {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.saveOrUpdateJob()");
		JobEntity jobEntity = new JobEntity();
		if (jobId != null && !jobId.isEmpty()) {
			Optional<JobEntity> mapEntityOptional = jobRepository.findById(jobId);
			if (!mapEntityOptional.isPresent()) {
				return null;
			} else {
				jobEntity = mapEntityOptional.get();
			}
		}
		jobEntity.setStopped(true);
		JobEntity map = jobRepository.save(jobEntity);
		return JobsUtil.buildResponseAllMapsDetail(map);

	}

	public TempJobResponse saveJobsDifference(TempJobRequest tempJobRequest) {
		TempJobEntity tempJobEntity = new TempJobEntity();
		tempJobEntity.setArchived(false);
		tempJobEntity.setJobId(tempJobRequest.getJobId());
		tempJobEntity.setNewMapping(tempJobRequest.getNewMapping());
		tempJobEntity = tempJobRepository.save(tempJobEntity);
		return JobsUtil.buildTempjobResponse(tempJobEntity);
	}

	@Async
	public void bulkSync(String bulkJobId, List<String> collectionNames, String sourceConnection,
			String destinationConnection, String prefix, String sourceConnectionName, String destinationConnectionName,
			String scheduleDateTime) {
		if (collectionNames != null && collectionNames.size() != 0) {
			for (int i = 0; i < collectionNames.size(); i++) {
				JobResponse jobResponse = new JobResponse();
				String status = "";
				String message = "";
				try {
					MetadataResponse metadataResponse = metadataService.getMetadata(sourceConnection,
							collectionNames.get(i));
					MetadataResponse tableMetadataResponse = connectorFactory.getTableMetadata(sourceConnection,
							collectionNames.get(i));
					String tableName = reduceSizeTable(collectionNames.get(i), prefix);
					JSONObject json = new JSONObject();
					for (int j = 0; j < metadataResponse.getCollectionMetadataResponseList().size(); j++) {
						for (int k = 0; k < tableMetadataResponse.getCollectionMetadataResponseList().size(); k++) {
							if (metadataResponse.getCollectionMetadataResponseList().get(j).getName().equalsIgnoreCase(
									tableMetadataResponse.getCollectionMetadataResponseList().get(k).getName())) {
								json.put(tableMetadataResponse.getCollectionMetadataResponseList().get(k).getName(),
										collectionNames.get(i) + "." + metadataResponse
												.getCollectionMetadataResponseList().get(j).getName());
							}
						}
					}
					System.out.println("json:::::" + json.toString());
					JSONObject newJson = new JSONObject();
					int count = 00;
					for (int k = 0; k < tableMetadataResponse.getCollectionMetadataResponseList().size(); k++) {
						if (tableMetadataResponse.getCollectionMetadataResponseList().get(k).getName().length() > 30) {
							count++;
							String newColumn = reduceSize(
									tableMetadataResponse.getCollectionMetadataResponseList().get(k).getName(), count);
							newJson.put(newColumn, json
									.get(tableMetadataResponse.getCollectionMetadataResponseList().get(k).getName()));
							tableMetadataResponse.getCollectionMetadataResponseList().get(k).setName(newColumn);
						} else {
							newJson.put(tableMetadataResponse.getCollectionMetadataResponseList().get(k).getName(), json
									.get(tableMetadataResponse.getCollectionMetadataResponseList().get(k).getName()));
						}
					}
					TableMetaDataRequest tableMetaDataRequest = new TableMetaDataRequest();
					tableMetaDataRequest.setTableName(tableName);
					List<TableMetadata> tableMetadataList = new ArrayList<TableMetadata>();
					for (int k = 0; k < tableMetadataResponse.getCollectionMetadataResponseList().size(); k++) {
						TableMetadata tableMetadata = new TableMetadata();
						tableMetadata
								.setName(tableMetadataResponse.getCollectionMetadataResponseList().get(k).getName());
						tableMetadata
								.setType(tableMetadataResponse.getCollectionMetadataResponseList().get(k).getType());
						tableMetadataList.add(tableMetadata);
					}
					tableMetaDataRequest.setTableMetadataList(tableMetadataList);
					connectorFactory.createTable(destinationConnection, tableMetaDataRequest);
					System.out.println("newJson:::::" + newJson.toString());
					JobRequest jobRequest = new JobRequest();
					jobRequest.setDestinationCollection(tableName);
					jobRequest.setSourceCollection(collectionNames.get(i));
					jobRequest.setSourceConnection(sourceConnection);
					jobRequest.setDestinationConnection(destinationConnection);
					jobRequest.setMapping(newJson.toString());
					jobRequest.setJobName(prefix + "_" + collectionNames.get(i));
					jobRequest.setSourceConnectionName(sourceConnectionName);
					jobRequest.setDestinationConnectionName(destinationConnectionName);
					jobResponse = saveOrUpdateJob(jobRequest);
					status = "Success";
				} catch (Exception ex) {
					status = "Failed";
					message = ex.getMessage();
					jobResponse = null;
				}
				BulkJobDetailsEntity bulkJobDetailsEntity = new BulkJobDetailsEntity();
				bulkJobDetailsEntity.setBulkJobId(bulkJobId);
				bulkJobDetailsEntity.setJobId(jobResponse != null ? jobResponse.getJobId() : "");
				bulkJobDetailsEntity.setJobName(jobResponse != null ? jobResponse.getJobName() : "");
				bulkJobDetailsEntity.setCollectionName(collectionNames.get(i));
				bulkJobDetailsEntity.setStatus(status);
				bulkJobDetailsEntity.setMessage(message);
				bulkJobDetailsRepository.save(bulkJobDetailsEntity);
			}
		}
		List<BulkJobDetailsEntity> bulkJobDetailsEntityList = bulkJobDetailsRepository.findByBulkJobId(bulkJobId);
		scheduleBulkJob(bulkJobDetailsEntityList, scheduleDateTime);
		mailService.sendMailForBulkJob(bulkJobDetailsEntityList);
	}

	private String reduceSize(String columnName, int count) {
		if (count < 10) {
			columnName = columnName.substring(0, 28) + "0" + count;
		} else {
			columnName = columnName.substring(0, 28) + count;
		}
		return columnName;
	}

	private String reduceSizeTable(String tableName, String prefix) {
		if (tableName.length() > (29 - prefix.length())) {
			tableName = prefix + "_" + tableName.substring(0, 29 - prefix.length());
		} else {
			tableName = prefix + "_" + tableName;
		}
		return tableName;
	}

	@Override
	public BulkJobResponse saveOrUpdateBulkJob(BulkJobRequest bulkJobRequest) throws DataSyncServiceException {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.saveOrUpdateBulkJob()");
		BulkJobResponse bulkJobResponse = new BulkJobResponse();
		try {
			List<BulkJobEntity> bulkJobEntityJobNameList = bulkJobRepository.findByName(bulkJobRequest.getName());
			if (bulkJobEntityJobNameList != null && bulkJobEntityJobNameList.size() != 0) {
				if (bulkJobEntityJobNameList.get(0).getName().equals(bulkJobRequest.getName())) {
					throw new DataSyncServiceException("Name Already Exists");
				}
			}
			List<BulkJobEntity> bulkJobEntityPrefixList = bulkJobRepository.findByPrefix(bulkJobRequest.getPrefix());
			if (bulkJobEntityPrefixList != null && bulkJobEntityPrefixList.size() != 0) {
				if (bulkJobEntityPrefixList.get(0).getPrefix().equals(bulkJobRequest.getPrefix())) {
					throw new DataSyncServiceException("Prefix Already Exists");
				}
			}
			BulkJobEntity bulkJobEntity = new BulkJobEntity();
			if (bulkJobRequest.getId() != null && !bulkJobRequest.getId().isEmpty()) {
				Optional<BulkJobEntity> bulkJobEntityOptional = bulkJobRepository.findById(bulkJobRequest.getId());
				if (!bulkJobEntityOptional.isPresent()) {
					bulkJobResponse = null;
				} else {
					bulkJobEntity = bulkJobEntityOptional.get();
				}
			}
			if (bulkJobRequest.getId() == null || bulkJobRequest.getId().isEmpty()) {
				bulkJobEntity.setCreatedBy(bulkJobRequest.getCreatedBy());
				bulkJobEntity.setCreatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			}
			bulkJobEntity.setName(bulkJobRequest.getName());
			bulkJobEntity.setDestinationConnectionName(bulkJobRequest.getDestinationConnectionName());
			bulkJobEntity.setDestinationConnection(bulkJobRequest.getDestinationConnection());
			bulkJobEntity.setSourceConnectionName(bulkJobRequest.getSourceConnectionName());
			bulkJobEntity.setSourceConnection(bulkJobRequest.getSourceConnection());
			bulkJobEntity.setCollectionNames(bulkJobRequest.getCollectionNames());
			bulkJobEntity.setPrefix(bulkJobRequest.getPrefix());
			bulkJobEntity.setUpdatedBy(bulkJobRequest.getUpdatedBy());
			bulkJobEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			BulkJobEntity map = bulkJobRepository.save(bulkJobEntity);
			bulkJobResponse = JobsUtil.buildResponseBulkJobDetail(map);
		} catch (DataAccessException de) {
			logger.error(de.getMessage());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new DataSyncServiceException(ex.getMessage(), ex.getCause());
		}
		if (bulkJobResponse != null) {
			bulkSync(bulkJobResponse.getId(), Arrays.asList(bulkJobResponse.getCollectionNames().split(",")),
					bulkJobRequest.getSourceConnection(), bulkJobRequest.getDestinationConnection(),
					bulkJobRequest.getPrefix(), bulkJobRequest.getSourceConnectionName(),
					bulkJobRequest.getDestinationConnectionName(), bulkJobRequest.getScheduleDateTime());
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.saveOrUpdateBulkJob()");
		return bulkJobResponse;
	}

	@Async
	public void scheduleBulkJob(List<BulkJobDetailsEntity> bulkJobDetailsEntityList, String scheduleDateTime) {
		if (bulkJobDetailsEntityList != null && bulkJobDetailsEntityList.size() != 0) {
			for (int i = 0; i < bulkJobDetailsEntityList.size(); i++) {
				if (bulkJobDetailsEntityList.get(i).getStatus().equalsIgnoreCase("Success")) {
					ScheduledJobRequest scheduledJobRequest = new ScheduledJobRequest();
					scheduledJobRequest.setJobId(bulkJobDetailsEntityList.get(i).getJobId());
					SimpleDateFormat sm = new SimpleDateFormat(DataSyncUtilityMessage.TIME_STAMP);
					Date dat = new Date();
					try {
						dat = sm.parse(scheduleDateTime);
						dat = CommonUtil.getDateOnTimeZone(dat, DataSyncUtilityMessage.DATE_FORMAT.getMessage(),
								DataSyncUtilityMessage.PST_TIME_ZONE.getMessage());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String croneExp = "";
					scheduledJobRequest.setJobStartDate(dat);
					scheduledJobRequest.setCronExpression(croneExp);
					saveOrUpdatescheduledJob(scheduledJobRequest);
				}
			}
		}
	}
}
