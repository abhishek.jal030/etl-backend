package com.datasync.serviceEngine.Impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.olingo.odata2.api.client.batch.BatchChangeSet;
import org.apache.olingo.odata2.api.client.batch.BatchChangeSetPart;
import org.apache.olingo.odata2.api.client.batch.BatchPart;
import org.apache.olingo.odata2.api.client.batch.BatchSingleResponse;
import org.apache.olingo.odata2.api.edm.Edm;
import org.apache.olingo.odata2.api.ep.EntityProvider;
import org.apache.olingo.odata2.api.ep.EntityProviderException;
import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.ep.feed.ODataFeed;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.datasync.entity.BulkJobDetailsEntity;
import com.datasync.entity.ConnectionEntity;
import com.datasync.entity.ConnectorTypeEntity;
import com.datasync.entity.JobAuditEntity;
import com.datasync.entity.JobEntity;
import com.datasync.entity.JobStatusEntity;
import com.datasync.entity.ScheduledJobEntity;
import com.datasync.entity.TempJobEntity;
import com.datasync.exception.ResourceNotFoundException;
import com.datasync.mail.MailService;
import com.datasync.repository.BulkJobDetailsRepository;
import com.datasync.repository.ConnectionEntityRepository;

import com.datasync.repository.ConnectorTypeRepository;
import com.datasync.repository.CustomEntityRepository;
import com.datasync.repository.JobAuditRepository;
import com.datasync.repository.JobRepository;
import com.datasync.repository.JobStatusRepository;
import com.datasync.repository.ScheduledjobRepository;
import com.datasync.repository.TempJobRepository;
import com.datasync.request.ConnectionTestRequest;
import com.datasync.request.ConnectorTypeRequest;
import com.datasync.request.MailRequest;
import com.datasync.request.MappingRequest;
import com.datasync.request.TableMetaDataRequest;
import com.datasync.request.TestFilterRequest;
import com.datasync.response.BulkJobStatusResponse;
import com.datasync.response.ConnectionTestResponse;
import com.datasync.response.ConnectorTypeResponse;
import com.datasync.response.DataPreviewResponse;
import com.datasync.response.MetadataResponse;
import com.datasync.response.MetadataResponseDetails;
import com.datasync.response.TestFilterQueryResponse;
import com.datasync.serviceEngine.ConnectorFactory;
import com.datasync.serviceEngine.apiConnector.ApiConnectorService;
import com.datasync.serviceEngine.odataConnector.HttpClientService;
import com.datasync.serviceEngine.odataConnector.MetadataService;
import com.datasync.serviceEngine.odataConnector.OdataEdmService;
import com.datasync.serviceEngine.rdbmsConnectors.OracleConnector.MySqlService;
import com.datasync.serviceEngine.rdbmsConnectors.OracleConnector.OracleService;
import com.datasync.util.ConnectorUtil;
import com.datasync.util.CommonUtil;
import com.datasync.util.DataSyncUtilityMessage;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ConnectorFactoryImpl implements ConnectorFactory {

	private static Logger logger = LogManager.getLogger(ConnectorFactoryImpl.class);

	@Autowired
	private ConnectionEntityRepository connectionEntityRepository;

	@Autowired
	MetadataService metadataService;

	@Autowired
	HttpClientService httpClientService;

	@Autowired
	OracleService oracleService;

	@Autowired
	MySqlService mySqlService;

	@Autowired
	ApiConnectorService apiConnectorService;

	@Autowired
	OdataEdmService odataEdmService;

	@Autowired
	ConnectorTypeRepository connectorTypeRepository;

	@Autowired
	JobStatusRepository jobStatusRepository;

	@Autowired
	JobAuditRepository jobAuditRepository;

	@Autowired
	JobRepository jobRepository;

	@Autowired
	MailService mailService;

	@Autowired
	ScheduledjobRepository scheduledjobRepository;

	@Autowired
	TempJobRepository tempJobRepository;

	@Autowired
	BulkJobDetailsRepository bulkJobDetailsRepository;

	@Autowired
	CustomEntityRepository customEntityRepository;

	private String boundary = "batch_" + UUID.randomUUID().toString();
	/*
	 * @Value("${destinationApiUrl}") String destinationApiUrl;
	 */

	private HttpClient m_httpClient = null;

	@Override
	public ConnectionTestResponse testConnection(ConnectionTestRequest connectionTestRequest) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.testConnection()");
		if (connectionTestRequest.getConnectionType().equalsIgnoreCase(DataSyncUtilityMessage.odata)) {
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.testConnection()");
			return httpClientService.testConnection(connectionTestRequest);

		}
		if (connectionTestRequest.getConnectionType().equalsIgnoreCase(DataSyncUtilityMessage.oracle)) {
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.testConnection()");
			return oracleService.testConnection(connectionTestRequest);

		}
		if (connectionTestRequest.getConnectionType().equalsIgnoreCase(DataSyncUtilityMessage.mysql)) {
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.testConnection()");
			return mySqlService.testConnection(connectionTestRequest);

		}
		if (connectionTestRequest.getConnectionType().equalsIgnoreCase(DataSyncUtilityMessage.api)
				|| connectionTestRequest.getConnectionType().equalsIgnoreCase(DataSyncUtilityMessage.datahug)) {
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.testConnection()");
			return httpClientService.testConnection(connectionTestRequest);

		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.testConnection()");
		return null;
	}

	@Override
	public MetadataResponseDetails getAllEntitiesName(String connectionId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.getAllEntitiesName()");
		ConnectionEntity connectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository.findById(connectionId);
		if (connectionEntityOptional.isPresent()) {
			connectionEntity = connectionEntityOptional.get();
		}
		if (connectionEntity != null && connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.oracle)) {
			try {
				return oracleService.getEntitiesName(connectionEntity);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		if (connectionEntity != null && connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.mysql)) {
			try {
				return mySqlService.getEntitiesName(connectionEntity);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		if (connectionEntity != null && connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.odata)) {
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.getAllEntitiesName()");
			return metadataService.getEntityNames(connectionEntity.getConnectionId());
		}
		if (connectionEntity != null && (connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.api)
				|| connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.datahug))) {
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.getAllEntitiesName()");
			return apiConnectorService.getEntityNames(connectionEntity.getConnectionId(), connectionEntity);
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.getAllEntitiesName()");
		return null;
	}

	public MetadataResponse getMetadata(String connectionId, String collectionName, String Name) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.getMetadata()");
		ConnectionEntity connectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository.findById(connectionId);
		if (connectionEntityOptional.isPresent()) {
			connectionEntity = connectionEntityOptional.get();
		}
		if (connectionEntity != null && connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.odata)) {
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.getMetadata()");
			return metadataService.getMetadata(connectionEntity.getConnectionId(), collectionName);
		}
		if (connectionEntity != null && connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.oracle)) {
			try {
				logger.debug("Exiting to com.datasync.serviceEngine.Impl.getMetadata()");
				return oracleService.getMetaData(collectionName, connectionEntity, Name);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		if (connectionEntity != null && connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.mysql)) {
			try {
				logger.debug("Exiting to com.datasync.serviceEngine.Impl.getMetadata()");
				return mySqlService.getMetaData(collectionName, connectionEntity, Name);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		if (connectionEntity != null && (connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.api)
				|| connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.datahug))) {
			try {
				logger.debug("Exiting to com.datasync.serviceEngine.Impl.getMetadata()");
				return apiConnectorService.getMetaData(collectionName, connectionEntity);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.getMetadata()");
		return null;
	}

	@Override
	public DataPreviewResponse getDataForPreview(String connectionId, String collectionName, String collectionType,
			String previewFilter, Integer startRecord, Integer recordCount) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.getDataForPreview()");
		ConnectionEntity connectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository.findById(connectionId);
		if (connectionEntityOptional.isPresent()) {
			connectionEntity = connectionEntityOptional.get();
		}
		if (connectionEntity != null && connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.oracle)) {
			try {
				logger.debug("Exiting to com.datasync.serviceEngine.Impl.getDataForPreview()");
				return oracleService.getDataForPreview(connectionEntity, collectionName, collectionType, previewFilter,
						startRecord, recordCount);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		if (connectionEntity != null && connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.mysql)) {
			try {
				logger.debug("Exiting to com.datasync.serviceEngine.Impl.getDataForPreview()");
				return mySqlService.getDataForPreview(connectionEntity, collectionName, collectionType, previewFilter,
						startRecord, recordCount);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		if (connectionEntity != null && connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.odata)) {
			return metadataService.getDataForPreview(connectionEntity, collectionName, collectionType, previewFilter,
					startRecord, recordCount);

		}
		if (connectionEntity != null && (connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.api)
				|| connectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.datahug))) {
			try {
				logger.debug("Exiting to com.datasync.serviceEngine.Impl.getMetadata()");
				return apiConnectorService.getDataForPreview(collectionName, connectionEntity);
			} catch (Exception e) {
				logger.error(e);
			}
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.getDataForPreview()");
		return null;
	}

	public boolean refreshMetadata(String connectionId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.refreshMetadata()");
		boolean status = false;
		ConnectionEntity connectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository.findById(connectionId);
		if (connectionEntityOptional.isPresent()) {
			connectionEntity = connectionEntityOptional.get();
		}
		status = metadataService.refreshMetadata(connectionEntity);
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.refreshMetadata()");
		return status;
	}

	public void refreshCollectionCounts(String connectionId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.refreshCollectionCounts()");
		ConnectionEntity connectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository.findById(connectionId);
		if (connectionEntityOptional.isPresent()) {
			connectionEntity = connectionEntityOptional.get();
		}
		metadataService.refreshCollectionCounts(connectionEntity);
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.refreshCollectionCounts()");
	}

	@Override
	public MetadataResponse getTableMetadata(String connectionId, String collectionName) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.getTableMetadata()");
		ConnectionEntity connectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository.findById(connectionId);
		if (connectionEntityOptional.isPresent()) {
			connectionEntity = connectionEntityOptional.get();
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.getTableMetadata()");
		return metadataService.getTableMetadata(connectionEntity, collectionName);

	}

	public MetadataResponse getUpdatedTableMetadata(String jobId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.getTableMetadata()");
		JobEntity jobEntity = new JobEntity();
		Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobId);
		if (jobEntityOptional.isPresent()) {
			jobEntity = jobEntityOptional.get();
		} else {
			return null;
		}
		ConnectionEntity connectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository
				.findById(jobEntity.getSourceConnection());
		if (connectionEntityOptional.isPresent()) {
			connectionEntity = connectionEntityOptional.get();
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.getTableMetadata()");
		return metadataService.getUpdatedTableMetadata(connectionEntity, jobEntity);
	}

	@Override
	public boolean createTable(String connectionId, TableMetaDataRequest tableMetaDataRequest) throws Exception {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.createTable()");
		ConnectionEntity connectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository.findById(connectionId);
		if (connectionEntityOptional.isPresent()) {
			connectionEntity = connectionEntityOptional.get();
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.createTable()");
		return oracleService.createTable(connectionEntity, tableMetaDataRequest);

	}

	@Override
	public boolean alterTable(String connectionId, TableMetaDataRequest tableMetaDataRequest) throws Exception {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.alterTable()");
		ConnectionEntity connectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository.findById(connectionId);
		if (connectionEntityOptional.isPresent()) {
			connectionEntity = connectionEntityOptional.get();
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.alterTable()");
		return oracleService.alterTable(connectionEntity, tableMetaDataRequest);

	}

	@Async
	public void runJob(JobEntity jobEntity, Optional<JobStatusEntity> jobStatusEntityOptional) throws Exception {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.runJob()");
		int startRecord = 0;
		JobStatusEntity jobStatusEntity = new JobStatusEntity();
		JobAuditEntity jobAuditEntity = new JobAuditEntity();
		Date startDate = new Date();
		if (jobStatusEntityOptional != null) {
			startRecord = jobStatusEntityOptional.get().getTotalRecordProcessed().intValue();
			jobStatusEntity = jobStatusEntityOptional.get();
		} else {
			jobStatusEntity.setJobId(jobEntity.getJobId());
			jobStatusEntity.setTotalRecordCount(0L);
			jobStatusEntity.setTotalRecordProcessed(0L);
			startDate = CommonUtil.getCurrentDateOnPSTTimeZone();
			jobStatusEntity.setStartDate(startDate);
			jobStatusEntity.setStatus(DataSyncUtilityMessage.initiated);
			jobEntity.setLastRun(startDate);
			jobEntity.setStatus(DataSyncUtilityMessage.initiated);
			jobRepository.save(jobEntity);
			jobStatusEntity = jobStatusRepository.save(jobStatusEntity);
			jobStatusEntityOptional = Optional.of(jobStatusEntity);
		}
		try {
			ConnectionEntity sourceConnectionEntity = new ConnectionEntity();
			Optional<ConnectionEntity> sourceConnectionEntityOptional = connectionEntityRepository
					.findById(jobEntity.getSourceConnection());
			if (sourceConnectionEntityOptional.isPresent()) {
				sourceConnectionEntity = sourceConnectionEntityOptional.get();
			}
			ConnectionEntity destinationConnectionEntity = new ConnectionEntity();
			Optional<ConnectionEntity> destinationConnectionEntityOptional = connectionEntityRepository
					.findById(jobEntity.getDestinationConnection());
			if (destinationConnectionEntityOptional.isPresent()) {
				destinationConnectionEntity = destinationConnectionEntityOptional.get();
			}
			if (sourceConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.odata)
					&& destinationConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.oracle)) {
				ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
				connectionTestRequest.setServiceName(
						destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.serviceName));
				connectionTestRequest
						.setPort(destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
				connectionTestRequest
						.setHost(destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
				connectionTestRequest.setUserName(
						destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
				connectionTestRequest.setPassword(
						destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
				connectionTestRequest.setConnectionType(destinationConnectionEntity.getType());
				Connection conn = oracleService.getConnection(connectionTestRequest);
				String entityName = jobEntity.getSourceCollection();
				String serviceUrl = sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.url);
				String totalRecordCountQuery = "";
				if (jobEntity.getJobFilter() != null && !jobEntity.getJobFilter().isEmpty()) {
					totalRecordCountQuery = totalRecordCountQuery + "?$filter="
							+ odataEdmService.getFilterQuery(jobEntity.getJobFilter());
				}
				if (jobStatusEntity.getTotalRecordCount() == 0L) {
					Long totalRecordCount = odataEdmService.getTotalRecordCount(sourceConnectionEntity, entityName,
							totalRecordCountQuery);
					jobStatusEntity.setTotalRecordCount(totalRecordCount);
				}
				MappingRequest mappingRequest = oracleService.createInsertSQL(jobEntity.getDestinationCollection(),
						jobEntity.getMapping(), entityName);
				jobStatusEntity = jobStatusRepository.save(jobStatusEntity);
				jobStatusEntityOptional = Optional.of(jobStatusEntity);
				MetadataResponse metadataResponse = metadataService.getMetadata(jobEntity.getSourceConnection(),
						jobEntity.getSourceCollection());
				boolean isTerminated = false;
				while (isTerminated == false) {
					Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
					if (jobEntityOptional.isPresent()) {
						if (jobEntityOptional.get().isStopped()) {
							throw new ResourceNotFoundException("Job has been stopped");
						}
					}
					String queryOptions = "?$inlinecount=allpages&$skip=" + startRecord + "&$top=100";
					if (!totalRecordCountQuery.isEmpty()) {
						queryOptions = queryOptions + "&"
								+ totalRecordCountQuery.substring(1, totalRecordCountQuery.length());
					}
					System.out.println("queryOptions :: " + queryOptions);
					ODataFeed feed = odataEdmService.readFeed(sourceConnectionEntity.getConnectionId(), serviceUrl,
							DataSyncUtilityMessage.APPLICATION_JSON, entityName, queryOptions,
							sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName),
							sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
					List<Map<String, Object>> entries = new ArrayList<Map<String, Object>>();
					for (ODataEntry entry : feed.getEntries()) {
						entries.add(odataEdmService.mapEntry(entry));
					}
					if (feed.getEntries().size() < 100) {
						isTerminated = true;
					}
					PreparedStatement ps = conn.prepareStatement(mappingRequest.getInsertSQL());
					for (int i = 0; i < entries.size(); i++) {
						Map<String, Object> singleEntry = entries.get(i);
						int j = 1;
						for (Map.Entry<String, String> entry : mappingRequest.getMapping().entrySet()) {
							if (singleEntry.get(entry.getValue().replace(entityName + ".", "")) != null) {
								if (entry.getValue().contains(entityName + ".")) {
									if (metadataResponse != null
											&& metadataResponse.getCollectionMetadataResponseList() != null
											&& metadataResponse.getCollectionMetadataResponseList().size() != 0) {
										for (int k = 0; k < metadataResponse.getCollectionMetadataResponseList()
												.size(); k++) {
											if (metadataResponse.getCollectionMetadataResponseList().get(k).getName()
													.equalsIgnoreCase(entry.getValue().replace(entityName + ".", ""))) {
												if (!metadataResponse.getCollectionMetadataResponseList().get(k)
														.getType().equalsIgnoreCase("String")
														&& !metadataResponse.getCollectionMetadataResponseList().get(k)
																.getType().equalsIgnoreCase("DateTimeOffset")
														&& !metadataResponse.getCollectionMetadataResponseList().get(k)
																.getType().equalsIgnoreCase("DateTime")
														&& !metadataResponse.getCollectionMetadataResponseList().get(k)
																.getType().equalsIgnoreCase("Decimal")
														&& !metadataResponse.getCollectionMetadataResponseList().get(k)
																.getType().equalsIgnoreCase("Boolean")
														&& !metadataResponse.getCollectionMetadataResponseList().get(k)
																.getType().equalsIgnoreCase("Int64")
														&& !metadataResponse.getCollectionMetadataResponseList().get(k)
																.getType().equalsIgnoreCase("Binary")
														&& !metadataResponse.getCollectionMetadataResponseList().get(k)
																.getType().equalsIgnoreCase("Double")
														&& !metadataResponse.getCollectionMetadataResponseList().get(k)
																.getType().equalsIgnoreCase("Single")
														&& !metadataResponse.getCollectionMetadataResponseList().get(k)
																.getType().equalsIgnoreCase("Int32")) {
													ps.setObject(j,
															singleEntry
																	.get(entry.getValue().replace(entityName + ".", ""))
																	.toString());
												} else {
													ps.setObject(j, singleEntry
															.get(entry.getValue().replace(entityName + ".", "")));
												}
											}
										}
									}
								} else {
									ps.setObject(j, entry.getValue());
								}
							} else {

								if (entry.getValue().equalsIgnoreCase("SYSDATE")) {
									ps.setObject(j, new java.sql.Timestamp(new java.util.Date().getTime()));
								} else {
									ps.setString(j, null);
								}
							}
							j++;
						}
						ps.addBatch();
					}
					ps.executeBatch();
					startRecord = startRecord + feed.getEntries().size();
					if (!isTerminated) {

						jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord));
						jobStatusEntityOptional.get().setStatus(DataSyncUtilityMessage.inProcess);
						jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
						jobEntityOptional.get().setStatus(DataSyncUtilityMessage.inProcess);
						jobRepository.save(jobEntityOptional.get());
					} else {
						jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord));
						jobStatusEntityOptional.get().setStatus(DataSyncUtilityMessage.completed);
					}
					jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntityOptional.get()));
				}
				conn.close();
			}
			if (sourceConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.mysql)
					&& destinationConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.api)) {
				ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
				connectionTestRequest.setConnectionType(sourceConnectionEntity.getType());
				connectionTestRequest.setSchemaName(
						sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.schemaName));
				connectionTestRequest
						.setPort(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
				connectionTestRequest
						.setHost(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
				connectionTestRequest
						.setUserName(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
				connectionTestRequest
						.setPassword(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
				Connection conn = mySqlService.getConnection(connectionTestRequest);
				Statement stmt = conn.createStatement();
				ResultSet rs = null;

				String sourceCollectionName = jobEntity.getSourceCollection();
				if (jobEntity.getSourceCollectionType().equalsIgnoreCase(DataSyncUtilityMessage.table)) {
					if (jobEntity.getJobFilter() != null) {
						String filterQuery = "";
						filterQuery = " where "
								+ mySqlService.getPreviewFilterQuery(jobEntity.getJobFilter(), connectionTestRequest);
						String query = "SELECT * FROM " + jobEntity.getSourceCollection() + filterQuery;
						rs = stmt.executeQuery(query);
					} else {
						String query = "select * from " + jobEntity.getSourceCollection();
						rs = stmt.executeQuery(query);
					}

				}
				if (jobEntity.getSourceCollectionType().equalsIgnoreCase(DataSyncUtilityMessage.procedure)) {
					MetadataResponse metadataResponse = getMetadata(jobEntity.getSourceConnection(),
							jobEntity.getSourceCollection(), "procedure");
					rs = mySqlService.createProcedureQuery(metadataResponse, conn, jobEntity.getJobFilter());

				}

				List<Map<String, Object>> requestBody = httpClientService.createAPIRequest(rs, jobEntity.getMapping(),
						sourceCollectionName);
				ObjectMapper mapper = new ObjectMapper();

//				System.out.println("Body/.." + requestBody);
				String apiUrl = destinationConnectionEntity.getConnectionConfig().get("url")
						+ jobEntity.getDestinationCollection();

				System.out.println("Destination API URL.." + apiUrl);
				URL obj = new URL(apiUrl);
				HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
				postConnection.setRequestMethod("POST");
				postConnection.setRequestProperty("Content-Type", "application/json");
				postConnection.setDoOutput(true);
				OutputStream os = postConnection.getOutputStream();
				os.write(mapper.writeValueAsString(requestBody).toString().getBytes());
				os.flush();
				os.close();
				int responseCode = postConnection.getResponseCode();
				System.out.println("POST Response Code :  " + responseCode);
				System.out.println("POST Response Message : " + postConnection.getResponseMessage());

			}
			if (sourceConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.oracle)
					&& destinationConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.odata)) {
				ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
				connectionTestRequest.setServiceName(
						sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.serviceName));
				connectionTestRequest
						.setPort(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
				connectionTestRequest
						.setHost(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
				connectionTestRequest
						.setUserName(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
				connectionTestRequest
						.setPassword(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
				connectionTestRequest.setConnectionType(sourceConnectionEntity.getType());
				Connection conn = oracleService.getConnection(connectionTestRequest);
				Statement stmt = conn.createStatement();
				ResultSet rs = null;
				String sourceCollectionName = jobEntity.getSourceCollection();
				if (jobEntity.getJobFilter() != null) {
					String filterQuery = "";
					filterQuery = " where "
							+ mySqlService.getPreviewFilterQuery(jobEntity.getJobFilter(), connectionTestRequest);
					String query = "SELECT * FROM " + jobEntity.getSourceCollection() + filterQuery;
					rs = stmt.executeQuery(query);
				} else {
					String query = "select * from " + jobEntity.getSourceCollection();
					rs = stmt.executeQuery(query);
				}

				List<Map<String, Object>> requestBody = httpClientService.createODATARequest(rs, jobEntity.getMapping(),
						sourceCollectionName);
				if (jobStatusEntity.getTotalRecordCount() == 0L) {
					Long totalRecordCount = Long.valueOf(requestBody.size());
					jobStatusEntity.setTotalRecordCount(totalRecordCount);
				}
				jobStatusEntity = jobStatusRepository.save(jobStatusEntity);
				jobStatusEntityOptional = Optional.of(jobStatusEntity);
				boolean isTerminated = false;
				while (isTerminated == false) {
					Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
					if (jobEntityOptional.isPresent()) {
						if (jobEntityOptional.get().isStopped()) {
							throw new ResourceNotFoundException("Job has been stopped");
						}
					}
					for (int i = 0; i < requestBody.size(); i++) {
						ObjectMapper mapper = new ObjectMapper();
						String url = destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.url);
						String serviceUrl = url;
						System.out.println("Destination ODATE API URL.." + serviceUrl);
						List<BatchPart> batchParts = new ArrayList<BatchPart>();
						BatchChangeSet changeSet = BatchChangeSet.newBuilder().build();
						String contentId = UUID.randomUUID().toString();
						Map<String, String> changeSetHeaders = new HashMap<String, String>();
						changeSetHeaders.put(DataSyncUtilityMessage.HTTP_HEADER_CONTENT_TYPE, "application/json");
						changeSetHeaders.put("Content-ID", contentId);
						changeSetHeaders.put("Accept", DataSyncUtilityMessage.APPLICATION_JSON);
						String uriTicket = new StringBuilder(jobEntity.getDestinationCollection()).toString();
						BatchChangeSetPart changeRequestTicket = BatchChangeSetPart
								.method(DataSyncUtilityMessage.HTTP_METHOD_POST).uri(uriTicket)
								.body(mapper.writeValueAsString(requestBody.get(i))).headers(changeSetHeaders)
								.contentId(contentId).build();
						changeSet.add(changeRequestTicket);
						batchParts.add(changeSet);
						InputStream body = EntityProvider.writeBatchRequest(batchParts, boundary);
						String payload = IOUtils.toString(body);
						logger.info("$batch request : ");
						logger.info(payload);
						HttpResponse batchResponse = executeBatchCall(serviceUrl, payload, destinationConnectionEntity);
						logger.info(
								"$batch response getStatusCode => " + batchResponse.getStatusLine().getStatusCode());
						for (Header h : batchResponse.getAllHeaders()) {
							logger.info(h.getName() + ":" + h.getValue());
						}
						String str = (String) payload.subSequence(payload.indexOf('{'), payload.indexOf('}'));
						InputStream responseBody = batchResponse.getEntity().getContent();
						String contentType = batchResponse.getFirstHeader(HttpHeaders.CONTENT_TYPE).getValue();
						String response = IOUtils.toString(responseBody);
						logger.info("$batch response :");
						logger.info(response);
						// Process the batch response to get the ticket key
						List<BatchSingleResponse> responses = EntityProvider
								.parseBatchResponse(IOUtils.toInputStream(response), contentType);
						for (BatchSingleResponse rsp : responses) {
							// Look for only created entries
							logger.info("Single Response status code => " + rsp.getStatusCode());
							if (Integer.parseInt(rsp.getStatusCode()) == 201) { // 201 - HttpStatus code created
								// Http POST call returns the link to the new resource via the location header
								// Format is https://<service_url>/<Collection>('key')
								/*
								 * String locationUrl = rsp.getHeader("location"); if
								 * (!StringUtils.isBlank(locationUrl)) { String ticketUUID =
								 * StringUtils.substringBetween(locationUrl, "'"); }
								 */
								startRecord = startRecord + 1;
							} else {
								jobAuditEntity = new JobAuditEntity();
								JSONObject obj = new JSONObject(rsp.getBody());
								JSONObject obj1 = obj.getJSONObject("error");
								JSONObject obj2 = obj1.getJSONObject("message");
								String value = obj2.getString("value");
								jobAuditEntity.setJobId(jobStatusEntityOptional.get().getJobId());
								jobAuditEntity.setJobStatusId(jobStatusEntityOptional.get().getId());
								jobAuditEntity.setStatus(DataSyncUtilityMessage.failed);
								jobAuditEntity.setSkipRecordCount(Long.valueOf(1));
								jobAuditEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
								jobAuditEntity.setMessage(value + ":- " + str + "}");
								jobAuditRepository.save(jobAuditEntity);
							}
						}
						if (i == requestBody.size() - 1) {
							isTerminated = true;
						}
					}
					if (!isTerminated) {
						jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord));
						jobStatusEntityOptional.get().setStatus(DataSyncUtilityMessage.inProcess);
						jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
						jobEntityOptional.get().setStatus(DataSyncUtilityMessage.inProcess);
						jobRepository.save(jobEntityOptional.get());
					} else {
						jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord));
						jobStatusEntityOptional.get().setStatus(DataSyncUtilityMessage.completed);
					}
					jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntityOptional.get()));
				}
			}
		} catch (

		ResourceNotFoundException ex) {
			Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
			jobEntityOptional.get().setStatus(DataSyncUtilityMessage.failed);
			jobRepository.save(jobEntityOptional.get());
			jobAuditEntity = new JobAuditEntity();
			jobAuditEntity.setJobId(jobEntity.getJobId());
			jobAuditEntity.setJobStatusId(jobStatusEntity.getId());
			jobAuditEntity.setStatus(DataSyncUtilityMessage.failed);
			jobAuditEntity.setSkipRecordCount(Long.valueOf(startRecord));
			jobAuditEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			jobAuditEntity.setMessage(ex.getMessage());
			logger.debug("Error : " + ex.getMessage());
			jobAuditRepository.save(jobAuditEntity);
			jobStatusEntity.setStatus(DataSyncUtilityMessage.failed);
			jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntity));
			saveNextScheduledJobDate(jobEntity.getJobId());
		} catch (Exception e) {
			jobAuditEntity = new JobAuditEntity();
			jobAuditEntity.setJobId(jobStatusEntityOptional.get().getJobId());
			jobAuditEntity.setJobStatusId(jobStatusEntityOptional.get().getId());
			jobAuditEntity.setStatus(DataSyncUtilityMessage.failed);
			jobAuditEntity.setSkipRecordCount(Long.valueOf(startRecord));
			jobAuditEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			jobAuditEntity.setMessage(e.getMessage());
			jobAuditRepository.save(jobAuditEntity);
			jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord) + 100);
			jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntity));
			logger.debug("Error : " + e.getMessage());
			runJob(jobEntity, jobStatusEntityOptional);
		}
		try {
			jobStatusEntityOptional.get().setEndDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			jobStatusEntity = jobStatusRepository.save(jobStatusEntityOptional.get());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Error : " + e.getMessage());
		}
		Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
		jobEntityOptional.get().setStatus(DataSyncUtilityMessage.completed);
		jobRepository.save(jobEntityOptional.get());

		saveNextScheduledJobDate(jobEntity.getJobId());
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.runJob()");
		sendMail(jobStatusEntity);
	}

	@Override
	public ConnectorTypeResponse[] getAllConnectorType() {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.getAllConnectorType()");
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.getAllConnectorType()");
		return ConnectorUtil.buildResponseConnectorTypeDetail(connectorTypeRepository.findAll());

	}

	@Override
	public boolean createConnectorType(ConnectorTypeRequest connectorTypeRequest) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.getTableMetadata()");
		if (connectorTypeRequest != null) {
			ConnectorTypeEntity connectorTypeEntity = new ConnectorTypeEntity();
			connectorTypeEntity.setConnectorId(connectorTypeRequest.getConnectorId());
			connectorTypeEntity.setConnectorType(connectorTypeRequest.getConnectorType());
			connectorTypeEntity.setImage(connectorTypeRequest.getImage());
			connectorTypeRepository.save(connectorTypeEntity);
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.createConnectorType()");
			return true;
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.createConnectorType()");
		return false;
	}

	@Async
	public void runJobIncremental(JobEntity jobEntity, Optional<JobStatusEntity> jobStatusEntityOptional)
			throws Exception {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.runJobIncremental()");
		int startRecord = 0;
		Date startDate = new Date();
		JobStatusEntity jobStatusEntity = new JobStatusEntity();
		JobAuditEntity jobAuditEntity = new JobAuditEntity();
		if (jobStatusEntityOptional != null) {
			startRecord = jobStatusEntityOptional.get().getTotalRecordProcessed().intValue();
			jobStatusEntity = jobStatusEntityOptional.get();
		} else {
			jobStatusEntity.setJobId(jobEntity.getJobId());
			jobStatusEntity.setTotalRecordCount(0L);
			jobStatusEntity.setTotalRecordProcessed(0L);
			startDate = CommonUtil.getCurrentDateOnPSTTimeZone();
			jobStatusEntity.setStartDate(startDate);
			jobStatusEntity.setStatus(DataSyncUtilityMessage.initiated);
			jobEntity.setLastRun(startDate);
			jobEntity.setStatus(DataSyncUtilityMessage.initiated);
			jobEntity = jobRepository.save(jobEntity);
			jobStatusEntity = jobStatusRepository.save(jobStatusEntity);
			jobStatusEntityOptional = Optional.of(jobStatusEntity);
		}
		try {
			ConnectionEntity sourceConnectionEntity = new ConnectionEntity();
			Optional<ConnectionEntity> sourceConnectionEntityOptional = connectionEntityRepository
					.findById(jobEntity.getSourceConnection());
			if (sourceConnectionEntityOptional.isPresent()) {
				sourceConnectionEntity = sourceConnectionEntityOptional.get();
			}
			ConnectionEntity destinationConnectionEntity = new ConnectionEntity();
			Optional<ConnectionEntity> destinationConnectionEntityOptional = connectionEntityRepository
					.findById(jobEntity.getDestinationConnection());
			if (destinationConnectionEntityOptional.isPresent()) {
				destinationConnectionEntity = destinationConnectionEntityOptional.get();
			}
			if (sourceConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.odata)
					&& destinationConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.oracle)) {
				ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
				connectionTestRequest.setServiceName(
						destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.serviceName));
				connectionTestRequest
						.setPort(destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
				connectionTestRequest
						.setHost(destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
				connectionTestRequest.setUserName(
						destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
				connectionTestRequest.setPassword(
						destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
				Connection conn = oracleService.getConnection(connectionTestRequest);
				String entityName = jobEntity.getSourceCollection();
				String totalRecordCountQuery = "";
				if (jobEntity.getIncrementalParameter() != null && !jobEntity.getIncrementalParameter().isEmpty()) {
					if (jobEntity.getIncrementalParameterValue() != null) {
						totalRecordCountQuery = "?$filter=" + jobEntity.getIncrementalParameter()
								+ "%20ge%20datetimeoffset'" + DataSyncUtilityMessage.C4CTimestampFormat
										.format(jobEntity.getIncrementalParameterValue())
								+ "'";
					}
				}
				if (jobEntity.getJobFilter() != null && !jobEntity.getJobFilter().isEmpty()) {
					if (!totalRecordCountQuery.isEmpty()) {
						totalRecordCountQuery = totalRecordCountQuery + "%20and%20"
								+ odataEdmService.getFilterQuery(jobEntity.getJobFilter());
					} else {
						totalRecordCountQuery = totalRecordCountQuery + "?$filter="
								+ odataEdmService.getFilterQuery(jobEntity.getJobFilter());
					}
				}
				System.out.println(totalRecordCountQuery);
				String serviceUrl = sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.url);
				if (jobStatusEntity.getTotalRecordCount() == 0L) {
					Long totalRecordCount = odataEdmService.getTotalRecordCount(sourceConnectionEntity, entityName,
							totalRecordCountQuery);
					jobStatusEntity.setTotalRecordCount(totalRecordCount);
				}
				MappingRequest mappingRequest = oracleService.createInsertSQL(jobEntity.getDestinationCollection(),
						jobEntity.getMapping(), entityName);
				jobStatusEntity = jobStatusRepository.save(jobStatusEntity);
				jobStatusEntityOptional = Optional.of(jobStatusEntity);
				MetadataResponse metadataResponse = metadataService.getMetadata(jobEntity.getSourceConnection(),
						jobEntity.getSourceCollection());
				boolean isTerminated = false;
				while (isTerminated == false) {
					Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
					if (jobEntityOptional.isPresent()) {
						if (jobEntityOptional.get().isStopped()) {
							throw new ResourceNotFoundException("Job has been stopped");
						}
					}
					String queryOptions = "?$inlinecount=allpages&$skip=" + startRecord + "&$top=100";
					if (!totalRecordCountQuery.isEmpty()) {
						queryOptions = queryOptions + "&"
								+ totalRecordCountQuery.substring(1, totalRecordCountQuery.length());
					}
					queryOptions = queryOptions + "&orderby=" + jobEntity.getIncrementalParameter();
					System.out.println("queryOptions :: " + queryOptions);
					ODataFeed feed = odataEdmService.readFeed(sourceConnectionEntity.getConnectionId(), serviceUrl,
							DataSyncUtilityMessage.APPLICATION_JSON, entityName, queryOptions,
							sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName),
							sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
					List<Map<String, Object>> entries = new ArrayList<Map<String, Object>>();
					for (ODataEntry entry : feed.getEntries()) {
						entries.add(odataEdmService.mapEntry(entry));
					}
					if (feed.getEntries().size() < 100) {
						isTerminated = true;
					}
					String objectIds = "";
					Map<String, String> objectId = new HashMap<String, String>();
					for (int i = 0; i < entries.size(); i++) {
						objectIds = objectIds + "'" + entries.get(i).get(DataSyncUtilityMessage.primaryKey) + "'"
								+ ", ";
					}
					if (!objectIds.isEmpty()) {
						objectIds = objectIds.substring(0, objectIds.length() - 2);
						String selectQuery = "SELECT " + mappingRequest.getObjectIdMapping() + " FROM "
								+ jobEntity.getDestinationCollection() + " WHERE " + mappingRequest.getObjectIdMapping()
								+ " in (" + objectIds + ")";
						PreparedStatement selectPS = conn.prepareStatement(selectQuery);
						ResultSet resultSet = selectPS.executeQuery();
						while (resultSet.next()) {
							String primaryKey = resultSet.getString(mappingRequest.getObjectIdMapping());
							System.out.println("primarKey :: " + primaryKey);
							objectId.put(primaryKey, primaryKey);
						}
						selectPS.close();
					}
					PreparedStatement insertps = conn.prepareStatement(mappingRequest.getInsertSQL());
					PreparedStatement updateps = conn.prepareStatement(mappingRequest.getUpdateSQL());
					for (int i = 0; i < entries.size(); i++) {
						Map<String, Object> singleEntry = entries.get(i);
						int j = 1;
						if (objectId.get(singleEntry.get(DataSyncUtilityMessage.primaryKey)) != null
								&& !objectId.get(singleEntry.get(DataSyncUtilityMessage.primaryKey)).isEmpty()) {
							for (Map.Entry<String, String> entry : mappingRequest.getMapping().entrySet()) {
								if (singleEntry.get(entry.getValue().replace(entityName + ".", "")) != null) {
									if (entry.getValue().contains(entityName + ".")) {
										if (metadataResponse != null
												&& metadataResponse.getCollectionMetadataResponseList() != null
												&& metadataResponse.getCollectionMetadataResponseList().size() != 0) {
											for (int k = 0; k < metadataResponse.getCollectionMetadataResponseList()
													.size(); k++) {
												if (metadataResponse.getCollectionMetadataResponseList().get(k)
														.getName().equalsIgnoreCase(
																entry.getValue().replace(entityName + ".", ""))) {
													if (!metadataResponse.getCollectionMetadataResponseList().get(k)
															.getType().equalsIgnoreCase("String")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("DateTimeOffset")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("DateTime")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Decimal")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Boolean")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Int64")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Binary")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Double")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Single")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Int32")) {
														updateps.setObject(j,
																singleEntry.get(
																		entry.getValue().replace(entityName + ".", ""))
																		.toString());
													} else {
														updateps.setObject(j, singleEntry
																.get(entry.getValue().replace(entityName + ".", "")));
													}
												}
											}
										}
									} else {
										updateps.setObject(j, entry.getValue());
									}
								} else {
									if (entry.getValue().equalsIgnoreCase("SYSDATE")) {
										updateps.setObject(j, new java.sql.Timestamp(new java.util.Date().getTime()));
									} else {
										updateps.setString(j, null);
									}
								}
								j++;
							}
							updateps.setObject(j, objectId.get(singleEntry.get(DataSyncUtilityMessage.primaryKey)));
							updateps.addBatch();
						} else {
							for (Map.Entry<String, String> entry : mappingRequest.getMapping().entrySet()) {
								if (singleEntry.get(entry.getValue().replace(entityName + ".", "")) != null) {
									if (entry.getValue().contains(entityName + ".")) {
										if (metadataResponse != null
												&& metadataResponse.getCollectionMetadataResponseList() != null
												&& metadataResponse.getCollectionMetadataResponseList().size() != 0) {
											for (int k = 0; k < metadataResponse.getCollectionMetadataResponseList()
													.size(); k++) {
												if (metadataResponse.getCollectionMetadataResponseList().get(k)
														.getName().equalsIgnoreCase(
																entry.getValue().replace(entityName + ".", ""))) {
													if (!metadataResponse.getCollectionMetadataResponseList().get(k)
															.getType().equalsIgnoreCase("String")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("DateTimeOffset")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("DateTime")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Decimal")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Boolean")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Int64")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Binary")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Double")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Single")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Int32")) {
														insertps.setObject(j,
																singleEntry.get(
																		entry.getValue().replace(entityName + ".", ""))
																		.toString());
													} else {
														insertps.setObject(j, singleEntry
																.get(entry.getValue().replace(entityName + ".", "")));
													}
												}
											}
										}
									} else {
										insertps.setObject(j, entry.getValue());
									}
								} else {
									if (entry.getValue().equalsIgnoreCase("SYSDATE")) {
										insertps.setObject(j, new java.sql.Timestamp(new java.util.Date().getTime()));
									} else {
										insertps.setString(j, null);
									}
								}
								j++;
							}
							insertps.addBatch();
						}
						if ((new java.util.Date(
								((java.sql.Date) singleEntry.get(jobEntity.getIncrementalParameter())).getTime()))
										.after(jobEntity.getIncrementalParameterValue())) {
							jobEntity.setIncrementalParameterValue(new java.util.Date(
									((java.sql.Date) singleEntry.get(jobEntity.getIncrementalParameter())).getTime()));
						}
						jobEntity = jobRepository.save(jobEntity);
					}
					updateps.executeBatch();
					insertps.executeBatch();
					startRecord = startRecord + feed.getEntries().size();
					if (!isTerminated) {
						jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord));
						jobStatusEntityOptional.get().setStatus(DataSyncUtilityMessage.inProcess);
						jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
						jobEntityOptional.get().setStatus(DataSyncUtilityMessage.inProcess);
						jobRepository.save(jobEntityOptional.get());
					} else {
						jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord));
						jobStatusEntityOptional.get().setStatus(DataSyncUtilityMessage.completed);
					}
					jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntityOptional.get()));
				}
				conn.close();
			}
			if (sourceConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.mysql)
					&& destinationConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.api)) {
				ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
				connectionTestRequest.setConnectionType(sourceConnectionEntity.getType());
				connectionTestRequest.setSchemaName(
						sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.schemaName));
				connectionTestRequest
						.setPort(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
				connectionTestRequest
						.setHost(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
				connectionTestRequest
						.setUserName(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
				connectionTestRequest
						.setPassword(sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
				Connection conn = mySqlService.getConnection(connectionTestRequest);
				Statement stmt = conn.createStatement();
				ResultSet rs = null;
				String sourceCollectionName = jobEntity.getSourceCollection();
				java.util.Date date = (java.util.Date) jobEntity.getIncrementalParameterValue();
				String dateStr = new java.text.SimpleDateFormat("yyyy-MM-dd H:m:s").format(date);
				System.out.println(dateStr);
				if (jobEntity.getSourceCollectionType().equalsIgnoreCase(DataSyncUtilityMessage.table)) {
					String query = "select * from " + jobEntity.getSourceCollection() + " where "
							+ jobEntity.getIncrementalParameter() + " >= '" + dateStr + "'";
					rs = stmt.executeQuery(query);
				}
				if (jobEntity.getSourceCollectionType().equalsIgnoreCase(DataSyncUtilityMessage.procedure)) {
					MetadataResponse metadataResponse = getMetadata(jobEntity.getSourceConnection(),
							jobEntity.getSourceCollection(), "procedure");
					rs = mySqlService.createProcedureQuery(metadataResponse, conn, jobEntity.getJobFilter());
				}
				List<Map<String, Object>> requestBody = httpClientService.createAPIRequest(rs, jobEntity.getMapping(),
						sourceCollectionName);
				ObjectMapper mapper = new ObjectMapper();
				// System.out.println("Body.." + mapper.writeValueAsString(requestBody));
				String apiUrl = destinationConnectionEntity.getConnectionConfig().get("url")
						+ jobEntity.getDestinationCollection();
				System.out.println("Destination API URL.." + apiUrl);
				URL obj = new URL(apiUrl);
				HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
				postConnection.setRequestMethod("POST");
				postConnection.setRequestProperty("Content-Type", "application/json");
				postConnection.setDoOutput(true);
				OutputStream os = postConnection.getOutputStream();
				os.write(mapper.writeValueAsString(requestBody).toString().getBytes());
				os.flush();
				os.close();
				int responseCode = postConnection.getResponseCode();
				System.out.println("POST Response Code :  " + responseCode);
				System.out.println("POST Response Message : " + postConnection.getResponseMessage());

			}
		} catch (ResourceNotFoundException ex) {
			Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
			jobEntityOptional.get().setStatus(DataSyncUtilityMessage.failed);
			jobRepository.save(jobEntityOptional.get());
			jobAuditEntity = new JobAuditEntity();
			jobAuditEntity.setJobId(jobEntity.getJobId());
			jobAuditEntity.setJobStatusId(jobStatusEntity.getId());
			jobAuditEntity.setStatus(DataSyncUtilityMessage.failed);
			jobAuditEntity.setSkipRecordCount(Long.valueOf(startRecord));
			jobAuditEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			jobAuditEntity.setMessage(ex.getMessage());
			logger.debug("Error : " + ex.getMessage());
			jobAuditRepository.save(jobAuditEntity);
			jobStatusEntity.setStatus(DataSyncUtilityMessage.failed);
			jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntity));
			saveNextScheduledJobDate(jobEntity.getJobId());
		} catch (Exception e) {
			jobAuditEntity.setJobId(jobEntity.getJobId());
			jobAuditEntity.setJobStatusId(jobStatusEntity.getId());
			jobAuditEntity.setStatus(DataSyncUtilityMessage.failed);
			jobAuditEntity.setSkipRecordCount(Long.valueOf(startRecord));
			jobAuditEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			jobAuditEntity.setMessage(e.getMessage());
			jobAuditRepository.save(jobAuditEntity);
			jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord) + 100);
			jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntity));
			logger.debug("Error : " + e.getMessage());
			runJob(jobEntity, jobStatusEntityOptional);
		}
		try {
			jobStatusEntityOptional.get().setEndDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			jobStatusEntity = jobStatusRepository.save(jobStatusEntityOptional.get());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Error : " + e.getMessage());
		}
		Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
		jobEntityOptional.get().setStatus(DataSyncUtilityMessage.completed);
		jobRepository.save(jobEntityOptional.get());
		saveNextScheduledJobDate(jobEntity.getJobId());
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.runJobIncremental()");
		sendMail(jobStatusEntity);
	}

	public TestFilterQueryResponse testFilterQuery(TestFilterRequest testFilterRequest) {
		ConnectionEntity connectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository
				.findById(testFilterRequest.getConnectionId());
		if (connectionEntityOptional.isPresent()) {
			connectionEntity = connectionEntityOptional.get();
		}
		return odataEdmService.testFilterQuery(testFilterRequest, connectionEntity);
	}

	public void sendMail(JobStatusEntity jobStatusEntity) {
		Optional<JobEntity> jobEntityList = jobRepository.findById(jobStatusEntity.getJobId());
		JobEntity jobEntity = new JobEntity();
		if (jobEntityList != null && jobEntityList.isPresent()) {
			jobEntity = jobEntityList.get();
		}
		List<BulkJobStatusResponse> bulkJobStatusResponseList = new ArrayList<BulkJobStatusResponse>();
		List<BulkJobDetailsEntity> bulkJobDetailsEntityListJobId = bulkJobDetailsRepository
				.findByJobId(jobStatusEntity.getJobId());
		if (bulkJobDetailsEntityListJobId != null && bulkJobDetailsEntityListJobId.size() != 0) {
			List<BulkJobDetailsEntity> bulkJobDetailsEntityList = bulkJobDetailsRepository
					.findByBulkJobId(bulkJobDetailsEntityListJobId.get(0).getBulkJobId());
			for (int i = 0; i < bulkJobDetailsEntityList.size(); i++) {
				BulkJobStatusResponse bulkJobStatusResponse = new BulkJobStatusResponse();
				if (bulkJobDetailsEntityList.get(i).getJobId() != null
						&& !bulkJobDetailsEntityList.get(i).getJobId().isEmpty()) {
					List<JobStatusEntity> jobStatusEntityList = customEntityRepository
							.findByJobId(bulkJobDetailsEntityList.get(i).getJobId(), false);
					if (jobStatusEntityList != null && !jobStatusEntityList.isEmpty()) {
						bulkJobStatusResponse.setCollectionName(bulkJobDetailsEntityList.get(i).getCollectionName());
						bulkJobStatusResponse.setCreationStatus(bulkJobDetailsEntityList.get(i).getStatus());
						bulkJobStatusResponse.setMessage(bulkJobDetailsEntityList.get(i).getMessage());
						bulkJobStatusResponse.setJobName(bulkJobDetailsEntityList.get(i).getJobName());
						bulkJobStatusResponse.setRunningStatus(jobStatusEntityList.get(0).getStatus());
						bulkJobStatusResponse
								.setTotalRecord(jobStatusEntityList.get(0).getTotalRecordCount().toString());
						bulkJobStatusResponse
								.setRecordProcessed(jobStatusEntityList.get(0).getTotalRecordProcessed().toString());
						if (jobStatusEntityList.get(0).getStartDate() != null) {
							bulkJobStatusResponse.setStartDate(jobStatusEntityList.get(0).getStartDate().toString());
						} else {
							bulkJobStatusResponse.setStartDate("");
						}
						if (jobStatusEntityList.get(0).getEndDate() != null) {
							bulkJobStatusResponse.setEndDate(jobStatusEntityList.get(0).getEndDate().toString());
						} else {
							bulkJobStatusResponse.setEndDate("");
						}
					} else {
						bulkJobStatusResponse.setCollectionName(bulkJobDetailsEntityList.get(i).getCollectionName());
						bulkJobStatusResponse.setCreationStatus(bulkJobDetailsEntityList.get(i).getStatus());
						bulkJobStatusResponse.setMessage(bulkJobDetailsEntityList.get(i).getMessage());
						bulkJobStatusResponse.setJobName(bulkJobDetailsEntityList.get(i).getJobName());
						bulkJobStatusResponse.setRunningStatus("Queued");
						bulkJobStatusResponse.setTotalRecord("");
						bulkJobStatusResponse.setRecordProcessed("");
						bulkJobStatusResponse.setStartDate("");
						bulkJobStatusResponse.setEndDate("");
					}
				} else {
					bulkJobStatusResponse.setCollectionName(bulkJobDetailsEntityList.get(i).getCollectionName());
					bulkJobStatusResponse.setCreationStatus(bulkJobDetailsEntityList.get(i).getStatus());
					bulkJobStatusResponse.setMessage(bulkJobDetailsEntityList.get(i).getMessage());
					bulkJobStatusResponse.setJobName(bulkJobDetailsEntityList.get(i).getJobName());
					bulkJobStatusResponse.setRunningStatus("");
					bulkJobStatusResponse.setTotalRecord("");
					bulkJobStatusResponse.setRecordProcessed("");
					bulkJobStatusResponse.setStartDate("");
					bulkJobStatusResponse.setEndDate("");
				}
				bulkJobStatusResponseList.add(bulkJobStatusResponse);
			}
			mailService.sendMailStatusForBulkJob(bulkJobStatusResponseList);
		} else {
			Optional<JobStatusEntity> jobStatusEntityOptional = jobStatusRepository.findById(jobStatusEntity.getId());
			if (jobStatusEntityOptional != null && jobStatusEntityOptional.isPresent()) {
				jobStatusEntity = jobStatusEntityOptional.get();
			}
			MailRequest mailRequest = new MailRequest();
			mailRequest.setJobId(jobStatusEntity.getJobId());
			mailRequest.setJobName(jobEntity.getJobName());
			mailRequest.setTotalRecordCount(jobStatusEntity.getTotalRecordCount());
			mailRequest.setProcessedRecordCount(jobStatusEntity.getTotalRecordProcessed());
			mailRequest.setStatus(jobStatusEntity.getStatus());
			mailRequest.setStartDateTime(jobStatusEntity.getStartDate());
			mailRequest.setFinishDateTime(new java.util.Date());
			mailService.sendMail(mailRequest);
		}
	}

	@Async
	public void saveNextScheduledJobDate(String jobId) {
		try {
			ScheduledJobEntity scheduledJobEntity = scheduledjobRepository.findByJobId(jobId);
			if (scheduledJobEntity != null && !scheduledJobEntity.getCronExpression().isEmpty()) {
				final String cronExpression = scheduledJobEntity.getCronExpression();
				final CronSequenceGenerator generator = new CronSequenceGenerator(cronExpression);
				final Date nextExecutionDate = generator.next(CommonUtil.getCurrentDateOnPSTTimeZone());
				scheduledJobEntity.setJobEndDate(CommonUtil.getCurrentDateOnPSTTimeZone());
				scheduledJobEntity.setJobStartDate(nextExecutionDate);
				scheduledjobRepository.save(scheduledJobEntity);
				System.out.println("JobId..." + scheduledJobEntity.getJobId());
				System.out.println("JobId...nextExecutionDate" + scheduledJobEntity.getJobStartDate());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Async
	public void runJobDifference(JobEntity jobEntity, String newMapping,
			Optional<JobStatusEntity> jobStatusEntityOptional) throws Exception {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.runJobDifference()");
		int startRecord = 0;
		Date startDate = new Date();
		JobStatusEntity jobStatusEntity = new JobStatusEntity();
		JobAuditEntity jobAuditEntity = new JobAuditEntity();
		if (jobStatusEntityOptional != null) {
			startRecord = jobStatusEntityOptional.get().getTotalRecordProcessed().intValue();
			jobStatusEntity = jobStatusEntityOptional.get();
		} else {
			jobStatusEntity.setJobId(jobEntity.getJobId());
			jobStatusEntity.setTotalRecordCount(0L);
			jobStatusEntity.setTotalRecordProcessed(0L);
			startDate = CommonUtil.getCurrentDateOnPSTTimeZone();
			jobStatusEntity.setStartDate(startDate);
			jobStatusEntity.setStatus(DataSyncUtilityMessage.initiated);
			jobEntity.setLastRun(startDate);
			jobEntity.setStatus(DataSyncUtilityMessage.initiated);
			jobEntity = jobRepository.save(jobEntity);
			jobStatusEntity = jobStatusRepository.save(jobStatusEntity);
			jobStatusEntityOptional = Optional.of(jobStatusEntity);
		}
		try {
			ConnectionEntity sourceConnectionEntity = new ConnectionEntity();
			Optional<ConnectionEntity> sourceConnectionEntityOptional = connectionEntityRepository
					.findById(jobEntity.getSourceConnection());
			if (sourceConnectionEntityOptional.isPresent()) {
				sourceConnectionEntity = sourceConnectionEntityOptional.get();
			}
			ConnectionEntity destinationConnectionEntity = new ConnectionEntity();
			Optional<ConnectionEntity> destinationConnectionEntityOptional = connectionEntityRepository
					.findById(jobEntity.getDestinationConnection());
			if (destinationConnectionEntityOptional.isPresent()) {
				destinationConnectionEntity = destinationConnectionEntityOptional.get();
			}
			if (sourceConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.odata)
					&& destinationConnectionEntity.getType().equalsIgnoreCase(DataSyncUtilityMessage.oracle)) {
				ConnectionTestRequest connectionTestRequest = new ConnectionTestRequest();
				connectionTestRequest.setServiceName(
						destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.serviceName));
				connectionTestRequest
						.setPort(destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.port));
				connectionTestRequest
						.setHost(destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.host));
				connectionTestRequest.setUserName(
						destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName));
				connectionTestRequest.setPassword(
						destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
				Connection conn = oracleService.getConnection(connectionTestRequest);
				String entityName = jobEntity.getSourceCollection();
				String totalRecordCountQuery = "";
				if (jobEntity.getJobFilter() != null && !jobEntity.getJobFilter().isEmpty()) {
					if (!totalRecordCountQuery.isEmpty()) {
						totalRecordCountQuery = totalRecordCountQuery + "%20and%20"
								+ odataEdmService.getFilterQuery(jobEntity.getJobFilter());
					} else {
						totalRecordCountQuery = totalRecordCountQuery + "?$filter="
								+ odataEdmService.getFilterQuery(jobEntity.getJobFilter());
					}
				}
				System.out.println(totalRecordCountQuery);
				String serviceUrl = sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.url);
				if (jobStatusEntity.getTotalRecordCount() == 0L) {
					Long totalRecordCount = odataEdmService.getTotalRecordCount(sourceConnectionEntity, entityName,
							totalRecordCountQuery);
					jobStatusEntity.setTotalRecordCount(totalRecordCount);
				}
				MappingRequest mappingRequest = oracleService.createInsertSQL(jobEntity.getDestinationCollection(),
						newMapping, entityName);
				jobStatusEntity = jobStatusRepository.save(jobStatusEntity);
				jobStatusEntityOptional = Optional.of(jobStatusEntity);
				MetadataResponse metadataResponse = metadataService.getMetadata(jobEntity.getSourceConnection(),
						jobEntity.getSourceCollection());
				boolean isTerminated = false;
				while (isTerminated == false) {
					Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
					if (jobEntityOptional.isPresent()) {
						if (jobEntityOptional.get().isStopped()) {
							throw new ResourceNotFoundException("Job has been stopped");
						}
					}
					String queryOptions = "?$inlinecount=allpages&$skip=" + startRecord + "&$top=100";
					if (!totalRecordCountQuery.isEmpty()) {
						queryOptions = queryOptions + "&"
								+ totalRecordCountQuery.substring(1, totalRecordCountQuery.length());
					}
					System.out.println("queryOptions :: " + queryOptions);
					ODataFeed feed = odataEdmService.readFeed(sourceConnectionEntity.getConnectionId(), serviceUrl,
							DataSyncUtilityMessage.APPLICATION_JSON, entityName, queryOptions,
							sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName),
							sourceConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
					List<Map<String, Object>> entries = new ArrayList<Map<String, Object>>();
					for (ODataEntry entry : feed.getEntries()) {
						entries.add(odataEdmService.mapEntry(entry));
					}
					if (feed.getEntries().size() < 100) {
						isTerminated = true;
					}
					String objectIds = "";
					Map<String, String> objectId = new HashMap<String, String>();
					for (int i = 0; i < entries.size(); i++) {
						objectIds = objectIds + "'" + entries.get(i).get(DataSyncUtilityMessage.primaryKey) + "'"
								+ ", ";
					}
					if (!objectIds.isEmpty()) {
						objectIds = objectIds.substring(0, objectIds.length() - 2);
						String selectQuery = "SELECT " + mappingRequest.getObjectIdMapping() + " FROM "
								+ jobEntity.getDestinationCollection() + " WHERE " + mappingRequest.getObjectIdMapping()
								+ " in (" + objectIds + ")";
						PreparedStatement selectPS = conn.prepareStatement(selectQuery);
						ResultSet resultSet = selectPS.executeQuery();
						while (resultSet.next()) {
							String primaryKey = resultSet.getString(mappingRequest.getObjectIdMapping());
							System.out.println("primarKey :: " + primaryKey);
							objectId.put(primaryKey, primaryKey);
						}
						selectPS.close();
					}
					PreparedStatement updateps = conn.prepareStatement(mappingRequest.getUpdateSQL());
					for (int i = 0; i < entries.size(); i++) {
						Map<String, Object> singleEntry = entries.get(i);
						int j = 1;
						if (objectId.get(singleEntry.get(DataSyncUtilityMessage.primaryKey)) != null
								&& !objectId.get(singleEntry.get(DataSyncUtilityMessage.primaryKey)).isEmpty()) {
							for (Map.Entry<String, String> entry : mappingRequest.getMapping().entrySet()) {
								if (singleEntry.get(entry.getValue().replace(entityName + ".", "")) != null) {
									if (entry.getValue().contains(entityName + ".")) {
										if (metadataResponse != null
												&& metadataResponse.getCollectionMetadataResponseList() != null
												&& metadataResponse.getCollectionMetadataResponseList().size() != 0) {
											for (int k = 0; k < metadataResponse.getCollectionMetadataResponseList()
													.size(); k++) {
												if (metadataResponse.getCollectionMetadataResponseList().get(k)
														.getName().equalsIgnoreCase(
																entry.getValue().replace(entityName + ".", ""))) {
													if (!metadataResponse.getCollectionMetadataResponseList().get(k)
															.getType().equalsIgnoreCase("String")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("DateTimeOffset")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("DateTime")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Decimal")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Boolean")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Int64")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Binary")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Double")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Single")
															&& !metadataResponse.getCollectionMetadataResponseList()
																	.get(k).getType().equalsIgnoreCase("Int32")) {
														updateps.setObject(j,
																singleEntry.get(
																		entry.getValue().replace(entityName + ".", ""))
																		.toString());
													} else {
														updateps.setObject(j, singleEntry
																.get(entry.getValue().replace(entityName + ".", "")));
													}
												}
											}
										}
									} else {
										updateps.setObject(j, entry.getValue());
									}
								} else {
									if (entry.getValue().equalsIgnoreCase("SYSDATE")) {
										updateps.setObject(j, new java.sql.Timestamp(new java.util.Date().getTime()));
									} else {
										updateps.setString(j, null);
									}
								}
								j++;
							}
							updateps.setObject(j, objectId.get(singleEntry.get(DataSyncUtilityMessage.primaryKey)));
							updateps.addBatch();
						}
						jobEntity = jobRepository.save(jobEntity);
					}
					updateps.executeBatch();
					startRecord = startRecord + feed.getEntries().size();
					if (!isTerminated) {
						jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord));
						jobStatusEntityOptional.get().setStatus(DataSyncUtilityMessage.inProcess);
						jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
						jobEntityOptional.get().setStatus(DataSyncUtilityMessage.inProcess);
						jobRepository.save(jobEntityOptional.get());
					} else {
						jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord));
						jobStatusEntityOptional.get().setStatus(DataSyncUtilityMessage.completed);
					}
					jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntityOptional.get()));
				}
				conn.close();
			}
		} catch (ResourceNotFoundException ex) {
			Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
			jobEntityOptional.get().setStatus(DataSyncUtilityMessage.failed);
			jobRepository.save(jobEntityOptional.get());
			jobAuditEntity = new JobAuditEntity();
			jobAuditEntity.setJobId(jobEntity.getJobId());
			jobAuditEntity.setJobStatusId(jobStatusEntity.getId());
			jobAuditEntity.setStatus(DataSyncUtilityMessage.failed);
			jobAuditEntity.setSkipRecordCount(Long.valueOf(startRecord));
			jobAuditEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			jobAuditEntity.setMessage(ex.getMessage());
			logger.debug("Error : " + ex.getMessage());
			jobAuditRepository.save(jobAuditEntity);
			jobStatusEntity.setStatus(DataSyncUtilityMessage.failed);
			jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntity));
			saveNextScheduledJobDate(jobEntity.getJobId());
		} catch (Exception e) {
			jobAuditEntity.setJobId(jobEntity.getJobId());
			jobAuditEntity.setJobStatusId(jobStatusEntity.getId());
			jobAuditEntity.setStatus(DataSyncUtilityMessage.failed);
			jobAuditEntity.setSkipRecordCount(Long.valueOf(startRecord));
			jobAuditEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			jobAuditEntity.setMessage(e.getMessage());
			jobAuditRepository.save(jobAuditEntity);
			jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord) + 100);
			jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntity));
			logger.debug("Error : " + e.getMessage());
			runJob(jobEntity, jobStatusEntityOptional);
		}
		try {
			jobStatusEntityOptional.get().setEndDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			jobStatusEntity = jobStatusRepository.save(jobStatusEntityOptional.get());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Error : " + e.getMessage());
		}
		TempJobEntity tempJobEntity = tempJobRepository.findByJobIdAndIsArchived(jobEntity.getJobId(), false);
		tempJobEntity.setArchived(true);
		tempJobRepository.save(tempJobEntity);
		Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
		jobEntityOptional.get().setStatus(DataSyncUtilityMessage.completed);
		jobRepository.save(jobEntityOptional.get());
		saveNextScheduledJobDate(jobEntity.getJobId());
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.runJobDifference()");
		sendMail(jobStatusEntity);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> runJobExport(JobEntity jobEntity, Optional<JobStatusEntity> jobStatusEntityOptional)
			throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		ConnectionEntity sourceConnectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> sourceConnectionEntityOptional = connectionEntityRepository
				.findById(jobEntity.getSourceConnection());
		if (sourceConnectionEntityOptional.isPresent()) {
			sourceConnectionEntity = sourceConnectionEntityOptional.get();
		}
		HttpURLConnection connectionAccessToken = httpClientService.getConnection(
				sourceConnectionEntity.getConnectionConfig().get("tokenUrl"), DataSyncUtilityMessage.APPLICATION_JSON,
				DataSyncUtilityMessage.HTTP_METHOD_GET, sourceConnectionEntity.getConnectionConfig().get("userName"),
				sourceConnectionEntity.getConnectionConfig().get("password"));
		InputStream content = connectionAccessToken.getInputStream();
		String responseAccessToken = IOUtils.toString(content);
		JsonParser springParser = JsonParserFactory.getJsonParser();
		System.out.println("response :" + responseAccessToken);
		Map<String, Object> mapAccessToken = springParser.parseMap(responseAccessToken);
		String accessToken = mapAccessToken.get("access_token").toString();
		HttpURLConnection connectionAPI = httpClientService.getConnectionAPIWithoutSwagger(
				sourceConnectionEntity.getConnectionConfig().get("url"), DataSyncUtilityMessage.APPLICATION_JSON,
				DataSyncUtilityMessage.HTTP_METHOD_GET, accessToken);
		content = connectionAPI.getInputStream();
		String responseAPI = IOUtils.toString(content);
		content.close();
		Map<String, Object> map = springParser.parseMap(responseAPI);
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			List<Map<String, Object>> responseList = new ArrayList<Map<String, Object>>();
			if (entry.getValue() instanceof List<?>) {
				List<Map<String, Object>> objectList = (ArrayList<Map<String, Object>>) entry.getValue();
				for (Map<String, Object> object : objectList) {
					Map<String, Object> responseMap = new HashMap<String, Object>();
					for (Map.Entry<String, Object> entry1 : object.entrySet()) {
						if (entry1.getValue() instanceof List<?>) {
							List<Map<String, Object>> objectList1 = (ArrayList<Map<String, Object>>) entry1.getValue();
							for (Map<String, Object> object1 : objectList1) {
								for (Map.Entry<String, Object> entry2 : object1.entrySet()) {
									System.out.println("List Third Level List::: " + entry1.getKey() + "_"
											+ entry2.getKey() + " = " + entry2.getValue());
									responseMap.put(entry1.getKey() + "_" + entry2.getKey(), entry2.getValue());
								}
							}
						} else if (entry1.getValue() instanceof Map) {
							Map<String, Object> map2 = (Map<String, Object>) entry1.getValue();
							for (Map.Entry<String, Object> entry2 : map2.entrySet()) {
								System.out.println("List Third Level Map::: " + entry1.getKey() + "_" + entry2.getKey()
										+ " = " + entry2.getValue());
								responseMap.put(entry1.getKey() + "_" + entry2.getKey(), entry2.getValue());
							}
						} else {
							System.out.println("List Second Level Key Value Pair::: " + entry1.getKey() + " = "
									+ entry1.getValue());
							responseMap.put(entry1.getKey(), entry1.getValue());
						}
					}
					responseList.add(responseMap);
				}
				response.put(entry.getKey(), responseList);
			} else if (entry.getValue() instanceof Map) {
				Map<String, Object> map1 = (Map<String, Object>) entry.getValue();
				for (Map.Entry<String, Object> entry1 : map1.entrySet()) {
					if (entry1.getValue() instanceof Map) {
						Map<String, Object> map2 = (Map<String, Object>) entry1.getValue();
						for (Map.Entry<String, Object> entry2 : map2.entrySet()) {
							System.out.println("Map Third Level Map::: " + entry1.getKey() + "_" + entry2.getKey()
									+ " = " + entry2.getValue());
							response.put(entry1.getKey() + "_" + entry2.getKey(), entry2.getValue());
						}
					} else {
						System.out.println(
								"Map Second Level Key Value Pair::: " + entry1.getKey() + " = " + entry1.getValue());
						response.put(entry1.getKey(), entry1.getValue());
					}
				}
			} else {
				System.out.println("First Level Key Value Pair::: " + entry.getKey() + " = " + entry.getValue());
				response.put(entry.getKey(), entry.getValue());
			}
		}
		return response;
	}

	public void runJobUpload(JobEntity jobEntity, Optional<JobStatusEntity> jobStatusEntityOptional, MultipartFile file)
			throws Exception {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.runJob()");
		int startRecord = 0;
		JobStatusEntity jobStatusEntity = new JobStatusEntity();
		JobAuditEntity jobAuditEntity = new JobAuditEntity();
		Date startDate = new Date();
		jobStatusEntity.setJobId(jobEntity.getJobId());
		jobStatusEntity.setTotalRecordCount(0L);
		jobStatusEntity.setTotalRecordProcessed(0L);
		startDate = CommonUtil.getCurrentDateOnPSTTimeZone();
		jobStatusEntity.setStartDate(startDate);
		jobStatusEntity.setStatus(DataSyncUtilityMessage.initiated);
		jobEntity.setLastRun(startDate);
		jobEntity.setStatus(DataSyncUtilityMessage.initiated);
		jobRepository.save(jobEntity);
		jobStatusEntity = jobStatusRepository.save(jobStatusEntity);
		jobStatusEntityOptional = Optional.of(jobStatusEntity);
		try {
			ConnectionEntity destinationConnectionEntity = new ConnectionEntity();
			Optional<ConnectionEntity> destinationConnectionEntityOptional = connectionEntityRepository
					.findById(jobEntity.getDestinationConnection());
			if (destinationConnectionEntityOptional.isPresent()) {
				destinationConnectionEntity = destinationConnectionEntityOptional.get();
			}
			jobStatusEntity = jobStatusRepository.save(jobStatusEntity);
			jobStatusEntityOptional = Optional.of(jobStatusEntity);
			InputStream excelFile = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			HttpURLConnection connectionAccessToken = httpClientService.getConnection(
					destinationConnectionEntity.getConnectionConfig().get("tokenUrl"),
					DataSyncUtilityMessage.APPLICATION_JSON, DataSyncUtilityMessage.HTTP_METHOD_GET,
					destinationConnectionEntity.getConnectionConfig().get("userName"),
					destinationConnectionEntity.getConnectionConfig().get("password"));
			InputStream outContent = connectionAccessToken.getInputStream();
			String responseAccessToken = IOUtils.toString(outContent);
			JsonParser springParser = JsonParserFactory.getJsonParser();
			System.out.println("response :" + responseAccessToken);
			Map<String, Object> mapAccessToken = springParser.parseMap(responseAccessToken);
			String accessToken = mapAccessToken.get("access_token").toString();
			int count = 0;
			int processedCount = 0;
			while (iterator.hasNext()) {
				Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
				if (jobEntityOptional.isPresent()) {
					if (jobEntityOptional.get().isStopped()) {
						throw new ResourceNotFoundException("Job has been stopped");
					}
				}
				Row currentRow = iterator.next();
				if (count == 0) {
					// read keys
					count++;
				} else {
					Iterator<Cell> cellIterator = currentRow.iterator();
					int cellCount = 0;
					Map<String, Object> prop = new HashMap<String, Object>();
					String id = "";
					String territoryId = "";
					String fiscalPeriodType = "";
					String forecastTypeCode = "";
					String fiscalPeriodStartDate = "";
					double content = 0;
					String currencyCode = "";
					while (cellIterator.hasNext()) {
						Cell currentCell = cellIterator.next();
						if (currentCell == null) {
							cellCount++;
						}
						if (cellCount == 12) {
							id = currentCell.getStringCellValue();
							cellCount++;
						} else if (cellCount == 14) {
							territoryId = currentCell.getStringCellValue();
							cellCount++;
						} else if (cellCount == 10) {
							forecastTypeCode = currentCell.getStringCellValue();
							cellCount++;
						} else if (cellCount == 9) {
							fiscalPeriodType = currentCell.getStringCellValue();
							cellCount++;
						} else if (cellCount == 8) {
							fiscalPeriodStartDate = currentCell.getStringCellValue();
							cellCount++;
						} else if (cellCount == 6) {
							content = currentCell.getNumericCellValue();
							cellCount++;
						} else if (cellCount == 7) {
							currencyCode = currentCell.getStringCellValue();
							cellCount++;
						} else {
							cellCount++;
						}
					}
					prop.put("id", id);
					Map<String, Object> territory = new HashMap<String, Object>();
					territory.put("id", territoryId);
					prop.put("territory", territory);
					Map<String, Object> forecast = new HashMap<String, Object>();
					forecast.put("code", forecastTypeCode);
					prop.put("forecastType", forecast);
					Map<String, Object> value = new HashMap<String, Object>();
					value.put("content", content);
					value.put("currencyCode", currencyCode);
					prop.put("amount", value);
					Map<String, Object> fiscalYear = new HashMap<String, Object>();
					fiscalYear.put("type", fiscalPeriodType);
					fiscalYear.put("startDate", fiscalPeriodStartDate);
					prop.put("fiscalPeriod", fiscalYear);
					ObjectMapper mapper = new ObjectMapper();
					logger.debug("body......" + mapper.writeValueAsString(prop));
					HttpResponse response = httpClientService.executePutCall(mapper.writeValueAsString(prop),
							accessToken, id, destinationConnectionEntity.getConnectionConfig().get("url"));
					if (response.getStatusLine().getStatusCode() != 200) {
						jobAuditEntity = new JobAuditEntity();
						jobAuditEntity.setJobId(jobEntity.getJobId());
						jobAuditEntity.setJobStatusId(jobStatusEntity.getId());
						jobAuditEntity.setStatus(DataSyncUtilityMessage.failed);
						jobAuditEntity.setSkipRecordCount(Long.valueOf(startRecord));
						jobAuditEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
						jobAuditEntity.setMessage("failed for id = " + id);
						logger.debug("failed for id = " + id);
						jobAuditRepository.save(jobAuditEntity);
					} else {
						processedCount++;
					}
					count++;
					jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(startRecord));
					jobStatusEntityOptional.get().setStatus(DataSyncUtilityMessage.inProcess);
					jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
					jobEntityOptional.get().setStatus(DataSyncUtilityMessage.inProcess);
					jobRepository.save(jobEntityOptional.get());
				}
			}
			excelFile.close();
			workbook.close();
			jobStatusEntityOptional.get().setTotalRecordCount(Long.valueOf(count - 1));
			jobStatusEntityOptional.get().setTotalRecordProcessed(Long.valueOf(processedCount));
			jobStatusEntityOptional.get().setStatus(DataSyncUtilityMessage.completed);
			jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntityOptional.get()));
		} catch (Exception ex) {
			Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
			jobEntityOptional.get().setStatus(DataSyncUtilityMessage.failed);
			jobRepository.save(jobEntityOptional.get());
			jobAuditEntity = new JobAuditEntity();
			jobAuditEntity.setJobId(jobEntity.getJobId());
			jobAuditEntity.setJobStatusId(jobStatusEntity.getId());
			jobAuditEntity.setStatus(DataSyncUtilityMessage.failed);
			jobAuditEntity.setSkipRecordCount(Long.valueOf(startRecord));
			jobAuditEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			jobAuditEntity.setMessage(ex.getMessage());
			logger.debug("Error : " + ex.getMessage());
			jobAuditRepository.save(jobAuditEntity);
			jobStatusEntity.setStatus(DataSyncUtilityMessage.failed);
			jobStatusEntityOptional = Optional.of(jobStatusRepository.save(jobStatusEntity));
		}
		try {
			jobStatusEntityOptional.get().setEndDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			jobStatusEntity = jobStatusRepository.save(jobStatusEntityOptional.get());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("Error : " + e.getMessage());
		}
		Optional<JobEntity> jobEntityOptional = jobRepository.findById(jobEntity.getJobId());
		jobEntityOptional.get().setStatus(DataSyncUtilityMessage.completed);
		jobRepository.save(jobEntityOptional.get());
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.runJob()");
		sendMail(jobStatusEntity);
	}

	public HttpResponse executeBatchCall(String serviceUrl, String payload,
			ConnectionEntity destinationConnectionEntity) throws ClientProtocolException, IOException {
		final HttpPost post = new HttpPost(URI.create(serviceUrl + "/$batch"));
		post.setHeader("Content-Type", "multipart/mixed;boundary=" + boundary);
		post.setHeader(DataSyncUtilityMessage.AUTHORIZATION_HEADER,
				getAuthorizationHeader(destinationConnectionEntity));
		post.setHeader(DataSyncUtilityMessage.CSRF_TOKEN_HEADER, getCsrfToken(serviceUrl, destinationConnectionEntity));
		HttpEntity entity = new StringEntity(payload);
		post.setEntity(entity);
		for (Header h : post.getAllHeaders()) {
			logger.info(h.getName() + " : " + h.getValue());
		}
		HttpResponse response = getHttpClient().execute(post);
		post.releaseConnection();
		logger.info("Response statusCode => " + response.getStatusLine().getStatusCode());
		return response;
	}

	private HttpClient getHttpClient() {
		if (this.m_httpClient == null) {
			this.m_httpClient = HttpClientBuilder.create().build();
		}
		return this.m_httpClient;
	}

	public String getCsrfToken(String serviceUrl, ConnectionEntity destinationConnectionEntity) {
		String m_csrfToken = null;
		try {
			m_csrfToken = readEdm(serviceUrl, destinationConnectionEntity);
		} catch (EntityProviderException | IllegalStateException | IOException e) {
			e.printStackTrace();
		}

		return m_csrfToken;
	}

	private String readEdm(String serviceUrl, ConnectionEntity destinationConnectionEntity)
			throws EntityProviderException, IllegalStateException, IOException {
		// This is used for both setting the Edm and CSRF Token :)
		String m_csrfToken = null;
		Edm m_edm = null;
		String serviceUrls = new StringBuilder(serviceUrl).append(DataSyncUtilityMessage.SEPARATOR)
				.append(DataSyncUtilityMessage.METADATA).toString();
		logger.info("Metadata url => " + serviceUrls);
		final HttpGet get = new HttpGet(serviceUrls);
		get.setHeader(DataSyncUtilityMessage.AUTHORIZATION_HEADER, getAuthorizationHeader(destinationConnectionEntity));
		get.setHeader(DataSyncUtilityMessage.CSRF_TOKEN_HEADER, DataSyncUtilityMessage.CSRF_TOKEN_FETCH);
		HttpResponse response = getHttpClient().execute(get);
		// get.releaseConnection();
		m_csrfToken = response.getFirstHeader(DataSyncUtilityMessage.CSRF_TOKEN_HEADER).getValue();
		logger.info("CSRF token => " + m_csrfToken);
		m_edm = EntityProvider.readMetadata(response.getEntity().getContent(), false);
		response.getEntity().getContent().close();
		return m_csrfToken;
	}

	private String getAuthorizationHeader(ConnectionEntity destinationConnectionEntity) {
		// Note: This example uses Basic Authentication
		// Preferred option is to use OAuth SAML bearer flow.,

		String temp = new StringBuilder(
				destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName)).append(":")
						.append(destinationConnectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password))
						.toString();
		String result = "Basic " + new String(Base64.encodeBase64(temp.getBytes()));
		logger.info("AuthorizationHeader " + result);
		return result;
	}

}
