package com.datasync.serviceEngine.Impl;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.datasync.entity.ConnectionEntity;
import com.datasync.entity.JobEntity;
import com.datasync.exception.DataSyncServiceException;
import com.datasync.repository.ConnectionEntityRepository;
import com.datasync.repository.CustomEntityRepository;
import com.datasync.request.ConnectionDataRequest;
import com.datasync.response.ConnectionDetailsResponse;
import com.datasync.response.ConnectionResponse;
import com.datasync.serviceEngine.ConnectionService;
import com.datasync.util.CommonUtil;
import com.datasync.util.ConnectionUtill;
import com.datasync.util.DataSyncUtilityMessage;

@Service
public class ConnectionServiceImpl implements ConnectionService {

	private static Logger logger = LogManager.getLogger(ConnectionServiceImpl.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private ConnectionEntityRepository connectionEntityRepository;

	@Autowired
	private CustomEntityRepository customEntityRepository;

	@Override
	public ConnectionResponse getConnection(String connectionId) {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.getConnection()");
		ConnectionEntity connectionEntity = new ConnectionEntity();
		Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository.findById(connectionId);
		if (connectionEntityOptional.isPresent()) {
			connectionEntity = connectionEntityOptional.get();
		} else {
			return null;
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.getConnection()");
		return ConnectionUtill.buildResponseConnectionDetail(connectionEntity);

	}

	@Override
	public ConnectionDetailsResponse getAllConnections(ConnectionDataRequest connectionDataRequest, Integer startRecord,
			Integer recordCount, String sortableColumn, String sortingType) throws DataSyncServiceException {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.getAllConnections()");
		List<ConnectionEntity> connectionEntityList = customEntityRepository.findAllConnection(connectionDataRequest,
				startRecord, recordCount, sortableColumn, sortingType);

		Long totalRecordCount = customEntityRepository.countTotalRecord(connectionDataRequest, startRecord, recordCount,
				sortableColumn, sortingType);
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.getAllConnections()");
		return ConnectionUtill.buildResponseAllConnectionDetail(connectionEntityList, totalRecordCount);

	}

	@Override
	public ConnectionResponse saveOrUpdateConnection(ConnectionDataRequest connectionDataRequest)
			throws DataSyncServiceException {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.saveOrUpdateConnection()");
		ConnectionEntity connectionEntity = new ConnectionEntity();
		ConnectionResponse connectionResponse = new ConnectionResponse();
		try {
			Map<String, String> connConfigMap = new HashMap<>();
			for (int i = 0; i < connectionDataRequest.getConnectionConfigRequestList().size(); i++) {
				connConfigMap.put(connectionDataRequest.getConnectionConfigRequestList().get(i).getKey(),
						connectionDataRequest.getConnectionConfigRequestList().get(i).getValue());
			}
			List<ConnectionEntity> connectionEntityNameList = connectionEntityRepository
					.findByName(connectionDataRequest.getName());
			if (connectionEntityNameList != null && connectionEntityNameList.size() != 0) {
				if (connectionDataRequest.getConnectionId() != null
						&& !connectionDataRequest.getConnectionId().isEmpty()) {
					if (!connectionEntityNameList.get(0).getConnectionId()
							.equals(connectionDataRequest.getConnectionId())) {
						throw new DataSyncServiceException("Name Already Exists");
					}
				}
			}
			List<ConnectionEntity> connectionList = customEntityRepository.findAll(connectionDataRequest);
			if (connectionDataRequest.getType().equalsIgnoreCase(DataSyncUtilityMessage.oracle)) {
				if (!connectionList.isEmpty()) {
					for (int i = 0; i < connectionList.size(); i++) {
						if (connectionList.get(i).getConnectionConfig().get(DataSyncUtilityMessage.serviceName)
								.equals(connConfigMap.get(DataSyncUtilityMessage.serviceName))
								&& connectionList.get(i).getConnectionConfig().get(DataSyncUtilityMessage.host)
										.equals(connConfigMap.get(DataSyncUtilityMessage.host))
								&& connectionList.get(i).getConnectionConfig().get(DataSyncUtilityMessage.port)
										.equals(connConfigMap.get(DataSyncUtilityMessage.port))
								&& connectionList.get(i).getConnectionConfig().get(DataSyncUtilityMessage.userName)
										.equals(connConfigMap.get(DataSyncUtilityMessage.userName))) {
							if (connectionDataRequest.getConnectionId() != null
									&& !connectionDataRequest.getConnectionId().isEmpty()) {
								if (!connectionList.get(0).getConnectionId()
										.equals(connectionDataRequest.getConnectionId())) {
									throw new DataSyncServiceException(
											"Connection Details Already Exists" + connectionList.get(0).getName());
								}
							}
						}
					}
				}
			}
			if (connectionDataRequest.getType().equalsIgnoreCase(DataSyncUtilityMessage.odata)) {
				if (!connectionList.isEmpty()) {
					for (int i = 0; i < connectionList.size(); i++) {
						if (connectionList.get(i).getConnectionConfig().get(DataSyncUtilityMessage.url)
								.equals(connConfigMap.get(DataSyncUtilityMessage.url))
								&& connectionList.get(i).getConnectionConfig().get(DataSyncUtilityMessage.userName)
										.equals(connConfigMap.get(DataSyncUtilityMessage.userName))) {
							if (connectionDataRequest.getConnectionId() != null
									&& !connectionDataRequest.getConnectionId().isEmpty()) {
								if (!connectionList.get(0).getConnectionId()
										.equals(connectionDataRequest.getConnectionId())) {
									throw new DataSyncServiceException(
											"Connection Details Already Exists" + connectionList.get(0).getName());
								}
							}
						}
					}
				}
			}
			if (connectionDataRequest.getType().equalsIgnoreCase(DataSyncUtilityMessage.mysql)) {
				if (!connectionList.isEmpty()) {
					for (int i = 0; i < connectionList.size(); i++) {
						if (connectionList.get(i).getConnectionConfig().get(DataSyncUtilityMessage.schemaName)
								.equals(connConfigMap.get(DataSyncUtilityMessage.schemaName))
								&& connectionList.get(i).getConnectionConfig().get(DataSyncUtilityMessage.host)
										.equals(connConfigMap.get(DataSyncUtilityMessage.host))
								&& connectionList.get(i).getConnectionConfig().get(DataSyncUtilityMessage.port)
										.equals(connConfigMap.get(DataSyncUtilityMessage.port))
								&& connectionList.get(i).getConnectionConfig().get(DataSyncUtilityMessage.userName)
										.equals(connConfigMap.get(DataSyncUtilityMessage.userName))) {
							if (connectionDataRequest.getConnectionId() != null
									&& !connectionDataRequest.getConnectionId().isEmpty()) {
								if (!connectionList.get(0).getConnectionId()
										.equals(connectionDataRequest.getConnectionId())) {
									throw new DataSyncServiceException(
											"Connection Details Already Exists" + connectionList.get(0).getName());
								}
							}
						}
					}
				}
			}
			if (connectionDataRequest.getConnectionId() != null && !connectionDataRequest.getConnectionId().isEmpty()) {
				Optional<ConnectionEntity> connectionEntityOptional = connectionEntityRepository
						.findById(connectionDataRequest.getConnectionId());
				if (!connectionEntityOptional.isPresent()) {
					return new ConnectionResponse();
				} else {
					connectionEntity = connectionEntityOptional.get();
				}
			}
			connectionEntity.setAliasName(connectionDataRequest.getAliasName());
			connectionEntity.setName(connectionDataRequest.getName());
			connectionEntity.setType(connectionDataRequest.getType());
			if (connectionDataRequest.getConnectionId() == null || connectionDataRequest.getConnectionId().isEmpty()) {
				connectionEntity.setCreatedBy(connectionDataRequest.getCreatedBy());
				connectionEntity.setCreatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			}
			connectionEntity.setUpdatedBy(connectionDataRequest.getUpdatedBy());
			connectionEntity.setUpdatedDate(CommonUtil.getCurrentDateOnPSTTimeZone());
			connectionEntity.setConnectionConfig(connConfigMap);
			ConnectionEntity temp = connectionEntityRepository.save(connectionEntity);
			connectionResponse = ConnectionUtill.buildResponseConnectionDetail(temp);
			logger.debug("Exiting to com.datasync.serviceEngine.Impl.saveOrUpdateConnection()");
			return connectionResponse;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new DataSyncServiceException(ex.getMessage(), ex.getCause());
		}
	}

	@Override
	public boolean deleteConnections(List<String> connectionids) throws DataSyncServiceException {
		logger.debug("Entering to com.datasync.serviceEngine.Impl.deleteConnections()");
		if (connectionids.isEmpty())
			return false;
		List<JobEntity> connectionidsList = new ArrayList<>();
		Query query = new Query();
		query.addCriteria(new Criteria().orOperator(Criteria.where("sourceConnection").in(connectionids),
				Criteria.where("destinationConnection").in(connectionids)));
		connectionidsList = mongoTemplate.find(query, JobEntity.class);
		if (!connectionidsList.isEmpty()) {
			throw new DataSyncServiceException("ConnectionIds Are already in Use");
		} else {
			query.addCriteria(Criteria.where("connectionId").in(connectionids));
			mongoTemplate.remove(query, ConnectionEntity.class);
		}
		logger.debug("Exiting to com.datasync.serviceEngine.Impl.deleteConnections()");
		return true;
	}
}
