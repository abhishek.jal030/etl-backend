package com.datasync.serviceEngine;

import java.util.Map;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.datasync.entity.JobEntity;
import com.datasync.entity.JobStatusEntity;
import com.datasync.request.ConnectionTestRequest;
import com.datasync.request.ConnectorTypeRequest;
import com.datasync.request.TableMetaDataRequest;
import com.datasync.request.TestFilterRequest;
import com.datasync.response.ConnectionTestResponse;
import com.datasync.response.ConnectorTypeResponse;
import com.datasync.response.DataPreviewResponse;
import com.datasync.response.MetadataResponse;
import com.datasync.response.MetadataResponseDetails;
import com.datasync.response.TestFilterQueryResponse;

public interface ConnectorFactory {

	public MetadataResponse getMetadata(String connectionId, String collectionName, String name);

	public ConnectionTestResponse testConnection(ConnectionTestRequest connectionTestRequest);

	public MetadataResponseDetails getAllEntitiesName(String connectionId);

	public DataPreviewResponse getDataForPreview(String connectionId, String collectionName, String collectionType,
			String previewFilter, Integer startRecord, Integer recordCount);

	public boolean refreshMetadata(String connectionId);

	public void refreshCollectionCounts(String connectionId);

	public MetadataResponse getTableMetadata(String connectionId, String collectionName);

	public MetadataResponse getUpdatedTableMetadata(String jobId);

	public boolean createTable(String connectionId, TableMetaDataRequest tableMetaDataRequest) throws Exception;

	public boolean alterTable(String connectionId, TableMetaDataRequest tableMetaDataRequest) throws Exception;

	public void runJob(JobEntity jobEntity, Optional<JobStatusEntity> jobStatusEntityOptional) throws Exception;

	public void runJobUpload(JobEntity jobEntity, Optional<JobStatusEntity> jobStatusEntityOptional, MultipartFile file)
			throws Exception;

	public Map<String, Object> runJobExport(JobEntity jobEntity, Optional<JobStatusEntity> jobStatusEntityOptional)
			throws Exception;

	public void runJobIncremental(JobEntity jobEntity, Optional<JobStatusEntity> jobStatusEntityOptional)
			throws Exception;

	public ConnectorTypeResponse[] getAllConnectorType();

	public boolean createConnectorType(ConnectorTypeRequest connectorTypeRequest);

	public TestFilterQueryResponse testFilterQuery(TestFilterRequest testFilterRequest);

	public void runJobDifference(JobEntity jobEntity, String newMapping,
			Optional<JobStatusEntity> jobStatusEntityOptional) throws Exception;

}
