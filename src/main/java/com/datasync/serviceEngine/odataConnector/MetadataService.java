package com.datasync.serviceEngine.odataConnector;

import com.datasync.entity.ConnectionEntity;
import com.datasync.entity.JobEntity;
import com.datasync.response.DataPreviewResponse;
import com.datasync.response.MetadataResponse;
import com.datasync.response.MetadataResponseDetails;

public interface MetadataService {

	public MetadataResponse getMetadata(String connectionId, String collectionName);

	public MetadataResponseDetails getEntityNames(String connectionName);

	public boolean refreshMetadata(ConnectionEntity connectionEntity);

	public DataPreviewResponse getDataForPreview(ConnectionEntity connectionEntity, String collectionName,String collectionType,
			String previewFilter, Integer startRecord, Integer recordCount);

	public MetadataResponse getTableMetadata(ConnectionEntity connectionEntity, String collectionName);

	public MetadataResponse getUpdatedTableMetadata(ConnectionEntity connectionEntity, JobEntity jobEntity);

	public void refreshCollectionCounts(ConnectionEntity connectionEntity);
}
