package com.datasync.serviceEngine.odataConnector;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Statement;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.olingo.odata2.api.edm.Edm;
import org.apache.olingo.odata2.api.edm.EdmException;
import org.apache.olingo.odata2.api.ep.EntityProviderException;
import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.ep.feed.ODataFeed;

import com.datasync.entity.ConnectionEntity;
import com.datasync.request.TestFilterRequest;
import com.datasync.response.TestFilterQueryResponse;

public interface OdataEdmService {

	public Edm readEdm(String connectionId) throws EntityProviderException, IllegalStateException, IOException;

	public ODataFeed readFeed(String connectionId, String serviceUri, String contentType, String entitySetName,
			String queryString, String userName, String password)
			throws IllegalStateException, IOException, EntityProviderException, EdmException;

	public Map<String, Object> mapEntry(ODataEntry entry);

	public Long getTotalRecordCount(ConnectionEntity connectionEntity, String entityName, String totalRecordCountQuery)
			throws IOException;

	public String getFilterQuery(String filterJson);

	public TestFilterQueryResponse testFilterQuery(TestFilterRequest testFilterRequest,
			ConnectionEntity connectionEntity);



}
