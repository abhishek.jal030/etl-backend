package com.datasync.serviceEngine.odataConnector;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.olingo.odata2.api.ep.EntityProviderException;

import com.datasync.request.ConnectionTestRequest;
import com.datasync.response.ConnectionTestResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface HttpClientService {

	public HttpURLConnection getConnection(String relativeUri, String contentType, String httpMethod, String userName,
			String password);

	public void close(HttpURLConnection connection);

	public ConnectionTestResponse testConnection(ConnectionTestRequest connectionTestRequest);

	public List<Map<String, Object>> createAPIRequest(ResultSet rs, String mapping, String sourceCollectionName)
			throws SQLException, JsonProcessingException;

	public HttpURLConnection getConnectionAPIWithoutSwagger(String relativeUri, String contentType, String httpMethod,
			String accessToken);

	public HttpResponse executePutCall(final String body, String authrizationHeader, String id, String url)
			throws ClientProtocolException, IOException, EntityProviderException, IllegalStateException;

	public List<Map<String, Object>> createODATARequest(ResultSet rs, String mapping, String sourceCollectionName)
			throws SQLException, JsonProcessingException;
}
