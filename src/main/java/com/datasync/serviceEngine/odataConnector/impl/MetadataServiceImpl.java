package com.datasync.serviceEngine.odataConnector.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.olingo.odata2.api.edm.Edm;
import org.apache.olingo.odata2.api.edm.EdmAnnotationAttribute;
import org.apache.olingo.odata2.api.edm.EdmEntitySet;
import org.apache.olingo.odata2.api.edm.EdmEntityType;
import org.apache.olingo.odata2.api.edm.EdmException;
import org.apache.olingo.odata2.api.edm.EdmProperty;
import org.apache.olingo.odata2.api.ep.EntityProviderException;
import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.ep.feed.ODataFeed;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import com.datasync.entity.ConnectionEntity;
import com.datasync.entity.JobEntity;
import com.datasync.entity.MetaDataEntity;
import com.datasync.repository.ConnectionEntityRepository;
import com.datasync.repository.MetaDataEntityRepository;
import com.datasync.response.CollectionMetadataResponse;
import com.datasync.response.DataPreviewResponse;
import com.datasync.response.InResponse;
import com.datasync.response.MetadataResponse;
import com.datasync.response.MetadataResponseDetails;
import com.datasync.serviceEngine.odataConnector.HttpClientService;
import com.datasync.serviceEngine.odataConnector.MetadataService;
import com.datasync.serviceEngine.odataConnector.OdataEdmService;
import com.datasync.serviceEngine.rdbmsConnectors.OracleConnector.OracleService;
import com.datasync.util.DataSyncUtilityMessage;

@Service
public class MetadataServiceImpl implements MetadataService {

	private static Logger logger = LogManager.getLogger(MetadataServiceImpl.class);

	@Autowired
	HttpClientService httpClientService;

	@Autowired
	OdataEdmService odataEdmService;

	@Autowired
	MetaDataEntityRepository metaDataEntityRepository;

	@Autowired
	ConnectionEntityRepository connectionEntityRepository;

	@Override
	public MetadataResponse getMetadata(String connectionId, String collectionName) {
		MetadataResponse metadataResponse = new MetadataResponse();
		List<String> navigableProperty = new ArrayList<String>();
		List<InResponse> inResponseList = new ArrayList<InResponse>();
		List<String> navigableProperties = new ArrayList<String>();
		try {
			Edm edm = odataEdmService.readEdm(connectionId);
			System.out.println(edm.getEntitySets().size());
			for (int i = 0; i < edm.getEntitySets().size(); i++) {
				EdmEntitySet entitySets = edm.getEntitySets().get(i);
				String collecType = entitySets.getName().toUpperCase();
				String entityType = entitySets.getEntityType().getName();
				String nameSpace = entitySets.getEntityType().getNamespace();
				if (collecType.equalsIgnoreCase(collectionName)) {
					metadataResponse.setCollectionName(entitySets.getName());
					List<CollectionMetadataResponse> collectionMetadataResponseList = new ArrayList<CollectionMetadataResponse>();
					System.out.println(entitySets.getName());
					if ("ContextualCodeList".equalsIgnoreCase(entityType)) {
						List<EdmProperty> edmKeyProperties = edm.getEntityType(nameSpace, entityType)
								.getKeyProperties();
						logger.debug(
								"Olingoc4cOdataEdm :: readMetaData :: SIZE OF LIST IS ::" + edmKeyProperties.size());
						edmKeyProperties.forEach(edmKeyProp -> {
							CollectionMetadataResponse collectionMetadataResponse = new CollectionMetadataResponse();
							try {
								collectionMetadataResponse.setName(edmKeyProp.getName());
								logger.debug("Olingoc4cOdataEdm :: readMetaData ::TYPE FOR " + edmKeyProp.getName()
										+ " :: " + edmKeyProp.getType().toString() + " MAXLENGTH ="
										+ edmKeyProp.getFacets().getMaxLength() + "IS NULLABLE ="
										+ edmKeyProp.getFacets().isNullable());
								collectionMetadataResponse.setType(edmKeyProp.getType().toString());
								collectionMetadataResponse.setCreatable("");
								collectionMetadataResponse.setUpdatable("");
								collectionMetadataResponse.setFilterable("");
								if (edmKeyProp.getFacets().getMaxLength() != null) {
									collectionMetadataResponse
											.setMaxLength(edmKeyProp.getFacets().getMaxLength().toString());
								} else {
									collectionMetadataResponse.setMaxLength("");
								}
								if (edmKeyProp.getFacets().isFixedLength() != null) {
									collectionMetadataResponse
											.setFixedLength(edmKeyProp.getFacets().isFixedLength().toString());
								} else {
									collectionMetadataResponse.setFixedLength("");
								}
								if (edmKeyProp.getFacets().getMaxLength() != null) {
									collectionMetadataResponse
											.setNullable(edmKeyProp.getFacets().isNullable().toString());
								} else {
									collectionMetadataResponse.setNullable("");
								}
							} catch (EdmException e) {
								logger.error("Olingoc4cOdataEdm :: readMetaData :: Exception Caught", e);
							}
							collectionMetadataResponseList.add(collectionMetadataResponse);
						});
					} else {
						List<String> edmProperties = null;
						EdmEntityType edmEntityType = edm.getEntityType(nameSpace, entityType);
						edmProperties = edmEntityType.getPropertyNames();
						edmProperties.forEach(edmProp -> {
							CollectionMetadataResponse collectionMetadataResponse = new CollectionMetadataResponse();
							logger.debug("$$$$:: " + edmProp);
							collectionMetadataResponse.setName(edmProp);
							EdmProperty property = null;
							try {
								property = (EdmProperty) edmEntityType.getProperty(edmProp);
								logger.debug("$$$$property type:: " + (property.getType()).toString());
								collectionMetadataResponse.setType(property.getType().getName());
								List<EdmAnnotationAttribute> annotationAttributesList = property.getAnnotations()
										.getAnnotationAttributes();
								for (int j = 0; j < 3; j++) {
									logger.debug("$$$$property details:: " + (annotationAttributesList.get(j).getName()
											+ ":::" + annotationAttributesList.get(j).getText()));
								}
								collectionMetadataResponse.setCreatable(annotationAttributesList.get(0).getText());
								collectionMetadataResponse.setUpdatable(annotationAttributesList.get(1).getText());
								collectionMetadataResponse.setFilterable(annotationAttributesList.get(2).getText());
								logger.debug(
										"$$$$property details IS Fixed Length =" + property.getFacets().isFixedLength()
												+ " MAXLENGTH =" + property.getFacets().getMaxLength()
												+ " IS NULLABLE =" + property.getFacets().isNullable());
								if (property.getFacets().getMaxLength() != null) {
									collectionMetadataResponse
											.setMaxLength(property.getFacets().getMaxLength().toString());
								} else {
									collectionMetadataResponse.setMaxLength("");
								}
								if (property.getFacets().isFixedLength() != null) {
									collectionMetadataResponse
											.setFixedLength(property.getFacets().isFixedLength().toString());
								} else {
									collectionMetadataResponse.setFixedLength("");
								}
								if (property.getFacets().isNullable() != null) {
									collectionMetadataResponse
											.setNullable(property.getFacets().isNullable().toString());
								} else {
									collectionMetadataResponse.setNullable("");
								}
							} catch (Exception e) {
								logger.error("Olingoc4cOdataEdm :: readMetaData :: Exception Caught", e);
							}
							collectionMetadataResponseList.add(collectionMetadataResponse);
						});
					}
					navigableProperty = readNavigablePropMetaData(edm, collectionName);
					if (navigableProperty != null && navigableProperty.size() != 0) {
						for (int j = 0; j < navigableProperty.size(); j++) {
							navigableProperties
									.add(navigableProperty.get(j) + DataSyncUtilityMessage.C4C_TABLE_LOWER_SUFFIX);
						}
					}
					metadataResponse.setCollectionMetadataResponseList(collectionMetadataResponseList);
					metadataResponse.setInResponseList(inResponseList);
					metadataResponse.setNavigableProperties(navigableProperties);
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return metadataResponse;
	}

	public List<String> readNavigablePropMetaData(Edm edm, String collectionName)
			throws IOException, EntityProviderException {
		logger.info("Olingoc4cOdataEdm :: readNavigablePropMetaData :: START");
		List<String> edmNavigableProperties = new ArrayList<String>();
		try {

			List<EdmEntitySet> edmEntitySet = edm.getEntitySets();
			edmEntitySet.forEach(item -> {
				try {
					if (null != item) {
						if (collectionName.equalsIgnoreCase(item.getName().toString())) {
							String collecType = item.getName().toUpperCase();
							String entityType = item.getEntityType().getName();
							String nameSpace = item.getEntityType().getNamespace();
							logger.debug("Olingoc4cOdataEdm :: readNavigablePropMetaData :: COLLECTION TYPE = "
									+ collecType + " ENTITY TYPE=" + entityType + " NAME SPACE =" + nameSpace);
							if ("ContextualCodeList".equalsIgnoreCase(entityType)) {
							} else {
								edmNavigableProperties
										.addAll(edm.getEntityType(nameSpace, entityType).getNavigationPropertyNames());
							}
						} // end of IF
					}

				} catch (EdmException e) {
					logger.debug("Olingoc4cOdataEdm :: readMetaData :: Exception Caught", e);
				}
			});
		} catch (EdmException e) {
			logger.debug("Olingoc4cOdataEdm :: readMetaData :: Exception Caught", e);
		}
		logger.info("Olingoc4cOdataEdm :: readNavigablePropMetaData :: END");
		return edmNavigableProperties;
	}

	public MetadataResponseDetails getEntityNames(String connectionId) {
		MetadataResponseDetails metadataResponseDetails = new MetadataResponseDetails();
		List<MetadataResponse> metadataResponseList = new ArrayList<MetadataResponse>();
		List<MetaDataEntity> metaDataEntityList = metaDataEntityRepository.findByConnectionId(connectionId);
		MetaDataEntity metaDataEntity = new MetaDataEntity();
		if (metaDataEntityList != null && metaDataEntityList.size() != 0) {
			metaDataEntity = metaDataEntityList.get(0);
		}
		try {
			Edm edm = odataEdmService.readEdm(connectionId);
			for (int i = 0; i < edm.getEntitySets().size(); i++) {
				MetadataResponse metadataResponse = new MetadataResponse();
				EdmEntitySet entitySets = edm.getEntitySets().get(i);
				metadataResponse.setCollectionName(entitySets.getName());
				metadataResponse.setCollectionCount(metaDataEntity.getCollectionCount().get(entitySets.getName()));
				metadataResponseList.add(metadataResponse);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		metadataResponseDetails.setMetadataTableResponse(metadataResponseList);
		return metadataResponseDetails;
	}

	public boolean refreshMetadata(ConnectionEntity connectionEntity) {
		boolean status = false;
		try {
			HttpURLConnection connection = httpClientService.getConnection(
					connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.url)
							+ DataSyncUtilityMessage.SEPARATOR + DataSyncUtilityMessage.METADATA,
					DataSyncUtilityMessage.APPLICATION_XML, DataSyncUtilityMessage.HTTP_METHOD_GET,
					connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName),
					connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));

			if (connection.getResponseCode() == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				MetaDataEntity metaDataEntity = new MetaDataEntity();
				List<MetaDataEntity> metaDataEntityList = metaDataEntityRepository
						.findByConnectionId(connectionEntity.getConnectionId());
				if (metaDataEntityList != null && metaDataEntityList.size() != 0) {
					metaDataEntity = metaDataEntityList.get(0);
					metaDataEntity.setMataData(response.toString());
				} else {
					metaDataEntity.setConnectionId(connectionEntity.getConnectionId());
					metaDataEntity.setMataData(response.toString());
				}
				metaDataEntityRepository.save(metaDataEntity);
				status = true;

			} else {
				status = false;
				return status;
			}
			httpClientService.close(connection);
		} catch (IOException e) {
			logger.error(e);
			status = false;
			return status;
		}
		return status;
	}

	@Async
	public void refreshCollectionCounts(ConnectionEntity connectionEntity) {
		Map<String, Long> collectionCount = new HashMap<String, Long>();
		Long totalRecordCount = 0L;
		try {
			Edm edm = odataEdmService.readEdm(connectionEntity.getConnectionId());
			for (int i = 0; i < edm.getEntitySets().size(); i++) {
				EdmEntitySet entitySets = edm.getEntitySets().get(i);
				try {
					totalRecordCount = odataEdmService.getTotalRecordCount(connectionEntity, entitySets.getName(), "");
				} catch (Exception e) {
					logger.error(e);
				}
				collectionCount.put(entitySets.getName(), totalRecordCount);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		MetaDataEntity metaDataEntity = new MetaDataEntity();
		List<MetaDataEntity> metaDataEntityList = metaDataEntityRepository
				.findByConnectionId(connectionEntity.getConnectionId());
		if (metaDataEntityList != null && metaDataEntityList.size() != 0) {
			metaDataEntity = metaDataEntityList.get(0);
			metaDataEntity.setCollectionCount(collectionCount);
		}
		metaDataEntityRepository.save(metaDataEntity);
	}

	@Override
	public DataPreviewResponse getDataForPreview(ConnectionEntity connectionEntity, String collectionName,String collectionType,
			String previewFilter, Integer startRecord, Integer recordCount) {
		List<Map<String, Object>> collectionKeysList = new ArrayList<Map<String, Object>>();
		DataPreviewResponse dataPreviewResponse = new DataPreviewResponse();

		MetadataResponse metadataResponse = getMetadata(connectionEntity.getConnectionId(), collectionName);

		StopWatch stopWatchSingle = new StopWatch();
		stopWatchSingle.start();

		String serviceUrl = connectionEntity.getConnectionConfig().get("url");

		// String serviceUrl =
		// "https://my345553.crm.ondemand.com/sap/c4c/odata/v1/c4codata";
		String queryString = null;
		String previewFilterquery = "?$";
		String totalRecordCountQuery = "";
		Long totalRecordCount = 0L;
		if (!previewFilter.equals("")) {
			previewFilterquery = previewFilterquery + "filter=" + odataEdmService.getFilterQuery(previewFilter) + "&";
			System.out.println("previewFilterquery.." + previewFilterquery);
			totalRecordCountQuery = totalRecordCountQuery + previewFilterquery;
			try {

				totalRecordCount = odataEdmService.getTotalRecordCount(connectionEntity, collectionName,
						totalRecordCountQuery);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

		queryString = previewFilterquery + "inlinecount=allpages&$skip=" + startRecord + "&$top=" + recordCount;
		System.out.println("queryString.." + queryString);

		// queryOptions.setQueryCondition(queryString);

		ODataFeed feed = null;
		try {
			feed = odataEdmService.readFeed(connectionEntity.getConnectionId(), serviceUrl,
					DataSyncUtilityMessage.APPLICATION_JSON, collectionName, queryString,
					connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName),
					connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
		} catch (EntityProviderException | EdmException | IllegalStateException | IOException e) {
			logger.error(e);
		}
		if (previewFilter.equals("")) {
			totalRecordCount = Long.valueOf(feed.getFeedMetadata().getInlineCount());

		}

		System.out.println("Read: " + feed.getEntries().size() + " entries");
		for (ODataEntry entry : feed.getEntries()) {
			collectionKeysList.add(mapEntryToCollection(entry, metadataResponse));
		}
		dataPreviewResponse.setReviewList(collectionKeysList);
		dataPreviewResponse.setTotalRecordCount(totalRecordCount.intValue());
		return dataPreviewResponse;
	}

	public Map<String, Object> mapEntryToCollection(ODataEntry entry, MetadataResponse metadataResponse) {

		Map<String, Object> propMap = entry.getProperties();
		Map<String, Object> collectionKeys = new HashMap<String, Object>();

		for (Map.Entry<String, Object> e : propMap.entrySet()) {
			Object value = e.getValue();
			String propName = e.getKey();
			if (value instanceof Calendar) {
				Calendar cal = (Calendar) value;
				collectionKeys.put(propName, cal.getTime());
			} else {
				if (value != null) {
					collectionKeys.put(propName, value);
				}
			}
		}
		return collectionKeys;
	}

	public MetadataResponse getTableMetadata(ConnectionEntity connectionEntity, String collectionName) {
		MetadataResponse metadataResponse = getMetadata(connectionEntity.getConnectionId(), collectionName);
		MetadataResponse tableMetadataResponse = new MetadataResponse();
		List<CollectionMetadataResponse> collectionMetadataResponseList = new ArrayList<CollectionMetadataResponse>();
		for (int i = 0; i < metadataResponse.getCollectionMetadataResponseList().size(); i++) {
			CollectionMetadataResponse collectionMetadataResponse = new CollectionMetadataResponse();
			collectionMetadataResponse.setName(metadataResponse.getCollectionMetadataResponseList().get(i).getName());
			collectionMetadataResponse.setType(OracleService
					.getOracleDataType(metadataResponse.getCollectionMetadataResponseList().get(i).getType(),
							!metadataResponse.getCollectionMetadataResponseList().get(i).getMaxLength().equals("")
									? Integer.parseInt(
											metadataResponse.getCollectionMetadataResponseList().get(i).getMaxLength())
									: 0));
			collectionMetadataResponseList.add(collectionMetadataResponse);

		}
		tableMetadataResponse.setCollectionMetadataResponseList(collectionMetadataResponseList);
		tableMetadataResponse.setCollectionName(collectionName);
		return tableMetadataResponse;
	}

	public MetadataResponse getUpdatedTableMetadata(ConnectionEntity connectionEntity, JobEntity jobEntity) {
		MetadataResponse metadataResponse = getMetadata(connectionEntity.getConnectionId(),
				jobEntity.getSourceCollection());
		MetadataResponse tableMetadataResponse = new MetadataResponse();
		List<CollectionMetadataResponse> collectionMetadataResponseList = new ArrayList<CollectionMetadataResponse>();
		for (int i = 0; i < metadataResponse.getCollectionMetadataResponseList().size(); i++) {
			CollectionMetadataResponse collectionMetadataResponse = new CollectionMetadataResponse();
			collectionMetadataResponse.setName(metadataResponse.getCollectionMetadataResponseList().get(i).getName());
			collectionMetadataResponse.setType(OracleService
					.getOracleDataType(metadataResponse.getCollectionMetadataResponseList().get(i).getType(),
							!metadataResponse.getCollectionMetadataResponseList().get(i).getMaxLength().equals("")
									? Integer.parseInt(
											metadataResponse.getCollectionMetadataResponseList().get(i).getMaxLength())
									: 0));
			collectionMetadataResponseList.add(collectionMetadataResponse);

		}
		Map<String, String> mappingObject = new HashMap<String, String>();
		JSONObject jo = new JSONObject(jobEntity.getMapping());
		jo.keySet().stream().forEach((key) -> {
			mappingObject.put(key, jo.get(key).toString());
		});
		for (Map.Entry<String, String> entry : mappingObject.entrySet()) {
			for (int i = 0; i < collectionMetadataResponseList.size(); i++) {
				if ((jobEntity.getSourceCollection() + "." + collectionMetadataResponseList.get(i).getName())
						.equalsIgnoreCase(entry.getValue())) {
					collectionMetadataResponseList.get(i).setName(entry.getKey());
				}
			}
		}
		tableMetadataResponse.setCollectionMetadataResponseList(collectionMetadataResponseList);
		tableMetadataResponse.setCollectionName(jobEntity.getDestinationCollection());
		return tableMetadataResponse;
	}

}
