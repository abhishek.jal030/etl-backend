package com.datasync.serviceEngine.odataConnector.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.olingo.odata2.api.edm.Edm;
import org.apache.olingo.odata2.api.edm.EdmEntityContainer;
import org.apache.olingo.odata2.api.edm.EdmException;
import org.apache.olingo.odata2.api.ep.EntityProvider;
import org.apache.olingo.odata2.api.ep.EntityProviderException;
import org.apache.olingo.odata2.api.ep.EntityProviderReadProperties;
import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.ep.feed.ODataFeed;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datasync.entity.ConnectionEntity;
import com.datasync.entity.MetaDataEntity;
import com.datasync.exception.ResourceNotFoundException;
import com.datasync.repository.MetaDataEntityRepository;
import com.datasync.request.TestFilterRequest;
import com.datasync.response.TestFilterQueryResponse;
import com.datasync.serviceEngine.odataConnector.HttpClientService;
import com.datasync.serviceEngine.odataConnector.OdataEdmService;
import com.datasync.util.DataSyncUtilityMessage;

@Service
public class OdataEdmServiceImpl implements OdataEdmService {

	private static Logger logger = LogManager.getLogger(OdataEdmServiceImpl.class);

	@Autowired
	HttpClientService httpClientService;

	@Autowired
	MetaDataEntityRepository metaDataEntityRepository;

	private String boundary = "batch_" + UUID.randomUUID().toString();

	private HttpClient m_httpClient = null;

	public Edm readEdm(String connectionId) throws EntityProviderException, IllegalStateException, IOException {
		Edm edm = null;
		MetaDataEntity metaDataEntity = new MetaDataEntity();
		List<MetaDataEntity> metaDataEntityList = metaDataEntityRepository.findByConnectionId(connectionId);
		if (metaDataEntityList != null && metaDataEntityList.size() != 0) {
			metaDataEntity = metaDataEntityList.get(0);
		}
		InputStream content;
		content = new ByteArrayInputStream(metaDataEntity.getMataData().getBytes(StandardCharsets.UTF_8));
		edm = EntityProvider.readMetadata(content, false);
		logger.debug("Success");
		return edm;
	}

	public ODataFeed readFeed(String connectionId, String serviceUri, String contentType, String entitySetName,
			String queryString, String userName, String password)
			throws IllegalStateException, IOException, EntityProviderException, EdmException {
		Edm edm = readEdm(connectionId);
		EdmEntityContainer entityContainer = edm.getDefaultEntityContainer();
		String absolutUri = createUri(serviceUri, entitySetName, null, queryString);
		HttpURLConnection connection = httpClientService.getConnection(absolutUri, contentType,
				DataSyncUtilityMessage.HTTP_METHOD_GET, userName, password);
		InputStream content = connection.getInputStream();
		return EntityProvider.readFeed(contentType, entityContainer.getEntitySet(entitySetName), content,
				EntityProviderReadProperties.init().build());
	}

	private static String createUri(String serviceUri, String entitySetName, String id, String queryString) {
		final StringBuilder absolauteUri = new StringBuilder(serviceUri).append(DataSyncUtilityMessage.SEPARATOR)
				.append(entitySetName);
		if (id != null) {
			absolauteUri.append("('").append(id).append("')");
		}
		absolauteUri.append(queryString);
		System.out.println("createUri : " + absolauteUri.toString());
		return absolauteUri.toString();
	}

	public Map<String, Object> mapEntry(ODataEntry entry) {
		Map<String, Object> propMap = entry.getProperties();
		Map<String, Object> collectionKeys = new HashMap<String, Object>();

		for (Map.Entry<String, Object> e : propMap.entrySet()) {
			Object value = e.getValue();
			String propName = e.getKey();
			if (value instanceof Calendar) {
				Calendar cal = (Calendar) value;
				collectionKeys.put(propName, new java.sql.Date(cal.getTime().getTime()));
			} else {
				if (value != null) {
					collectionKeys.put(propName, value);
				}
			}
		}
		return collectionKeys;
	}

	public Long getTotalRecordCount(ConnectionEntity connectionEntity, String entityName, String filterRecordCountQuery)
			throws IOException {
		String totalCountServiceUrl = connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.url)
				+ DataSyncUtilityMessage.SEPARATOR + entityName + DataSyncUtilityMessage.SEPARATOR
				+ DataSyncUtilityMessage.COUNT + filterRecordCountQuery;
		HttpURLConnection totalRecotrdConnection = httpClientService.getConnection(totalCountServiceUrl,
				DataSyncUtilityMessage.APPLICATION_JSON, DataSyncUtilityMessage.HTTP_METHOD_GET,
				connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.userName),
				connectionEntity.getConnectionConfig().get(DataSyncUtilityMessage.password));
		if (totalRecotrdConnection.getResponseCode() != 200) {
			BufferedReader in = new BufferedReader(new InputStreamReader(totalRecotrdConnection.getErrorStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			throw new ResourceNotFoundException(response.toString());
		}
		totalRecotrdConnection.getInputStream();
		BufferedReader in = new BufferedReader(new InputStreamReader(totalRecotrdConnection.getInputStream()));

		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		httpClientService.close(totalRecotrdConnection);
		return Long.parseLong(response.toString());
	}

	public String getFilterQuery(String filterJson) {
		{
			String filterQuery = "";
			List<Map<String, String>> mappingObjectList = new ArrayList<Map<String, String>>();
			JSONArray jo = new JSONArray(filterJson);
			for (int i = 0; i < jo.length(); i++) {
				JSONObject jo1 = jo.getJSONObject(i);
				Map<String, String> mappingObject = new HashMap<String, String>();
				jo1.keySet().stream().forEach((key) -> {
					mappingObject.put(key, jo1.get(key).toString());
				});
				mappingObjectList.add(mappingObject);
			}
			for (int i = 0; i < mappingObjectList.size(); i++) {
				if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.logicalOperator).isEmpty()) {
					filterQuery = filterQuery + "%20"
							+ mappingObjectList.get(i).get(DataSyncUtilityMessage.logicalOperator).toLowerCase()
							+ "%20";
				}
				filterQuery = filterQuery + mappingObjectList.get(i).get(DataSyncUtilityMessage.field) + "%20"
						+ getComparisonOperator(
								mappingObjectList.get(i).get(DataSyncUtilityMessage.comparisonOperator));
				if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType).isEmpty() && mappingObjectList.get(i)
						.get(DataSyncUtilityMessage.dataType).equalsIgnoreCase(DataSyncUtilityMessage.datetimeoffset)) {
					filterQuery = filterQuery + "%20datetimeoffset'"
							+ mappingObjectList.get(i).get(DataSyncUtilityMessage.value).replace(" ", "%20") + "'";
				} else if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType).isEmpty()
						&& mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType)
								.equalsIgnoreCase(DataSyncUtilityMessage.datetime)) {
					filterQuery = filterQuery + "%20datetime'"
							+ mappingObjectList.get(i).get(DataSyncUtilityMessage.value).replace(" ", "%20") + "'";
				} else if (!mappingObjectList.get(i).get(DataSyncUtilityMessage.dataType).isEmpty() && mappingObjectList
						.get(i).get(DataSyncUtilityMessage.dataType).equalsIgnoreCase(DataSyncUtilityMessage.bool)) {
					filterQuery = filterQuery + "%20" + mappingObjectList.get(i).get(DataSyncUtilityMessage.value);

				} else {
					filterQuery = filterQuery + "%20'" + mappingObjectList.get(i).get("value").replace(" ", "%20")
							+ "'";
				}
			}
			System.out.println("filterQuery ::" + filterQuery);
			return filterQuery;
		}
	}

	private String getComparisonOperator(String comparisonOperator) {
		switch (comparisonOperator) {
		case "equal":
			return "eq";
		case "not equal":
			return "ne";
		case "less than":
			return "lt";
		case "less than or equal":
			return "le";
		case "greater than":
			return "gt";
		case "Greater than or equal":
			return "ge";
		default:
			return "eq";
		}
	}

	public TestFilterQueryResponse testFilterQuery(TestFilterRequest testFilterRequest,
			ConnectionEntity connectionEntity) {
		TestFilterQueryResponse testFilterQueryResponse = new TestFilterQueryResponse();
		String filterQuery = "?$filter=" + getFilterQuery(testFilterRequest.getFilterJson());
		try {
			testFilterQueryResponse.setTotalRecordCount(
					getTotalRecordCount(connectionEntity, testFilterRequest.getCollectionName(), filterQuery));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return testFilterQueryResponse;
	}

	

}
