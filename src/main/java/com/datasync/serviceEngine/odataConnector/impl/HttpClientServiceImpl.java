package com.datasync.serviceEngine.odataConnector.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.olingo.odata2.api.ep.EntityProviderException;
import org.json.JSONObject;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.datasync.request.ConnectionTestRequest;
import com.datasync.request.MappingRequest;
import com.datasync.response.ConnectionTestResponse;
import com.datasync.serviceEngine.odataConnector.HttpClientService;
import com.datasync.util.DataSyncUtilityMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class HttpClientServiceImpl implements HttpClientService {

	private static Logger logger = LogManager.getLogger(HttpClientServiceImpl.class);

	private HttpClient m_httpClient = null;

	public HttpURLConnection getConnection(String relativeUri, String contentType, String httpMethod, String userName,
			String password) {
		logger.debug("Entering to com.datasync.serviceEngine.odataConnector.impl.getConnection()");
		HttpURLConnection conn = null;
		try {
			conn = connect(relativeUri, contentType, httpMethod, userName, password);
		} catch (IOException e) {
			logger.error("C4cODataAuthConnection :: getConnection() :: Exception Occured", e);
		}
		logger.debug("Exiting to com.datasync.serviceEngine.odataConnector.impl.getConnection()");

		return conn;
	}

	public HttpURLConnection getConnectionAPIWithoutSwagger(String relativeUri, String contentType, String httpMethod,
			String accessToken) {
		logger.debug("Entering to com.datasync.serviceEngine.odataConnector.impl.getConnection()");
		HttpURLConnection conn = null;
		try {
			conn = connect(relativeUri, contentType, httpMethod, accessToken);
		} catch (IOException e) {
			logger.error("C4cODataAuthConnection :: getConnection() :: Exception Occured", e);
		}
		logger.debug("Exiting to com.datasync.serviceEngine.odataConnector.impl.getConnection()");

		return conn;
	}

	public void close(HttpURLConnection connection) {
		connection.disconnect();
	}

	private HttpURLConnection connect(String relativeUri, String contentType, String httpMethod, String accessToken)
			throws IOException {
		logger.debug("Entering to com.datasync.serviceEngine.odataConnector.impl.connect()");
		HttpURLConnection connection = initializeConnection(relativeUri, contentType, httpMethod);
		connection = addAuthenticationHeader(connection, accessToken);
		connection.connect();
		// checkStatus(connection);
		logger.debug("Exiting to com.datasync.serviceEngine.odataConnector.impl.connect()");
		return connection;
	}

	private HttpURLConnection addAuthenticationHeader(HttpURLConnection connection, String accessToken) {
		// String userpass = "Mlohmor:Nopasswd1";
		logger.debug("Entering to com.datasync.serviceEngine.odataConnector.impl.addAuthenticationHeader()");
		connection.setRequestProperty("x-sap-crm-token", accessToken);
		logger.debug("Exiting to com.datasync.serviceEngine.odataConnector.impl.addAuthenticationHeader()");
		return connection;
	}

	private HttpURLConnection connect(String relativeUri, String contentType, String httpMethod, String userName,
			String password) throws IOException {
		logger.debug("Entering to com.datasync.serviceEngine.odataConnector.impl.connect()");
		HttpURLConnection connection = initializeConnection(relativeUri, contentType, httpMethod);
		connection = addAuthenticationHeader(connection, userName, password);
		connection.connect();
		// checkStatus(connection);
		logger.debug("Exiting to com.datasync.serviceEngine.odataConnector.impl.connect()");
		return connection;
	}

	public HttpURLConnection getConnectionApi(String relativeUri, String contentType, String httpMethod) {
		logger.debug("Entering to com.datasync.serviceEngine.odataConnector.impl.getConnection()");
		HttpURLConnection conn = null;
		try {
			conn = connectApi(relativeUri, contentType, httpMethod);
		} catch (IOException e) {
			logger.error("C4cODataAuthConnection :: getConnection() :: Exception Occured", e);
		}
		logger.debug("Exiting to com.datasync.serviceEngine.odataConnector.impl.getConnection()");

		return conn;
	}

	private HttpURLConnection connectApi(String relativeUri, String contentType, String httpMethod) throws IOException {
		logger.debug("Entering to com.datasync.serviceEngine.odataConnector.impl.connect()");
		HttpURLConnection connection = initializeConnection(relativeUri, contentType, httpMethod);
		connection.connect();
		// checkStatus(connection);
		logger.debug("Exiting to com.datasync.serviceEngine.odataConnector.impl.connect()");
		return connection;
	}

	private HttpURLConnection initializeConnection(String absolutUri, String contentType, String httpMethod)
			throws MalformedURLException, IOException {
		logger.debug("Entering to com.datasync.serviceEngine.odataConnector.impl.initializeConnection()");
		URL url = new URL(absolutUri);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod(httpMethod);
		connection.setRequestProperty(DataSyncUtilityMessage.HTTP_HEADER_ACCEPT, contentType);
		if (DataSyncUtilityMessage.HTTP_METHOD_POST.equals(httpMethod)
				|| DataSyncUtilityMessage.HTTP_METHOD_PUT.equals(httpMethod)) {
			connection.setDoOutput(true);
			connection.setRequestProperty(DataSyncUtilityMessage.HTTP_HEADER_CONTENT_TYPE, contentType);
		}
		logger.debug("Exiting to com.datasync.serviceEngine.odataConnector.impl.initializeConnection()");
		return connection;
	}

	private HttpURLConnection addAuthenticationHeader(HttpURLConnection connection, String userName, String password) {
		// String userpass = "Mlohmor:Nopasswd1";
		logger.debug("Entering to com.datasync.serviceEngine.odataConnector.impl.addAuthenticationHeader()");
		String userpass = userName + ":" + password;
		String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
		connection.setRequestProperty("Authorization", basicAuth);
		logger.debug("Exiting to com.datasync.serviceEngine.odataConnector.impl.addAuthenticationHeader()");
		return connection;
	}

	/*
	 * private HttpStatusCodes checkStatus(HttpURLConnection connection) throws
	 * IOException { HttpStatusCodes httpStatusCode =
	 * HttpStatusCodes.fromStatusCode(connection.getResponseCode()); if (400 <=
	 * httpStatusCode.getStatusCode() && httpStatusCode.getStatusCode() <= 599) { if
	 * (!(400 == httpStatusCode.getStatusCode())) { throw new
	 * RuntimeException("Http Connection failed with status " +
	 * httpStatusCode.getStatusCode() + " " + httpStatusCode.toString()); } } return
	 * httpStatusCode; }
	 */

	public ConnectionTestResponse testConnection(ConnectionTestRequest connectionTestRequest) {
		logger.debug("Entering to com.datasync.serviceEngine.odataConnector.impl.testConnection()");
		ConnectionTestResponse connectionTestResponse = new ConnectionTestResponse();
		try {
			if (connectionTestRequest.getConnectionType().equals("ODATA")) {

				HttpURLConnection connection = getConnection(connectionTestRequest.getUrl(),
						DataSyncUtilityMessage.APPLICATION_JSON, DataSyncUtilityMessage.HTTP_METHOD_GET,
						connectionTestRequest.getUserName(), connectionTestRequest.getPassword());
				if (connection.getResponseCode() != 200) {
					connectionTestResponse.setStatus(true);
					BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					connectionTestResponse.setMessage(response.toString());
				} else {
					connectionTestResponse.setStatus(true);
					connectionTestResponse.setMessage(DataSyncUtilityMessage.messages);
				}
				close(connection);
			} else if (connectionTestRequest.getConnectionType().equalsIgnoreCase(DataSyncUtilityMessage.datahug)) {
				HttpURLConnection connectionAccessToken = getConnection(connectionTestRequest.getTokenUrl(),
						DataSyncUtilityMessage.APPLICATION_JSON, DataSyncUtilityMessage.HTTP_METHOD_GET,
						connectionTestRequest.getUserName(), connectionTestRequest.getPassword());
				if (connectionAccessToken.getResponseCode() != 200) {
					InputStream content = connectionAccessToken.getInputStream();
					String responseAccessToken = IOUtils.toString(content);
					JsonParser springParser = JsonParserFactory.getJsonParser();
					System.out.println("response :" + responseAccessToken);
					Map<String, Object> mapAccessToken = springParser.parseMap(responseAccessToken);
					String accessToken = mapAccessToken.get("access_token").toString();
					HttpURLConnection connectionAPI = getConnectionAPIWithoutSwagger(connectionTestRequest.getUrl(),
							DataSyncUtilityMessage.APPLICATION_JSON, DataSyncUtilityMessage.HTTP_METHOD_GET,
							accessToken);
					if (connectionAPI.getResponseCode() != 200) {
						connectionTestResponse.setStatus(true);
						BufferedReader in = new BufferedReader(new InputStreamReader(connectionAPI.getInputStream()));
						String inputLine;
						StringBuffer response = new StringBuffer();

						while ((inputLine = in.readLine()) != null) {
							response.append(inputLine);
						}
						connectionTestResponse.setMessage(response.toString());
					} else {
						connectionTestResponse.setStatus(true);
						connectionTestResponse.setMessage(DataSyncUtilityMessage.messages);
					}
					close(connectionAPI);
				} else {
					connectionTestResponse.setStatus(true);
					connectionTestResponse.setMessage(DataSyncUtilityMessage.messages);
				}
				close(connectionAccessToken);
			} else {
				HttpURLConnection connection = getConnectionApi(connectionTestRequest.getTestUrl(),
						DataSyncUtilityMessage.APPLICATION_JSON, DataSyncUtilityMessage.HTTP_METHOD_GET);
				if (connection.getResponseCode() != 200) {
					connectionTestResponse.setStatus(true);
					BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					connectionTestResponse.setMessage(response.toString());
				} else {
					connectionTestResponse.setStatus(true);
					connectionTestResponse.setMessage(DataSyncUtilityMessage.messages);
				}
				close(connection);
			}
		} catch (Exception e) {
			logger.error(e);
			connectionTestResponse.setStatus(false);
			connectionTestResponse.setMessage(e.getMessage());
		}
		logger.debug("Exiting to com.datasync.serviceEngine.odataConnector.impl.testConnection()");
		return connectionTestResponse;
	}

	public List<Map<String, Object>> createAPIRequest(ResultSet rs, String mapping, String sourceCollectionName)
			throws SQLException, JsonProcessingException {
		Integer sourceCollLen = sourceCollectionName.length();
		Map<String, String> mappingObject = new HashMap<String, String>();
		Map<String, Object> prop = new HashMap<String, Object>();
		List<Map<String, Object>> propList = new ArrayList<Map<String, Object>>();

		JSONObject jo = new JSONObject(mapping);
		jo.keySet().stream().forEach((key) -> {
			mappingObject.put(key, jo.get(key).toString());
		});

		Map<String, String> dataTypeMap = new HashMap<String, String>();
		ResultSetMetaData rsmd = rs.getMetaData();
		for (int i = 1; i <= rsmd.getColumnCount(); i++) {
			dataTypeMap.put(rsmd.getColumnLabel(i), rsmd.getColumnTypeName(i));
			// System.out.println(rsmd.getColumnLabel(i));

		}

		while (rs.next()) {
			prop = new HashMap<String, Object>();
			for (Map.Entry<String, String> entry : mappingObject.entrySet()) {
				if (!entry.getValue().equals("") && entry.getValue() != null) {
					if (dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1)).equalsIgnoreCase("TIMESTAMP")
							|| dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1))
									.equalsIgnoreCase("DATETIME")) {
						// System.out.print(entry.getValue().substring(sourceCollLen + 1));
						// System.out.print((java.util.Date) rs
						// .getObject(entry.getValue().substring(sourceCollLen + 1)));
						java.util.Date date = (java.util.Date) rs
								.getObject(entry.getValue().substring(sourceCollLen + 1));
						if (date != null) {
							String dateStr = new java.text.SimpleDateFormat("yyyy-MM-dd H:m:s").format(date);
							// System.out.println(dateStr);
							prop.put(entry.getKey(), dateStr);

						} else {
							prop.put(entry.getKey(), "");

						}

					} else if (dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1)).equalsIgnoreCase("INT")
							|| dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1))
									.equalsIgnoreCase("tinyint")
							|| dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1))
									.equalsIgnoreCase("BIGINT")) {

						if (rs.getObject(entry.getValue().substring(sourceCollLen + 1)) != null) {
							prop.put(entry.getKey(), rs.getObject(entry.getValue().substring(sourceCollLen + 1)));
						} else {
							prop.put(entry.getKey(), 0);
						}

					} else if (dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1))
							.equalsIgnoreCase("decimal")
							|| dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1))
									.equalsIgnoreCase("double")) {

						if (rs.getObject(entry.getValue().substring(sourceCollLen + 1)) != null) {
							prop.put(entry.getKey(), rs.getObject(entry.getValue().substring(sourceCollLen + 1)));
						} else {
							prop.put(entry.getKey(), 0.0);
						}

					} else if (dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1))
							.equalsIgnoreCase("varchar")
							|| dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1))
									.equalsIgnoreCase("text")) {

						if (rs.getObject(entry.getValue().substring(sourceCollLen + 1)) != null) {
							prop.put(entry.getKey(), rs.getObject(entry.getValue().substring(sourceCollLen + 1)));
						} else {
							prop.put(entry.getKey(), null);
						}

					}
				}
			}
			propList.add(prop);
		}
		return propList;
	}

	public HttpResponse executePutCall(final String body, String authrizationHeader, String id, String url)
			throws ClientProtocolException, IOException, EntityProviderException, IllegalStateException {
		logger.debug("Entering to com.netapp.service.impl.executeBatchCall()");
		final HttpPut put = new HttpPut(URI.create(url + "/" + id));
		put.setHeader("Content-Type", DataSyncUtilityMessage.APPLICATION_JSON);
		put.setHeader("x-sap-crm-token", authrizationHeader);
		HttpEntity entity = new StringEntity(body);

		put.setEntity(entity);
		for (Header h : put.getAllHeaders()) {
			logger.debug(h.getName() + " : " + h.getValue());
		}

		HttpResponse response = getHttpClient().execute(put);
		put.releaseConnection();

		logger.debug("Response statusCode => " + response.getStatusLine().getStatusCode());
		logger.debug("Exiting to com.netapp.service.impl.executeBatchCall()");
		return response;
	}

	private HttpClient getHttpClient() {
		logger.debug("Entering to com.netapp.service.impl.DataHugServiceImpl.getHttpClient()");
		if (this.m_httpClient == null) {
			this.m_httpClient = HttpClientBuilder.create().build();
		}
		logger.debug("Exiting to com.netapp.service.impl.DataHugServiceImpl.getHttpClient()");
		return this.m_httpClient;
	}

	@Override
	public List<Map<String, Object>> createODATARequest(ResultSet rs, String mapping, String sourceCollectionName)
			throws SQLException, JsonProcessingException {
		Integer sourceCollLen = sourceCollectionName.length();
		Map<String, String> mappingObject = new HashMap<String, String>();
		Map<String, Object> prop = new HashMap<String, Object>();
		List<Map<String, Object>> propList = new ArrayList<Map<String, Object>>();

		JSONObject jo = new JSONObject(mapping);
		jo.keySet().stream().forEach((key) -> {
			mappingObject.put(key, jo.get(key).toString());
		});

		Map<String, String> dataTypeMap = new HashMap<String, String>();
		ResultSetMetaData rsmd = rs.getMetaData();
		for (int i = 1; i <= rsmd.getColumnCount(); i++) {
			dataTypeMap.put(rsmd.getColumnLabel(i), rsmd.getColumnTypeName(i));
			// System.out.println(rsmd.getColumnLabel(i));

		}

		while (rs.next()) {
			prop = new HashMap<String, Object>();
			for (Map.Entry<String, String> entry : mappingObject.entrySet()) {
				if (!entry.getValue().equals("") || entry.getValue() == null) {
					if (dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1)).equalsIgnoreCase("DATE")) {
						// System.out.print(entry.getValue().substring(sourceCollLen + 1));
						java.util.Date date = (java.util.Date) rs
								.getObject(entry.getValue().substring(sourceCollLen + 1));
						if (date != null) {
							String dateStr = new java.text.SimpleDateFormat("yyyy-MM-dd H:m:s").format(date);
							// System.out.println(dateStr);
							prop.put(entry.getKey(), dateStr);

						} else {
							prop.put(entry.getKey(), "");

						}

					} else if (dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1))
							.equalsIgnoreCase("NUMBER")) {

						if (rs.getObject(entry.getValue().substring(sourceCollLen + 1)) != null) {
							prop.put(entry.getKey(), rs.getObject(entry.getValue().substring(sourceCollLen + 1)));
						} else {
							prop.put(entry.getKey(), 0);
						}

					} else if (dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1))
							.equalsIgnoreCase("LONG")) {

						if (rs.getObject(entry.getValue().substring(sourceCollLen + 1)) != null) {
							prop.put(entry.getKey(), rs.getObject(entry.getValue().substring(sourceCollLen + 1)));
						} else {
							prop.put(entry.getKey(), 0.0);
						}

					} else if (dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1))
							.equalsIgnoreCase("VARCHAR2")
							|| dataTypeMap.get(entry.getValue().substring(sourceCollLen + 1))
									.equalsIgnoreCase("CHAR")) {

						if (rs.getObject(entry.getValue().substring(sourceCollLen + 1)) != null) {
							prop.put(entry.getKey(), rs.getObject(entry.getValue().substring(sourceCollLen + 1)));
						} else {
							prop.put(entry.getKey(), null);
						}

					}
				}
			}
			propList.add(prop);
		}
		return propList;
	}
}
