package com.datasync.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.datasync.entity.ScheduledJobEntity;

public interface ScheduledjobRepository extends MongoRepository<ScheduledJobEntity, String> {
	
	ScheduledJobEntity findByJobId(String jobId);
	
	List<ScheduledJobEntity> findJobIdByJobStartDate(Date jobStartDate);

}
