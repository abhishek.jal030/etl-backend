package com.datasync.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.datasync.entity.JobStatusEntity;

public interface JobStatusRepository extends MongoRepository<JobStatusEntity, String> {

}
