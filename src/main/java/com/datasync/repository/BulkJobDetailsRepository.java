package com.datasync.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.datasync.entity.BulkJobDetailsEntity;

public interface BulkJobDetailsRepository extends MongoRepository<BulkJobDetailsEntity, String> {

	List<BulkJobDetailsEntity> findByBulkJobId(String bulkJobId);

	List<BulkJobDetailsEntity> findByJobId(String jobId);

}
