package com.datasync.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.datasync.entity.TempJobEntity;

public interface TempJobRepository extends MongoRepository<TempJobEntity, String> {

	TempJobEntity findByJobIdAndIsArchived(String jobId, boolean isArchived);

}
