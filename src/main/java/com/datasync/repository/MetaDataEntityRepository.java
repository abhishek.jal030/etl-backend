package com.datasync.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.datasync.entity.MetaDataEntity;

public interface MetaDataEntityRepository extends MongoRepository<MetaDataEntity, String> {

	List<MetaDataEntity> findByConnectionId(String connectionId);

}
