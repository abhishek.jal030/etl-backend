package com.datasync.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.datasync.entity.ConnectorTypeEntity;

public interface ConnectorTypeRepository extends MongoRepository<ConnectorTypeEntity, String> {
}
