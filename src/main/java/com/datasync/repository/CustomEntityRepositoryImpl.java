package com.datasync.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.datasync.entity.ConnectionEntity;
import com.datasync.entity.JobAuditEntity;
import com.datasync.entity.JobEntity;
import com.datasync.entity.JobStatusEntity;
import com.datasync.request.ConnectionDataRequest;
import com.datasync.request.JobRequest;

@Repository
public class CustomEntityRepositoryImpl implements CustomEntityRepository {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<ConnectionEntity> findAllConnection(ConnectionDataRequest connectionDataRequest, Integer startRecord,
			Integer recordCount, String sortableColumn, String sortingType) {
		Query query = new Query();
		List<ConnectionEntity> connectionEntityList = new ArrayList<>();
		query.skip(startRecord);
		query.limit(recordCount);
		if (sortableColumn == null || sortableColumn.isEmpty()) {
			sortableColumn = "createdDate";
		}
		if (sortingType != null && sortingType.equalsIgnoreCase("ASC")) {
			query.with(new Sort(Sort.Direction.ASC, sortableColumn));
		} else {
			query.with(new Sort(Sort.Direction.DESC, sortableColumn));
		}
		if (connectionDataRequest.getType() != null && !connectionDataRequest.getType().isEmpty()) {
			query.addCriteria(Criteria.where("type").is(connectionDataRequest.getType()));
		}
		if (connectionDataRequest.getName() != null && !connectionDataRequest.getName().isEmpty()) {
			query.addCriteria(Criteria.where("name").is(connectionDataRequest.getName()));
		}
		if (connectionDataRequest.getAliasName() != null && !connectionDataRequest.getAliasName().isEmpty()) {
			query.addCriteria(Criteria.where("aliasName").is(connectionDataRequest.getAliasName()));
		}
		if (connectionDataRequest.getCreatedDate() != null && !connectionDataRequest.getCreatedDate().isEmpty()) {
			query.addCriteria(Criteria.where("createdDate").is(connectionDataRequest.getCreatedDate()));
		}
		if (connectionDataRequest.getUpdatedDate() != null && !connectionDataRequest.getUpdatedDate().isEmpty()) {
			query.addCriteria(Criteria.where("updatedDate").is(connectionDataRequest.getUpdatedDate()));
		}
		if (connectionDataRequest.getCreatedBy() != null && !connectionDataRequest.getCreatedBy().isEmpty()) {
			query.addCriteria(Criteria.where("createdBy").is(connectionDataRequest.getCreatedBy()));
		}
		if (connectionDataRequest.getUpdatedBy() != null && !connectionDataRequest.getUpdatedBy().isEmpty()) {
			query.addCriteria(Criteria.where("updatedBy").is(connectionDataRequest.getUpdatedBy()));
		}
		connectionEntityList = mongoTemplate.find(query, ConnectionEntity.class);
		return connectionEntityList;
	}

	@Override
	public Long countTotalRecord(ConnectionDataRequest connectionDataRequest, Integer startRecord, Integer recordCount,
			String sortableColumn, String sortingType) {
		Query query = new Query();
		if (sortableColumn == null || sortableColumn.isEmpty()) {
			sortableColumn = "createdDate";
		}
		if (sortingType != null && sortingType.equalsIgnoreCase("ASC")) {
			query.with(new Sort(Sort.Direction.ASC, sortableColumn));
		} else {
			query.with(new Sort(Sort.Direction.DESC, sortableColumn));
		}
		if (connectionDataRequest.getType() != null && !connectionDataRequest.getType().isEmpty()) {
			query.addCriteria(Criteria.where("type").is(connectionDataRequest.getType()));
		}
		if (connectionDataRequest.getName() != null && !connectionDataRequest.getName().isEmpty()) {
			query.addCriteria(Criteria.where("name").is(connectionDataRequest.getName()));
		}
		if (connectionDataRequest.getAliasName() != null && !connectionDataRequest.getAliasName().isEmpty()) {
			query.addCriteria(Criteria.where("aliasName").is(connectionDataRequest.getAliasName()));
		}
		if (connectionDataRequest.getCreatedDate() != null && !connectionDataRequest.getCreatedDate().isEmpty()) {
			query.addCriteria(Criteria.where("createdDate").is(connectionDataRequest.getCreatedDate()));
		}
		if (connectionDataRequest.getUpdatedDate() != null && !connectionDataRequest.getUpdatedDate().isEmpty()) {
			query.addCriteria(Criteria.where("updatedDate").is(connectionDataRequest.getUpdatedDate()));
		}
		if (connectionDataRequest.getCreatedBy() != null && !connectionDataRequest.getCreatedBy().isEmpty()) {
			query.addCriteria(Criteria.where("createdBy").is(connectionDataRequest.getCreatedBy()));
		}
		if (connectionDataRequest.getUpdatedBy() != null && !connectionDataRequest.getUpdatedBy().isEmpty()) {
			query.addCriteria(Criteria.where("updatedBy").is(connectionDataRequest.getUpdatedBy()));
		}
		Long totalRecordCount = mongoTemplate.count(query, ConnectionEntity.class);
		return totalRecordCount;
	}

	@Override
	public List<JobEntity> findAllJobs(JobRequest jobRequest, Integer startRecord, Integer recordCount,
			String sortableColumn, String sortingType, boolean isAll) {
		Query query = new Query();
		List<JobEntity> mapEntityList = new ArrayList<>();
		if (!isAll) {
			query.skip(startRecord);
			query.limit(recordCount);
		}
		if (sortableColumn == null || sortableColumn.isEmpty()) {
			sortableColumn = "createdDate";
		}
		if (sortingType != null && sortingType.equalsIgnoreCase("ASC")) {
			query.with(new Sort(Sort.Direction.ASC, sortableColumn));
		} else {
			query.with(new Sort(Sort.Direction.DESC, sortableColumn));
		}

		if (jobRequest.getJobId() != null && !jobRequest.getJobId().isEmpty()) {
			query.addCriteria(Criteria.where("jobId").is(jobRequest.getJobId()));
		}
		if (jobRequest.getJobName() != null && !jobRequest.getJobName().isEmpty()) {
			query.addCriteria(Criteria.where("jobName").is(jobRequest.getJobName()));
		}
		if (jobRequest.getSourceConnectionName() != null && !jobRequest.getSourceConnectionName().isEmpty()) {
			query.addCriteria(Criteria.where("sourceConnectionName").is(jobRequest.getSourceConnectionName()));
		}
		if (jobRequest.getDestinationConnectionName() != null && !jobRequest.getDestinationConnectionName().isEmpty()) {
			query.addCriteria(
					Criteria.where("destinationConnectionName").is(jobRequest.getDestinationConnectionName()));
		}
		if (jobRequest.getSourceCollection() != null && !jobRequest.getSourceCollection().isEmpty()) {
			query.addCriteria(Criteria.where("sourceCollection").is(jobRequest.getSourceCollection()));
		}
		if (jobRequest.getDestinationCollection() != null && !jobRequest.getDestinationCollection().isEmpty()) {
			query.addCriteria(Criteria.where("destinationCollection").is(jobRequest.getDestinationCollection()));
		}
		if (jobRequest.getMapping() != null && !jobRequest.getMapping().isEmpty()) {
			query.addCriteria(Criteria.where("mapping").is(jobRequest.getMapping()));
		}
		if (jobRequest.getStatus() != null && !jobRequest.getStatus().isEmpty()) {
			query.addCriteria(Criteria.where("status").is(jobRequest.getStatus()));
		}
		if (jobRequest.getCreatedBy() != null && !jobRequest.getCreatedBy().isEmpty()) {
			query.addCriteria(Criteria.where("createdBy").is(jobRequest.getCreatedBy()));
		}
		if (jobRequest.getUpdatedBy() != null && !jobRequest.getUpdatedBy().isEmpty()) {
			query.addCriteria(Criteria.where("updatedBy").is(jobRequest.getUpdatedBy()));
		}
		mapEntityList = mongoTemplate.find(query, JobEntity.class);
		return mapEntityList;
	}

	@Override
	public Long countTotalJobsRecord(JobRequest jobRequest, Integer startRecord, Integer recordCount,
			String sortableColumn, String sortingType) {
		Query query = new Query();
		if (sortableColumn == null || sortableColumn.isEmpty()) {
			sortableColumn = "createdDate";
		}
		if (sortingType != null && sortingType.equalsIgnoreCase("ASC")) {
			query.with(new Sort(Sort.Direction.ASC, sortableColumn));
		} else {
			query.with(new Sort(Sort.Direction.DESC, sortableColumn));
		}
		if (jobRequest.getJobName() != null && !jobRequest.getJobName().isEmpty()) {
			query.addCriteria(Criteria.where("jobName").is(jobRequest.getJobName()));
		}
		if (jobRequest.getSourceConnectionName() != null && !jobRequest.getSourceConnectionName().isEmpty()) {
			query.addCriteria(Criteria.where("sourceConnectionName").is(jobRequest.getSourceConnectionName()));
		}
		if (jobRequest.getDestinationConnectionName() != null && !jobRequest.getDestinationConnectionName().isEmpty()) {
			query.addCriteria(
					Criteria.where("destinationConnectionName").is(jobRequest.getDestinationConnectionName()));
		}
		if (jobRequest.getSourceCollection() != null && !jobRequest.getSourceCollection().isEmpty()) {
			query.addCriteria(Criteria.where("sourceCollection").is(jobRequest.getSourceCollection()));
		}
		if (jobRequest.getDestinationCollection() != null && !jobRequest.getDestinationCollection().isEmpty()) {
			query.addCriteria(Criteria.where("destinationCollection").is(jobRequest.getDestinationCollection()));
		}
		if (jobRequest.getMapping() != null && !jobRequest.getMapping().isEmpty()) {
			query.addCriteria(Criteria.where("mapping").is(jobRequest.getMapping()));
		}
		if (jobRequest.getCreatedBy() != null && !jobRequest.getCreatedBy().isEmpty()) {
			query.addCriteria(Criteria.where("createdBy").is(jobRequest.getCreatedBy()));
		}
		if (jobRequest.getUpdatedBy() != null && !jobRequest.getUpdatedBy().isEmpty()) {
			query.addCriteria(Criteria.where("updatedBy").is(jobRequest.getUpdatedBy()));
		}
		Long totalRecordCount = mongoTemplate.count(query, JobEntity.class);
		return totalRecordCount;
	}

	@Override
	public List<ConnectionEntity> findAll(ConnectionDataRequest connectionDataRequest) {
		Query query = new Query();
		List<ConnectionEntity> connectionEntityList = new ArrayList<>();
		if (connectionDataRequest.getType() != null && !connectionDataRequest.getType().isEmpty()) {
			query.addCriteria(Criteria.where("type").is(connectionDataRequest.getType()));
		}
		connectionEntityList = mongoTemplate.find(query, ConnectionEntity.class);
		return connectionEntityList;
	}

	@Override
	public List<JobEntity> findAllJobName(String connectionId) {
		Query query = new Query();
		List<JobEntity> connectionEntityList = new ArrayList<>();
		if (!connectionId.isEmpty()) {
			query.addCriteria(new Criteria().orOperator(Criteria.where("sourceConnection").is(connectionId),
					Criteria.where("destinationConnection").is(connectionId)));
		}
		connectionEntityList = mongoTemplate.find(query, JobEntity.class);
		return connectionEntityList;
	}

	@Override
	public Long countTotalRecord(String connectionId) {
		Query query = new Query();
		if (!connectionId.isEmpty()) {
			query.addCriteria(new Criteria().orOperator(Criteria.where("sourceConnection").is(connectionId),
					Criteria.where("destinationConnection").is(connectionId)));
		}
		Long totalRecordCount = mongoTemplate.count(query, JobEntity.class);
		return totalRecordCount;
	}

	public List<JobStatusEntity> findByJobId(String jobId, boolean isAll) {
		Query query = new Query();
		List<JobStatusEntity> jobStatusEntity = new ArrayList<JobStatusEntity>();
		if (!isAll) {
			query.skip(0);
			query.limit(1);
		}
		query.with(new Sort(Sort.Direction.DESC, "startDate"));
		if (jobId != null && !jobId.isEmpty()) {
			query.addCriteria(Criteria.where("jobId").is(jobId));
		}
		jobStatusEntity = mongoTemplate.find(query, JobStatusEntity.class);
		return jobStatusEntity;
	}

	public List<JobStatusEntity> findByStatus() {
		Query query = new Query();
		List<JobStatusEntity> jobStatusEntity = new ArrayList<JobStatusEntity>();
		query.skip(0);
		query.limit(1);
		query.with(new Sort(Sort.Direction.DESC, "startDate"));
		query.addCriteria(Criteria.where("status").is("In Process"));
		jobStatusEntity = mongoTemplate.find(query, JobStatusEntity.class);
		return jobStatusEntity;
	}

	public List<JobStatusEntity> findByIdAndStatus(String jobId) {
		Query query = new Query();
		List<JobStatusEntity> jobStatusEntity = new ArrayList<JobStatusEntity>();
		query.with(new Sort(Sort.Direction.DESC, "startDate"));
		if (jobId != null && !jobId.isEmpty()) {
			query.addCriteria(Criteria.where("jobId").is(jobId));
		}
		query.addCriteria(Criteria.where("status").is("In Process"));
		jobStatusEntity = mongoTemplate.find(query, JobStatusEntity.class);
		return jobStatusEntity;
	}

	@Override
	public List<JobAuditEntity> findByStatusId(String id) {
		Query query = new Query();
		List<JobAuditEntity> jobAuditEntityList = new ArrayList<JobAuditEntity>();
		if (id != null && !id.isEmpty()) {
			query.addCriteria(Criteria.where("jobStatusId").is(id));
		}
		jobAuditEntityList = mongoTemplate.find(query, JobAuditEntity.class);
		return jobAuditEntityList;
	}
}
