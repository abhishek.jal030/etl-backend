package com.datasync.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.datasync.entity.JobAuditEntity;

public interface JobAuditRepository extends MongoRepository<JobAuditEntity, String> {


}
