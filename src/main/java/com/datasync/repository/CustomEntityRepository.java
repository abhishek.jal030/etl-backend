package com.datasync.repository;

import java.util.List;

import com.datasync.entity.ConnectionEntity;
import com.datasync.entity.JobAuditEntity;
import com.datasync.entity.JobEntity;
import com.datasync.entity.JobStatusEntity;
import com.datasync.request.ConnectionDataRequest;
import com.datasync.request.JobRequest;

public interface CustomEntityRepository {

	public List<ConnectionEntity> findAllConnection(ConnectionDataRequest connectionDataRequest, Integer startRecord,
			Integer recordCount, String sortableColumn, String sortingType);

	public Long countTotalRecord(ConnectionDataRequest connectionDataRequest, Integer startRecord, Integer recordCount,
			String sortableColumn, String sortingType);

	public List<JobEntity> findAllJobs(JobRequest jobRequest, Integer startRecord, Integer recordCount,
			String sortableColumn, String sortingType, boolean isAll);

	public Long countTotalJobsRecord(JobRequest jobRequest, Integer startRecord, Integer recordCount,
			String sortableColumn, String sortingType);

	public List<ConnectionEntity> findAll(ConnectionDataRequest connectionDataRequest);

	public List<JobEntity> findAllJobName(String connectionId);

	public Long countTotalRecord(String connectionId);

	public List<JobStatusEntity> findByJobId(String jobId, boolean isAll);

	public List<JobStatusEntity> findByStatus();

	public List<JobStatusEntity> findByIdAndStatus(String jobId);

	public List<JobAuditEntity> findByStatusId(String jobStatusId);
}
