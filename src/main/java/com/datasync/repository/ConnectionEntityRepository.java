package com.datasync.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.datasync.entity.ConnectionEntity;

public interface ConnectionEntityRepository extends MongoRepository<ConnectionEntity, String> {

	List<ConnectionEntity> findByName(String name);

	List<ConnectionEntity> findByType(String type);
}
