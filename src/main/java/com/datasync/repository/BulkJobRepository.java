package com.datasync.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.datasync.entity.BulkJobEntity;

public interface BulkJobRepository extends MongoRepository<BulkJobEntity, String> {
	
	List<BulkJobEntity> findByName(String name);

	List<BulkJobEntity> findByPrefix(String prefix);
}
