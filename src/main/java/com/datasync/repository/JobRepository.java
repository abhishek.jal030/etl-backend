package com.datasync.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.datasync.entity.JobEntity;

public interface JobRepository extends MongoRepository<JobEntity, String> {

	List<JobEntity> findByJobName(String jobName);

}
