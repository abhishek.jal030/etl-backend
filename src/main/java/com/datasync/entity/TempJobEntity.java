package com.datasync.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "TEMP_JOBS")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TempJobEntity {

	@Id
	private String id;
	private String jobId;
	private String newMapping;
	private boolean isArchived;

}
