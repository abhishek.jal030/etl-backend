package com.datasync.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "CONNECTION")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ConnectionEntity {

	@Id
	private String connectionId;
	private String type;
	private String name;
	private String aliasName;
	private Map<String, String> connectionConfig = new HashMap<>();
	private Date createdDate;
	private Date updatedDate;
	private String createdBy;
	private String updatedBy;
	
}
