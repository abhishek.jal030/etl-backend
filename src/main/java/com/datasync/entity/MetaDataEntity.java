package com.datasync.entity;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "METADATA")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MetaDataEntity {
	
	@Id
	private String id;
	private String connectionId;
	private String mataData;
	private Map<String, Long> collectionCount = new HashMap<>();

}
