package com.datasync.entity;

import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "CONNECTORTYPE")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ConnectorTypeEntity {

	@Id
	private String id;
	private String connectorId;
	private String connectorType;
	private String image;
}