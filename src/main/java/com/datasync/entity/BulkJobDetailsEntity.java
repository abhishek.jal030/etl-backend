package com.datasync.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "BULKJOBDETAILS")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BulkJobDetailsEntity {

	@Id
	private String id;
	private String bulkJobId;
	private String jobId;
	private String jobName;
	private String collectionName;
	private String status;
	private String message;
}
