package com.datasync.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "JOBSTATUS")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JobStatusEntity {

	@Id
	private String id;
	private String jobId;
	private String status;
	private Long totalRecordCount;
	private Long totalRecordProcessed;
	private Integer failedCount;
	private Date startDate;
	private Date endDate;
	private String startedBy;
	private String updatedBy;

}
