package com.datasync.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "JOBAUDIT")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JobAuditEntity {

	@Id
	private String id;
	private String jobId;
	private String jobStatusId;
	private Long skipRecordCount;
	private String status;
	private String message;
	private Date updatedDate;

}
