package com.datasync.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "JOBDETAILS")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JobDetailsEntity {

	@Id
	private String id;
	private String jobId;
	private String sourceConnection;
	private String destinationConnection;
	private String sourceConnectionName;
	private String destinationConnectionName;
	private String sourceCollection;
	private String destinationCollection;
	private String mapping;
	private String jobFilter;
	private String incrementalParameter;
	private Date incrementalParameterValue;
	private boolean isParent;
	private boolean isSelected;
	private Date createdDate;
	private Date updatedDate;
	private String createdBy;
	private String updatedBy;

}
