package com.datasync.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "BULKJOBS")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BulkJobEntity {

	@Id
	private String id;
	private String name;
	private String sourceConnection;
	private String destinationConnection;
	private String sourceConnectionName;
	private String destinationConnectionName;
	private String collectionNames;
	private String prefix;
	private Date  scheduleDateTime;
	private Date createdDate;
	private Date updatedDate;
	private String createdBy;
	private String updatedBy;
	

}
