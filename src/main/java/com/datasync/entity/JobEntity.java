package com.datasync.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "JOBS")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JobEntity {

	@Id
	private String jobId;
	private String jobName;
	private String sourceConnection;
	private String destinationConnection;
	private String sourceConnectionName;
	private String destinationConnectionName;
	private String sourceCollection;
	private String sourceCollectionType;
	private String destinationCollection;
	private String mapping;
	private String jobFilter;
	private String incrementalParameter;
	private Date incrementalParameterValue;
	private String emails;
	private String status;
	private Date lastRun;
	private boolean isStopped;
	private Date createdDate;
	private Date updatedDate;
	private String createdBy;
	private String updatedBy;

}
