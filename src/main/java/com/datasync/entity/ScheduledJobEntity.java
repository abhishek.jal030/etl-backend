package com.datasync.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Document(collection = "SCHEDULEDJOBS")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ScheduledJobEntity {
	
	@Id
	private String id;
	private String jobId;
	private Date jobStartDate;
	private Date jobEndDate;
	private String cronExpression;
	private String createdBy;
	private String updatedBy;

}
