/**
 * 
 */
package com.datasync.util;

import java.text.SimpleDateFormat;

/**
 * @author Shubham
 *
 */
public enum DataSyncUtilityMessage {

	PST_TIME_ZONE("PST"), DATE_FORMAT("dd MMM yyyy hh:mm:ss a");

	public static final String HTTP_METHOD_PUT = "PUT";
	public static final String HTTP_METHOD_POST = "POST";
	public static final String HTTP_METHOD_GET = "GET";
	public static final String HTTP_METHOD_PATCH = "PATCH";

	public static final String TIME_STAMP = "MM/dd/yyyy HH:mm:ss";

	public static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
	public static final String HTTP_HEADER_ACCEPT = "Accept";

	public static final String APPLICATION_JSON = "application/json";
	public static final String APPLICATION_XML = "application/xml";
	public static final String APPLICATION_ATOM_XML = "application/atom+xml";
	public static final String METADATA = "$metadata";
	public static final String COUNT = "$count";
	public static final String SEPARATOR = "/";
	public static final String AUTHORIZATION_HEADER = "Authorization";
	public static final String CSRF_TOKEN_HEADER = "X-CSRF-Token";
	public static final String CSRF_TOKEN_FETCH = "Fetch";
	public static final String C4C_TENANT = "C4C_TENANT";

	// Table suffix
	public static final String C4C_TABLE_PREFIX = "RE_";
	public static final String C4C_TABLE_LOWER_SUFFIX = "Collection";
	public static final String C4C_TABLE_UPPER_SUFFIX = "COLLECTION";

	public static final String ENTITY_TYPE_CODE_CONTEXTUAL_LIST = "ContextualCodeList";

	public static final SimpleDateFormat C4CTimestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	public static final String ORACLE_DRIVER = "oracle.jdbc.OracleDriver";

	public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";

	public static final String THREAD_POOL_SIZE = "10";

	public static final String serviceName = "serviceName";
	public static final String port = "port";
	public static final String host = "host";
	public static final String userName = "userName";
	public static final String password = "password";
	public static final String url = "url";
	public static final String schemaName = "schemaName";

	public static final String procedure = "PROCEDURE";
	public static final String table = "TABLE";

	public static final String primaryKey = "ObjectID";
	public static final String UUID = "UUID";

	public static final String initiated = "Initiated";
	public static final String inProcess = "In Process";
	public static final String failed = "Failed";
	public static final String completed = "Completed";
	public static final String newString = "New";
	public static final String deleted = "Deleted";
	public static final String completedWithErrors = "Job Completed With Errors";
	public static final String oracle = "ORACLE";
	public static final String mysql = "MYSQL";
	public static final String odata = "ODATA";
	public static final String api = "API";
	public static final String datahug = "DATAHUG";
	public static final String excel = "EXCEL";
	public static final String messages = "Test Connection Successfully";
	public static final String logicalOperator = "logicalOperator";
	public static final String comparisonOperator = "comparisonOperator";
	public static final String dataType = "dataType";
	public static final String value = "value";
	public static final String datetimeoffset = "datetimeoffset";
	public static final String datetime = "datetime";
	public static final String bool = "boolean";
	public static final String field = "field";

	private String message;

	DataSyncUtilityMessage() {
	}

	DataSyncUtilityMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

}
