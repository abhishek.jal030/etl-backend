package com.datasync.util;

public class C4cODataClientUtil {

	public static String checkIdentifierLenForTable(String identifier){
		identifier = identifier.replaceAll(DataSyncUtilityMessage.C4C_TABLE_LOWER_SUFFIX, "");
		identifier = identifier.replaceAll(DataSyncUtilityMessage.C4C_TABLE_UPPER_SUFFIX, "");
		int len = identifier.length();
		if(len>27){
			identifier = DataSyncUtilityMessage.C4C_TABLE_PREFIX.concat(identifier.substring(0, 12).concat(identifier.substring(len-14, len)));
		}else {
			identifier = DataSyncUtilityMessage.C4C_TABLE_PREFIX.concat(identifier);
		}
	return identifier;
	}
	
	public static String checkIdentifierLength(String identifier){
		identifier = removeOracleReservedKeywords(identifier);
		int len = identifier.length();
		if(len>30){
			identifier = identifier.substring(0, 14).concat(identifier.substring(len-15, len));
		}
	return identifier;
	}
	
	public static String removeOracleReservedKeywords(String identifier){
		StringBuffer changedString = null;
		if(identifier.equalsIgnoreCase("LEVEL")){
			changedString = new StringBuffer("C4C");
			changedString.append(identifier);
		}
	return 	(changedString==null)?identifier:changedString.toString();
	}
}
