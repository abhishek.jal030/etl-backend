package com.datasync.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.datasync.entity.BulkJobEntity;
import com.datasync.entity.JobEntity;
import com.datasync.entity.JobStatusEntity;
import com.datasync.entity.TempJobEntity;
import com.datasync.response.BulkJobResponse;
import com.datasync.response.JobDetailsResponse;
import com.datasync.response.JobProgressResponse;
import com.datasync.response.JobResponse;
import com.datasync.response.TempJobResponse;

public class JobsUtil {

	public static JobResponse buildResponseAllMapsDetail(JobEntity jobEntity) {
		JobResponse jobResponse = new JobResponse();
		jobResponse.setJobId(jobEntity.getJobId());
		jobResponse.setJobName(jobEntity.getJobName());
		jobResponse.setDestinationConnectionName(jobEntity.getDestinationConnectionName());
		jobResponse.setSourceConnectionName(jobEntity.getSourceConnectionName());
		jobResponse.setDestinationCollection(jobEntity.getDestinationCollection());
		jobResponse.setSourceCollection(jobEntity.getSourceCollection());
		jobResponse.setMapping(jobEntity.getMapping());
		if (jobEntity.getJobFilter() == null || jobEntity.getJobFilter().isEmpty()) {
			jobResponse.setJobFilter("");
		} else {
			jobResponse.setJobFilter(jobEntity.getJobFilter());
		}

		jobResponse.setIncrementalParameter(jobEntity.getIncrementalParameter());
		jobResponse.setStatus(jobEntity.getStatus());
		jobResponse.setCreatedBy(jobEntity.getCreatedBy());
		jobResponse.setUpdatedBy(jobEntity.getUpdatedBy());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date createdDateParsedDate = new Date();
		Date updatedDateParsedDate = new Date();
		Date incrementalParameterValue = new Date();
		Date lastRun = new Date();
		try {
			createdDateParsedDate = dateFormat.parse(dateFormat.format(jobEntity.getCreatedDate()));
			updatedDateParsedDate = dateFormat.parse(dateFormat.format(jobEntity.getUpdatedDate()));
			if (jobEntity.getIncrementalParameterValue() != null) {
				incrementalParameterValue = dateFormat
						.parse(dateFormat.format(jobEntity.getIncrementalParameterValue()));
				Timestamp incrementalDateTime = new java.sql.Timestamp(incrementalParameterValue.getTime());
				jobResponse.setIncrementalParameterValue(incrementalDateTime.toString());
			}
			if (jobEntity.getLastRun() != null) {
				lastRun = dateFormat.parse(dateFormat.format(jobEntity.getLastRun()));
				Timestamp lastDateTime = new java.sql.Timestamp(lastRun.getTime());
				jobResponse.setLastRun(lastDateTime.toString());
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}
		Timestamp createdDateTime = new java.sql.Timestamp(createdDateParsedDate.getTime());
		jobResponse.setCreatedDate(createdDateTime.toString());
		Timestamp updatedDateTime = new java.sql.Timestamp(updatedDateParsedDate.getTime());
		jobResponse.setUpdatedDate(updatedDateTime.toString());
		return jobResponse;
	}

	public static JobDetailsResponse buildResponseAllMapsDetail(List<JobEntity> jobEntityList, Long totalRecordCount) {
		JobDetailsResponse jobDetailsResponse = new JobDetailsResponse();
		List<JobResponse> jobResponseList = new ArrayList<JobResponse>();
		if (!jobEntityList.isEmpty()) {
			for (int i = 0; i < jobEntityList.size(); i++) {
				JobResponse jobResponse = new JobResponse();
				jobResponse
						.setJobName(jobEntityList.get(i).getJobName() != null ? jobEntityList.get(i).getJobName() : "");
				jobResponse.setJobId(jobEntityList.get(i).getJobId() != null ? jobEntityList.get(i).getJobId() : "");
				jobResponse.setDestinationConnectionName(jobEntityList.get(i).getDestinationConnectionName() != null
						? jobEntityList.get(i).getDestinationConnectionName()
						: "");
				jobResponse.setSourceConnectionName(jobEntityList.get(i).getSourceConnectionName() != null
						? jobEntityList.get(i).getSourceConnectionName()
						: "");
				jobResponse.setDestinationCollection(jobEntityList.get(i).getDestinationCollection() != null
						? jobEntityList.get(i).getDestinationCollection()
						: "");
				jobResponse.setSourceCollection(
						jobEntityList.get(i).getSourceCollection() != null ? jobEntityList.get(i).getSourceCollection()
								: "");
				jobResponse.setStatus(jobEntityList.get(i).getStatus() != null ? jobEntityList.get(i).getStatus() : "");
				jobResponse
						.setMapping(jobEntityList.get(i).getMapping() != null ? jobEntityList.get(i).getMapping() : "");
				jobResponse.setCreatedBy(
						jobEntityList.get(i).getCreatedBy() != null ? jobEntityList.get(i).getCreatedBy() : "");
				jobResponse.setUpdatedBy(
						jobEntityList.get(i).getUpdatedBy() != null ? jobEntityList.get(i).getUpdatedBy() : "");

				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				Date createdDateParsedDate = new Date();
				Date updatedDateParsedDate = new Date();
				Date lastRun = new Date();
				try {
					createdDateParsedDate = dateFormat.parse(dateFormat.format(jobEntityList.get(i).getCreatedDate()));
					updatedDateParsedDate = dateFormat.parse(dateFormat.format(jobEntityList.get(i).getUpdatedDate()));
					if (jobEntityList.get(i).getLastRun() != null) {
						lastRun = dateFormat.parse(dateFormat.format(jobEntityList.get(i).getLastRun()));
						Timestamp lastDateTime = new java.sql.Timestamp(lastRun.getTime());
						jobResponse.setLastRun(lastDateTime.toString());
					} else {
						jobResponse.setLastRun("");
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Timestamp createdDateTime = new java.sql.Timestamp(createdDateParsedDate.getTime());
				jobResponse.setCreatedDate(createdDateTime.toString());
				Timestamp updatedDateTime = new java.sql.Timestamp(updatedDateParsedDate.getTime());
				jobResponse.setUpdatedDate(updatedDateTime.toString());
				jobResponseList.add(jobResponse);

			}

			jobDetailsResponse.setJobResponseList(jobResponseList);
			jobDetailsResponse.setTotalRecordCount(totalRecordCount);
		}
		return jobDetailsResponse;

	}

	public static JobProgressResponse buildResponseJobProgress(JobStatusEntity jobStatusEntity) {
		JobProgressResponse jobProgressResponse = new JobProgressResponse();
		jobProgressResponse.setJobId(jobStatusEntity.getJobId());
		jobProgressResponse.setTotalRecordCount(jobStatusEntity.getTotalRecordCount());
		jobProgressResponse.setProcessedRecord(jobStatusEntity.getTotalRecordProcessed());
		jobProgressResponse.setStatus(jobStatusEntity.getStatus());
		return jobProgressResponse;
	}

	public static JobProgressResponse[] buildAllResponseJobProgress(List<JobStatusEntity> jobStatusEntityList) {
		List<JobProgressResponse> jobProgressResponseList = new ArrayList<JobProgressResponse>();
		if (jobStatusEntityList != null && jobStatusEntityList.size() != 0) {
			for (int i = 0; i < jobStatusEntityList.size(); i++) {
				JobProgressResponse jobProgressResponse = new JobProgressResponse();
				jobProgressResponse.setJobId(jobStatusEntityList.get(i).getJobId());
				jobProgressResponse.setTotalRecordCount(jobStatusEntityList.get(i).getTotalRecordCount());
				jobProgressResponse.setProcessedRecord(jobStatusEntityList.get(i).getTotalRecordProcessed());
				jobProgressResponse.setStatus(jobStatusEntityList.get(i).getStatus());
				jobProgressResponseList.add(jobProgressResponse);
			}
		}
		return jobProgressResponseList.toArray(new JobProgressResponse[jobProgressResponseList.size()]);
	}

	public static JobResponse buildResponseAllconnectionJobDetails(JobEntity jobEntity) {
		JobResponse jobResponse = new JobResponse();
		jobResponse.setJobName(jobEntity.getJobName());
		jobResponse.setDestinationConnectionName(jobEntity.getDestinationConnectionName());
		jobResponse.setSourceConnectionName(jobEntity.getSourceConnectionName());
		jobResponse.setDestinationCollection(jobEntity.getDestinationCollection());
		jobResponse.setSourceCollection(jobEntity.getSourceCollection());
		jobResponse.setMapping(jobEntity.getMapping());
		jobResponse.setCreatedBy(jobEntity.getCreatedBy());
		jobResponse.setUpdatedBy(jobEntity.getUpdatedBy());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date createdDateParsedDate = new Date();
		Date updatedDateParsedDate = new Date();
		try {
			createdDateParsedDate = dateFormat.parse(dateFormat.format(jobEntity.getCreatedDate()));
			updatedDateParsedDate = dateFormat.parse(dateFormat.format(jobEntity.getUpdatedDate()));
		}

		catch (ParseException e) {
			e.printStackTrace();
		}
		Timestamp createdDateTime = new java.sql.Timestamp(createdDateParsedDate.getTime());
		jobResponse.setCreatedDate(createdDateTime.toString());
		Timestamp updatedDateTime = new java.sql.Timestamp(updatedDateParsedDate.getTime());
		jobResponse.setUpdatedDate(updatedDateTime.toString());

		return jobResponse;
	}

	public static TempJobResponse buildTempjobResponse(TempJobEntity tempJobEntity) {
		TempJobResponse tempJobResponse = new TempJobResponse();
		tempJobResponse.setJobId(tempJobEntity.getJobId());
		tempJobResponse.setNewMapping(tempJobEntity.getNewMapping());
		return tempJobResponse;
	}
	
	public static BulkJobResponse buildResponseBulkJobDetail(BulkJobEntity bulkJobEntity) {
		BulkJobResponse bulkJobResponse = new BulkJobResponse();
		bulkJobResponse.setId(bulkJobEntity.getId());
		bulkJobResponse.setName(bulkJobEntity.getName());
		bulkJobResponse.setDestinationConnectionName(bulkJobEntity.getDestinationConnectionName());
		bulkJobResponse.setSourceConnectionName(bulkJobEntity.getSourceConnectionName());
		bulkJobResponse.setCollectionNames(bulkJobEntity.getCollectionNames());
		bulkJobResponse.setCreatedBy(bulkJobEntity.getCreatedBy());
		bulkJobResponse.setUpdatedBy(bulkJobEntity.getUpdatedBy());
		bulkJobResponse.setPrefix(bulkJobEntity.getPrefix());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date createdDateParsedDate = new Date();
		Date updatedDateParsedDate = new Date();
		try {
			createdDateParsedDate = dateFormat.parse(dateFormat.format(bulkJobEntity.getCreatedDate()));
			updatedDateParsedDate = dateFormat.parse(dateFormat.format(bulkJobEntity.getUpdatedDate()));

		} catch (ParseException e) {
			e.printStackTrace();
		}
		Timestamp createdDateTime = new java.sql.Timestamp(createdDateParsedDate.getTime());
		bulkJobResponse.setCreatedDate(createdDateTime.toString());
		Timestamp updatedDateTime = new java.sql.Timestamp(updatedDateParsedDate.getTime());
		bulkJobResponse.setUpdatedDate(updatedDateTime.toString());
		return bulkJobResponse;
	}
}
