package com.datasync.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.datasync.entity.ConnectionEntity;
import com.datasync.response.ConnectionDetailsResponse;
import com.datasync.response.ConnectionResponse;

public class ConnectionUtill {

	private static Logger logger = LogManager.getLogger(ConnectionUtill.class);

	public static ConnectionResponse buildResponseConnectionDetail(ConnectionEntity connectionEntity) {
		logger.debug("Entering to com.datasync.util.buildResponseConnectionDetail()");
		ConnectionResponse connectionDetailsResponse = new ConnectionResponse();
		connectionDetailsResponse.setConnectionId(connectionEntity.getConnectionId());
		connectionDetailsResponse.setType(connectionEntity.getType());
		connectionDetailsResponse.setName(connectionEntity.getName());
		connectionDetailsResponse.setAliasName(connectionEntity.getAliasName());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date createdDateParsedDate = new Date();
		Date updatedDateParsedDate = new Date();
		try {
			createdDateParsedDate = dateFormat.parse(dateFormat.format(connectionEntity.getCreatedDate()));
			updatedDateParsedDate = dateFormat.parse(dateFormat.format(connectionEntity.getUpdatedDate()));
		} catch (ParseException e) {
			logger.error(e);
		}
		Timestamp createdDateTime = new java.sql.Timestamp(createdDateParsedDate.getTime());
		connectionDetailsResponse.setCreatedDate(createdDateTime.toString());
		Timestamp updatedDateTime = new java.sql.Timestamp(updatedDateParsedDate.getTime());
		connectionDetailsResponse.setUpdatedDate(updatedDateTime.toString());
		connectionDetailsResponse.setUpdatedBy(connectionEntity.getUpdatedBy());
		connectionDetailsResponse.setCreatedBy(connectionEntity.getCreatedBy());
		if (connectionEntity.getConnectionConfig() != null) {
			Map<String, String> connectionConfig = new HashMap<>();
			for (Entry<String, String> entry : connectionEntity.getConnectionConfig().entrySet()) {
				connectionConfig.put(entry.getKey(), entry.getValue());
				connectionDetailsResponse.setConnectionConfigRequestList(connectionConfig);
			}
		}
		logger.debug("Exiting to com.datasync.util.buildResponseConnectionDetail()");
		return connectionDetailsResponse;
	}

	public static ConnectionDetailsResponse buildResponseAllConnectionDetail(
			List<ConnectionEntity> connectionEntityList, Long totalRecordCount) {
		logger.debug("Entering to com.datasync.util.buildResponseAllConnectionDetail()");
		ConnectionDetailsResponse connectionDetailsResponse = new ConnectionDetailsResponse();
		List<ConnectionResponse> connectionResponseList = new ArrayList<ConnectionResponse>();
		if (!connectionEntityList.isEmpty()) {
			connectionEntityList.forEach(ConnectionEntity -> {
				ConnectionResponse connectionResponse = new ConnectionResponse();
				connectionResponse.setConnectionId(ConnectionEntity.getConnectionId());
				connectionResponse.setType(ConnectionEntity.getType());
				connectionResponse.setName(ConnectionEntity.getName());
				connectionResponse.setAliasName(ConnectionEntity.getAliasName());
				if (ConnectionEntity.getConnectionConfig() != null) {
					Map<String, String> connectionConfig = new HashMap<>();
					for (Entry<String, String> entry : ConnectionEntity.getConnectionConfig().entrySet()) {
						connectionConfig.put(entry.getKey(), entry.getValue());
						connectionResponse.setConnectionConfigRequestList(connectionConfig);
					}
				}

				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				Date createdDateParsedDate = new Date();
				Date updatedDateParsedDate = new Date();
				try {
					createdDateParsedDate = dateFormat.parse(dateFormat.format(ConnectionEntity.getCreatedDate()));
					updatedDateParsedDate = dateFormat.parse(dateFormat.format(ConnectionEntity.getUpdatedDate()));
				} catch (ParseException e) {
					logger.error(e);
				}
				Timestamp createdDateTime = new java.sql.Timestamp(createdDateParsedDate.getTime());
				connectionResponse.setCreatedDate(createdDateTime.toString());
				Timestamp updatedDateTime = new java.sql.Timestamp(updatedDateParsedDate.getTime());
				connectionResponse.setUpdatedDate(updatedDateTime.toString());
				connectionResponse.setUpdatedBy(ConnectionEntity.getUpdatedBy());
				connectionResponse.setCreatedBy(ConnectionEntity.getCreatedBy());
				connectionResponseList.add(connectionResponse);
				connectionDetailsResponse.setConnectionResponseList(connectionResponseList);
				connectionDetailsResponse.setTotalRecordCount(totalRecordCount);

			});

		}
		logger.debug("Exiting to com.datasync.util.buildResponseAllConnectionDetail()");
		return connectionDetailsResponse;
	}

}