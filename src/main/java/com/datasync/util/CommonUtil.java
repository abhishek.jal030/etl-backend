package com.datasync.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Class CommonUtil.
 */

@Service
public class CommonUtil {

	/** The logger. */
	private static Logger logger = LogManager.getLogger(CommonUtil.class);

	/**
	 * Gets the elapsed time.
	 *
	 * @param startTime the start time
	 * @return the elapsed time
	 */
	public static long getElapsedTime(long startTime) {
		logger.debug("Entering to com.netapp.util.CommonUtil.getElapsedTime()");
		long elapsedSeconds = 0l;
		try {
			long elapsedTime = System.currentTimeMillis() - startTime;
			elapsedSeconds = elapsedTime / 1000;
		} catch (Exception e) {
			logger.error("Exception in com.netapp.util.CommonUtil.getElapsedTime():: " + e);
			throw e;
		}
		logger.debug("Exiting from com.netapp.util.CommonUtil.getElapsedTime()");
		return elapsedSeconds;
	}

	/**
	 * Utility function to convert java Date to TimeZone format.
	 *
	 * @param date     the date
	 * @param format   the format
	 * @param timeZone the time zone
	 * @return the date on time zone
	 * @throws Exception the exception
	 */
	public static Date getDateOnTimeZone(Date date, String format, String timeZone) throws Exception {
		logger.debug("Entering to com.netapp.util.CommonUtil.getDateOnTimeZone()");
		SimpleDateFormat sdf = null;
		try {
			sdf = new SimpleDateFormat(format);
			if (timeZone == null || "".equalsIgnoreCase(timeZone.trim())) {
				timeZone = Calendar.getInstance().getTimeZone().getID();
			}
			sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		} catch (Exception e) {
			logger.error("Exception in com.netapp.util.CommonUtil.getDateOnTimeZone():: " + e);
			throw e;
		}
		logger.debug("Exiting from com.netapp.util.CommonUtil.getDateOnTimeZone()");
		return getDateFromString(format, sdf.format(date));
	}

	public static Date getCurrentDateOnPSTTimeZone() throws Exception {
		logger.debug("Entering to com.netapp.util.CommonUtil.getDateOnTimeZone()");
		Date date = null;
		try {
			date = getDateOnTimeZone(new Date(), DataSyncUtilityMessage.DATE_FORMAT.getMessage(),
					DataSyncUtilityMessage.PST_TIME_ZONE.getMessage());
		} catch (Exception e) {
			logger.error("Exception in com.netapp.util.CommonUtil.getDateOnTimeZone():: " + e);
			throw e;
		}
		logger.debug("Exiting from com.netapp.util.CommonUtil.getDateOnTimeZone()");
		return date;
	}

	/**
	 * Gets the date from string.
	 *
	 * @param pattern the pattern
	 * @param date    the date
	 * @return the date from string
	 * @throws Exception the exception
	 */
	public static Date getDateFromString(String pattern, String date) throws Exception {
		logger.debug("Entering to com.netapp.util.CommonUtil.getDateFromString()");
		DateFormat df = null;
		Date dateObj = null;
		try {
			df = new SimpleDateFormat(pattern);
			dateObj = df.parse(date);
		} catch (Exception e) {
			logger.error("Exception in com.netapp.util.CommonUtil.getDateFromString():: " + e);
			throw e;
		}
		logger.debug("Exiting from com.netapp.util.CommonUtil.getDateFromString()");
		return dateObj;
	}

	public static final String EMPTY_STRING = "";

	public static String convertToJson(Object obj) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(obj);
		return json;
	}

}
