package com.datasync.util;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

import com.datasync.response.responsemanager.ApiError;
import com.datasync.response.responsemanager.ApiResponse;
import com.datasync.response.responsemanager.ApiResult;


public class ResponseUtil {


	public static ApiResponse buildSuccessResponse() {
		ApiResponse response = new ApiResponse();
		response.setSuccess(true);
		return response;
	}
	
	public static ApiResponse buildSuccessResponse(ApiResult... results) {
		ApiResponse response = new ApiResponse();
		response.setSuccess(true);

		if (results!=null) {
			response.setResults(Arrays.asList(results));
		}
		return response;
	}
	

	public static ApiResponse buildErrorResponse(ApiError... errors) {
		ApiResponse response = new ApiResponse();
		response.setSuccess(false);

		if (ArrayUtils.isNotEmpty(errors)) {
			response.setErrors(Arrays.asList(errors));
		}
		return response;
	}
	
	public static ApiResponse buildErrorResponse(ApiResult... results) {
		ApiResponse response = new ApiResponse();
		response.setSuccess(false);

		if (results!=null) {
			response.setResults(Arrays.asList(results));
		}
		return response;
	}

	public static ApiResponse buildNotFoundResponse() {
		ApiError badRequestError = new ApiError("404","Error");

		return buildErrorResponse(badRequestError);
	}
	
	public static ApiResponse buildErrorResponse() {
		ApiResponse response = new ApiResponse();
		response.setSuccess(false);
		return response;

	}
}
