package com.datasync.util;

/**
 * @author Karnesh
 *
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.datasync.response.ExcelResponse;

public class ExcelUtil {

	@SuppressWarnings("unchecked")
	public static ExcelResponse jsonExcelExport(Map<String, Object> jsonResponse) throws IOException, SQLException {
		ExcelResponse excelResponse = new ExcelResponse();
		try (SXSSFWorkbook workbook = new SXSSFWorkbook(100);
				ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("Export Details");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLUE.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			Row header = sheet.createRow(0);

			CellStyle ageCellStyle = workbook.createCellStyle();
			ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));

			int rowCount = 1;
			if (jsonResponse != null) {
				for (Map.Entry<String, Object> entry : jsonResponse.entrySet()) {
					if (entry.getValue() instanceof List<?>) {
						List<Map<String, Object>> objectList = (ArrayList<Map<String, Object>>) entry.getValue();
						for (int i = 0; i < 1; i++) {
							// Header
							int j = 0;
							Map<String, Object> sortObject = new LinkedHashMap<>();
							objectList.get(i).entrySet().stream().sorted(Map.Entry.comparingByKey())
									.forEachOrdered(x -> sortObject.put(x.getKey(), x.getValue()));
							header = sheet.createRow(0);
							for (Map.Entry<String, Object> entry1 : sortObject.entrySet()) {
								header.createCell(j).setCellValue(entry1.getKey());
								header.getCell(j).setCellStyle(headerCellStyle);
								j++;
							}
						}
						for (Map<String, Object> object : objectList) {
							Map<String, Object> sortObject = new LinkedHashMap<>();
							object.entrySet().stream().sorted(Map.Entry.comparingByKey())
									.forEachOrdered(x -> sortObject.put(x.getKey(), x.getValue()));
							Row userRow = sheet.createRow(rowCount++);
							int j = 0;
							for (Map.Entry<String, Object> entry1 : sortObject.entrySet()) {
								if (entry1.getValue() != null) {
									if (entry1.getValue() instanceof Double) {
										userRow.createCell(j)
												.setCellValue(Double.valueOf(entry1.getValue().toString()));
									} else {
										userRow.createCell(j).setCellValue(entry1.getValue().toString());
									}
								} else {
									userRow.createCell(j).setCellValue("");
								}
								j++;
							}
						}
						workbook.write(out);
						workbook.close();
						workbook.dispose();
						excelResponse.setByteInputStream(new ByteArrayInputStream(out.toByteArray()));
						excelResponse.setSize(objectList.size());
					}
				}
			}
			return excelResponse;
		}
	}
}
