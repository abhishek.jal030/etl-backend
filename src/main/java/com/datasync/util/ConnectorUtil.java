package com.datasync.util;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.datasync.entity.ConnectorTypeEntity;
import com.datasync.response.ConnectionTestResponse;
import com.datasync.response.ConnectorTypeResponse;

public class ConnectorUtil {

	private static Logger logger = LogManager.getLogger(ConnectorUtil.class);

	public static ConnectionTestResponse buildTestConnectorResponse(Connection conn) {
		logger.debug("Entering to com.datasync.util.buildTestConnectorResponse()");
		ConnectionTestResponse connectionTestResponse = new ConnectionTestResponse();
		if (conn != null) {
			connectionTestResponse.setStatus(true);
			connectionTestResponse.setMessage("Test Connection Successfully");

		}
		logger.debug("Exiting to com.datasync.util.buildTestConnectorResponse()");
		return connectionTestResponse;
	}

	public static ConnectorTypeResponse[] buildResponseConnectorTypeDetail(List<ConnectorTypeEntity> connectorTypeList) {
		logger.debug("Entering to com.datasync.util.buildResponseConnectorTypeDetail()");
		List<ConnectorTypeResponse> connectorTypeResponseList = new ArrayList<ConnectorTypeResponse>();
		if (!connectorTypeList.isEmpty()) {
			connectorTypeList.forEach(connectorTypeEntity -> {
				ConnectorTypeResponse connectorTypeResponse = new ConnectorTypeResponse();
				connectorTypeResponse.setId(connectorTypeEntity.getId());
				connectorTypeResponse.setConnectorId(connectorTypeEntity.getConnectorId());
				connectorTypeResponse.setConnectorType(connectorTypeEntity.getConnectorType());
				connectorTypeResponse.setImage(connectorTypeEntity.getImage());
				connectorTypeResponseList.add(connectorTypeResponse);
			});
		}
		logger.debug("Exiting to com.datasync.util.buildResponseConnectorTypeDetail()");
		return connectorTypeResponseList.toArray(new ConnectorTypeResponse[connectorTypeResponseList.size()]);
	}

}
