package com.datasync.mail;

/**
 * @author Shubham
 *
 */

import java.util.Collection;
import lombok.Data;

@Data
public class EmailRequestTemplate {
	private String from;
	private Collection<String> to;
	private Collection<String> cc;
	private Collection<String> bcc;
	private String subject;
	private String body;
}
