package com.datasync.mail;

import java.util.List;

/**
 * @author Shubham
 *
 */

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.datasync.entity.BulkJobDetailsEntity;
import com.datasync.response.BulkJobStatusResponse;
import com.datasync.util.CommonUtil;

@Service
public class EmailTemplateService {
	private TemplateEngine templateEngine;

	@Autowired
	public EmailTemplateService(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}

	/**
	 * Renders passed template in resource folder with context map
	 * 
	 * @param templateName
	 * @param contextMap
	 *            -key value pair of variable on template and value
	 * @return
	 */
	public String renderBodyTemplate(String templateName, Map<String, String> contextMap) {
		StringBuilder actualTemplate = new StringBuilder(CommonUtil.EMPTY_STRING);
		actualTemplate.append("email/body/");
		actualTemplate.append(templateName);
		actualTemplate.append("Body");
		return templateEngine.process(actualTemplate.toString(), buildContext(contextMap));
	}

	private Context buildContext(Map<String, String> contextMap) {
		Context context = new Context();
		Set<Entry<String, String>> contextEntrySet = contextMap.entrySet();
		contextEntrySet.forEach(entry -> {
			context.setVariable(entry.getKey(), entry.getValue());
		});
		return context;
	}

	public String renderBodyTemplateMultipleValues(String templateName,
			Map<String, List<BulkJobDetailsEntity>> contextMap) {
		StringBuilder actualTemplate = new StringBuilder(CommonUtil.EMPTY_STRING);
		actualTemplate.append("email/body/");
		actualTemplate.append(templateName);
		actualTemplate.append("Body");
		return templateEngine.process(actualTemplate.toString(), buildContextMultipleValues(contextMap));
	}

	private Context buildContextMultipleValues(Map<String, List<BulkJobDetailsEntity>> contextMap) {
		Context context = new Context();
		Set<Entry<String, List<BulkJobDetailsEntity>>> contextEntrySet = contextMap.entrySet();
		contextEntrySet.forEach(entry -> {
			context.setVariable(entry.getKey(), entry.getValue());
		});
		return context;
	}

	public String renderBodyTemplateMultipleValuesStatus(String templateName,
			Map<String, List<BulkJobStatusResponse>> contextMap) {
		StringBuilder actualTemplate = new StringBuilder(CommonUtil.EMPTY_STRING);
		actualTemplate.append("email/body/");
		actualTemplate.append(templateName);
		actualTemplate.append("Body");
		return templateEngine.process(actualTemplate.toString(), buildContextMultipleValuesStatus(contextMap));
	}

	private Context buildContextMultipleValuesStatus(Map<String, List<BulkJobStatusResponse>> contextMap) {
		Context context = new Context();
		Set<Entry<String, List<BulkJobStatusResponse>>> contextEntrySet = contextMap.entrySet();
		contextEntrySet.forEach(entry -> {
			context.setVariable(entry.getKey(), entry.getValue());
		});
		return context;
	}

}
