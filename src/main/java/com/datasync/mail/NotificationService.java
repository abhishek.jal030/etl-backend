package com.datasync.mail;

/**
 * @author Shubham
 *
 */

import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {
	private static final Logger logger = LogManager.getLogger(NotificationService.class);
	private JavaMailSenderImpl javaMailSender;

	@Autowired
	public NotificationService(JavaMailSenderImpl javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	@Async
	public void sendNotification(EmailRequestTemplate emailRequestTemplate) {
		MimeMessage mail = javaMailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(mail, true);
			helper.setFrom(emailRequestTemplate.getFrom());
			helper.setTo(emailRequestTemplate.getTo().toArray(new String[emailRequestTemplate.getTo().size()]));
			helper.setSubject(emailRequestTemplate.getSubject());
			helper.setText(emailRequestTemplate.getBody(), true);
			if (emailRequestTemplate.getBcc() != null) {
				helper.setBcc(emailRequestTemplate.getBcc().toArray(new String[emailRequestTemplate.getBcc().size()]));
			}
			if (emailRequestTemplate.getCc() != null) {
				helper.setCc(emailRequestTemplate.getCc().toArray(new String[emailRequestTemplate.getCc().size()]));
			}
			// helper.addAttachment("Notes.txt", new ClassPathResource("Notes.txt"));
			javaMailSender.send(mail);
			logger.info("Email Sent!");
		} catch (Exception e) {
			logger.error("error in sending notifications", e);
		}

	}

}
