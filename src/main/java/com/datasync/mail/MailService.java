package com.datasync.mail;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * @author Shubham
 *
 */

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datasync.entity.BulkJobDetailsEntity;
import com.datasync.repository.JobRepository;
import com.datasync.request.MailRequest;
import com.datasync.response.BulkJobStatusResponse;

@Service
public class MailService {

	@Autowired
	private NotificationService notificationService;
	@Autowired
	private EmailTemplateService emailTemplateService;
	@Autowired
	JobRepository jobRepository;

	public void sendMail(MailRequest mailRequest) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		EmailRequestTemplate emailRequestTemplate = new EmailRequestTemplate();
		emailRequestTemplate.setSubject(String.format("Scheduler Status Notification"));
		String Name = "DataSync" + " " + "Team";
		Map<String, String> contextMap = new HashMap<String, String>();
		contextMap.put("Name", "DataSync Team");
		contextMap.put("jobName", mailRequest.getJobName());
		contextMap.put("status", mailRequest.getStatus());
		contextMap.put("totalCount", Long.toString(mailRequest.getTotalRecordCount()));
		contextMap.put("processedCount", Long.toString(mailRequest.getProcessedRecordCount()));
		contextMap.put("jobStartDateTime", dateFormat.format(mailRequest.getStartDateTime()));
		contextMap.put("jobEndDateTime", dateFormat.format(mailRequest.getFinishDateTime()));
		contextMap.put("Name", Name);
		/*
		 * Optional<JobEntity> jobEntityList =
		 * jobRepository.findById(mailRequest.getJobId()); JobEntity jobEntity = new
		 * JobEntity(); if (jobEntityList != null && jobEntityList.isPresent()) {
		 * jobEntity = jobEntityList.get(); }
		 */
		emailRequestTemplate.setBody(emailTemplateService.renderBodyTemplate("sendReviewNotification", contextMap));
		emailRequestTemplate.setTo(Arrays.asList("abhishek.jaiswal@cstinfotech.co.in,ajit.baghel@netapp.com,vishal.verma@cstinfotech.co.in".split(",")));
		emailRequestTemplate.setFrom("noreply@netapp.com");
		notificationService.sendNotification(emailRequestTemplate);
	}

	public void sendMailForBulkJob(List<BulkJobDetailsEntity> bulkJobDetailsEntityList) {
		EmailRequestTemplate emailRequestTemplate = new EmailRequestTemplate();
		emailRequestTemplate.setSubject(String.format("Bulk Job Creation Status Notification"));
		Map<String, List<BulkJobDetailsEntity>> contextMap = new HashMap<String, List<BulkJobDetailsEntity>>();
		contextMap.put("bulkjobdetails", bulkJobDetailsEntityList);
		emailRequestTemplate.setBody(
				emailTemplateService.renderBodyTemplateMultipleValues("sendBulkJobCreationNotification", contextMap));
		emailRequestTemplate.setTo(Arrays.asList("shubham.kumar@cstinfotech.co.in".split(",")));
		emailRequestTemplate.setFrom("shubham.kumar@cstinfotech.co.in");
		notificationService.sendNotification(emailRequestTemplate);
	}

	public void sendMailStatusForBulkJob(List<BulkJobStatusResponse> bulkJobStatusResponseList) {
		EmailRequestTemplate emailRequestTemplate = new EmailRequestTemplate();
		emailRequestTemplate.setSubject(String.format("Bulk Job Creation Status Notification"));
		Map<String, List<BulkJobStatusResponse>> contextMap = new HashMap<String, List<BulkJobStatusResponse>>();
		contextMap.put("bulkJobStatusResponseList", bulkJobStatusResponseList);
		emailRequestTemplate.setBody(emailTemplateService
				.renderBodyTemplateMultipleValuesStatus("sendBulkJobStatusNotification", contextMap));
		emailRequestTemplate.setTo(Arrays.asList("manoj.lohmor@netapp.com,shubham.kumar@cstinfotech.co.in".split(",")));
		emailRequestTemplate.setFrom("shubham.kumar@cstinfotech.co.in");
		notificationService.sendNotification(emailRequestTemplate);
	}
}
