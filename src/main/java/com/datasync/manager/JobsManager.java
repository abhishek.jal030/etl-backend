package com.datasync.manager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.datasync.exception.DataSyncServiceException;
import com.datasync.request.BulkJobRequest;
import com.datasync.request.JobRequest;
import com.datasync.request.ScheduledJobRequest;
import com.datasync.request.TempJobRequest;
import com.datasync.response.BulkJobResponse;
import com.datasync.response.ExcelResponse;
import com.datasync.response.JobDetailsResponse;
import com.datasync.response.JobProgressResponse;
import com.datasync.response.JobResponse;
import com.datasync.response.TempJobResponse;
import com.datasync.response.ViewJobDifferenceResponse;
import com.datasync.response.ViewLogDetailsResponse;
import com.datasync.response.ViewLogResponse;
import com.datasync.response.responsemanager.AbstractResponseManager;
import com.datasync.response.responsemanager.ApiResponse;
import com.datasync.serviceEngine.JobsService;
import com.datasync.util.CommonUtil;
import com.datasync.util.ExcelUtil;
import com.datasync.util.ResponseUtil;

@Service
public class JobsManager extends AbstractResponseManager {

	private static Logger logger = LogManager.getLogger(JobsManager.class);

	@Autowired
	JobsService jobsService;

	@Autowired
	CommonUtil CommonUtil;

	public ResponseEntity<ApiResponse> getJob(String jobId) {
		logger.debug("Entering to com.datasync.manager.getJob()");
		logger.info("Get Job based on jobId");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Job Id :::: " + jobId);
			JobResponse jobResponse;
			jobResponse = jobsService.getJob(jobId);
			if (jobResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(jobResponse);
				logger.debug("Exiting to com.datasync.manager.getJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Job Not Found");
				logger.debug("Exiting to com.datasync.manager.getJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.getJob() ::::" + ex.getMessage());
			ex.printStackTrace();
			logger.debug("Exiting to com.datasync.manager.getJob()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> getAllJobs(JobRequest jobRequest, String sortableColumn, String sortingType,
			Integer startRecord, Integer recordCount) {
		logger.debug("Entering to com.datasync.manager.getAllJobs()");
		logger.info("Getting All Maps");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Job response body :::: " + CommonUtil.convertToJson(jobRequest));
			JobDetailsResponse jobDetailsResponse;
			jobDetailsResponse = jobsService.getAllJobs(jobRequest, sortableColumn, sortingType, startRecord,
					recordCount);
			apiResponse = ResponseUtil.buildSuccessResponse(jobDetailsResponse);
			logger.debug("Exiting to com.datasync.manager.getAllJobs()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);

		} catch (Exception e) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(e.getMessage());
			logger.error("Error in com.datasync.manager.getAllJobs() ::::" + e.getMessage());
			e.printStackTrace();
			logger.debug("Exiting to com.datasync.manager.getAllJobs()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);

		}
	}

	public ResponseEntity<ApiResponse> saveOrUpdateJob(JobRequest jobRequest) {
		logger.debug("Entering to com.datasync.manager.saveOrUpdateMap()");
		logger.info("Saving or updating Maps");
		ApiResponse apiResponse = null;
		JobResponse jobResponse;
		try {
			logger.debug("Job response body :::: " + CommonUtil.convertToJson(jobRequest));
			jobResponse = jobsService.saveOrUpdateJob(jobRequest);
			if (jobResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(jobResponse);
				apiResponse.setMessage("The Job has been created or updated");
				logger.debug("Exiting to com.datasync.manager.saveOrUpdateJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("The Job has not been created or updated");
				logger.debug("Exiting to com.datasync.manager.saveOrUpdateJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.saveOrUpdateJob() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.saveOrUpdateJob()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> deleteJobs(List<String> jobIds) {
		logger.debug("Entering to com.datasync.manager.deleteJobs()");
		logger.info("Enter in Delete Map");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Job Id :::: " + jobIds);
			boolean deleted;
			deleted = jobsService.deleteJobs(jobIds);
			if (deleted) {
				apiResponse = ResponseUtil.buildSuccessResponse();
				apiResponse.setMessage("Job Deleted Successfully");
				logger.debug("Exiting to com.datasync.manager.deleteJobs()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Job Not Found");
				logger.debug("Exiting to com.datasync.manager.deleteJobs()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (DataSyncServiceException ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.deleteJobs() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.deleteJobs()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> runJob(String jobId) {
		logger.debug("Entering to com.datasync.manager.runJob()");
		logger.info("Enter in Run Job");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Job Id :::: " + jobId);
			boolean status = jobsService.runJob(jobId);
			if (status) {
				apiResponse = ResponseUtil.buildSuccessResponse();
				apiResponse.setMessage("Job Run Successfully");
				logger.debug("Exiting to com.datasync.manager.runJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);

			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Job Not Found");
				logger.debug("Exiting to com.datasync.manager.runJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (DataSyncServiceException ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.runJob() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.runJob()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> runJobUpload(String jobId, MultipartFile file) {
		logger.debug("Entering to com.datasync.manager.runJob()");
		logger.info("Enter in Run Job");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Job Id :::: " + jobId);
			boolean status = jobsService.runJobUpload(jobId, file);
			if (status) {
				apiResponse = ResponseUtil.buildSuccessResponse();
				apiResponse.setMessage("Job Run Successfully");
				logger.debug("Exiting to com.datasync.manager.runJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);

			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Job Not Found");
				logger.debug("Exiting to com.datasync.manager.runJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (DataSyncServiceException ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.runJob() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.runJob()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<InputStreamResource> runJobExport(String jobId) {
		logger.debug("Entering to com.datasync.manager.runJob()");
		logger.info("Enter in Run Job");
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS");
		Date date = new Date();
		String fileName = "ForecastTerritoryDetails_" + dateFormat.format(date);
		fileName = fileName.replaceAll(" ", "_");
		fileName = fileName.replaceAll(":", "_");
		HttpHeaders headers = new HttpHeaders();
		try {
			logger.debug("Job Id :::: " + jobId);
			Map<String, Object> jsonResponse = jobsService.runJobExport(jobId);
			if (jsonResponse == null) {
				logger.debug("Exiting to com.datasync.manager.runJob()");
				return ResponseEntity.noContent().headers(headers).build();
			} else {
				ExcelResponse excelResponse = ExcelUtil.jsonExcelExport(jsonResponse);
				headers.add("Content-Disposition", "attachment; filename=" + fileName + ".xlsx");
				return ResponseEntity.ok().headers(headers)
						.body(new InputStreamResource(excelResponse.getByteInputStream()));
			}
		} catch (Exception ex) {
			logger.error("Error in com.datasync.manager.runJob() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.runJob()");
			return ResponseEntity.noContent().headers(headers).build();
		}
	}

	public ResponseEntity<ApiResponse> jobProgress(String jobId) {
		logger.debug("Entering to com.datasync.manager.jobProgress()");
		ApiResponse apiResponse = null;
		JobProgressResponse jobProgressResponse;
		try {
			logger.debug("Job Id :::: " + jobId);
			jobProgressResponse = jobsService.jobProgress(jobId);
			if (jobProgressResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(jobProgressResponse);
				logger.debug("Exiting to com.datasync.manager.jobProgress()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Job Not Found");
				logger.debug("Exiting to com.datasync.manager.jobProgress()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (DataSyncServiceException ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.jobProgress() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.jobProgress()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> jobInProgress() {
		logger.debug("Entering to com.datasync.manager.jobInProgress()");
		ApiResponse apiResponse = null;
		try {
			JobProgressResponse[] jobProgressResponse;
			jobProgressResponse = jobsService.jobInProgress();
			apiResponse = ResponseUtil.buildSuccessResponse(jobProgressResponse);
			logger.debug("Exiting to com.datasync.manager.jobInProgress()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		} catch (DataSyncServiceException ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.jobInProgress() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.jobInProgress()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> connectionJobDetails(String connectionId) {
		logger.debug("Entering to com.datasync.manager.connectionJobDetails()");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Connection Id :::: " + connectionId);
			JobDetailsResponse jobDetailsResponse;
			jobDetailsResponse = jobsService.connectionJobDetails(connectionId);
			if (jobDetailsResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(jobDetailsResponse);
				logger.debug("Exiting to com.datasync.manager.connectionJobDetails()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Connection Not Found");
				logger.debug("Exiting to com.datasync.manager.connectionJobDetails()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (DataSyncServiceException ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.connectionJobDetails() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.connectionJobDetails()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> saveOrUpdatescheduledJob(ScheduledJobRequest scheduledJobRequest) {
		logger.debug("Entering to com.datasync.manager.saveOrUpdatescheduledJob()");
		logger.info("Saving or updating Maps");
		ApiResponse apiResponse = null;
		boolean success = false;
		try {
			logger.debug("Scheduled Job response body :::: " + CommonUtil.convertToJson(scheduledJobRequest));
			success = jobsService.saveOrUpdatescheduledJob(scheduledJobRequest);
			if (success) {
				apiResponse = ResponseUtil.buildSuccessResponse();
				apiResponse.setMessage("The Job has been scheduled successfully");
				logger.debug("Exiting to com.datasync.manager.saveOrUpdatescheduledJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("The Job has been scheduled Failed");
				logger.debug("Exiting to com.datasync.manager.saveOrUpdatescheduledJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.saveOrUpdatescheduledJob() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.saveOrUpdatescheduledJob()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> deleteScheduledJobs(String jobId) {
		logger.debug("Entering to com.datasync.manager.deleteJobs()");
		logger.info("Enter in Delete Map");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Job Id :::: " + jobId);
			boolean deleted;
			deleted = jobsService.deleteScheduledJobs(jobId);
			if (deleted) {
				apiResponse = ResponseUtil.buildSuccessResponse();
				apiResponse.setMessage("Job Deleted Successfully");
				logger.debug("Exiting to com.datasync.manager.deleteScheduledJobs()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);

			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Job Not Found");
				logger.debug("Exiting to com.datasync.manager.deleteScheduledJobs()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.deleteScheduledJobs() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.deleteScheduledJobs()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> viewLogs(String jobId) {
		logger.debug("Entering to com.datasync.manager.viewLogs()");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Job Id :::: " + jobId);
			ViewLogResponse[] viewLogResponse;
			viewLogResponse = jobsService.viewLogs(jobId);
			if (viewLogResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(viewLogResponse);
				logger.debug("Exiting to com.datasync.manager.viewLogs()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				logger.debug("Exiting to com.datasync.manager.viewLogs()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.viewLogs() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.viewLogs()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> viewLogDetails(String jobId, String jobStatusId) {
		logger.debug("Entering to com.datasync.manager.viewLogDetails()");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Job Id :::: " + jobId);
			logger.debug("Job Status Id :::: " + jobStatusId);
			ViewLogDetailsResponse[] viewLogDetailsResponse;
			viewLogDetailsResponse = jobsService.viewLogDetails(jobId, jobStatusId);
			if (viewLogDetailsResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(viewLogDetailsResponse);
				logger.debug("Exiting to com.datasync.manager.viewLogDetails()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				logger.debug("Exiting to com.datasync.manager.viewLogDetails()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.viewLogDetails() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.viewLogDetails()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> viewJobsDifference(JobRequest jobRequest) {
		logger.debug("Entering to com.datasync.manager.viewJobsDifference()");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Job response body :::: " + CommonUtil.convertToJson(jobRequest));
			ViewJobDifferenceResponse[] viewJobDifferenceResponse;
			viewJobDifferenceResponse = jobsService.viewJobsDifference(jobRequest);
			if (viewJobDifferenceResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(viewJobDifferenceResponse);
				logger.debug("Exiting to com.datasync.manager.viewJobsDifference()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				logger.debug("Exiting to com.datasync.manager.viewJobsDifference()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.viewJobsDifference() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.viewJobsDifference()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> stopJob(String jobId) {
		logger.debug("Entering to com.datasync.manager.stopJob()");
		logger.info("Getting All Maps");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Job Id :::: " + jobId);
			JobResponse jobResponse;
			jobResponse = jobsService.stopJob(jobId);
			apiResponse = ResponseUtil.buildSuccessResponse(jobResponse);
			logger.debug("Exiting to com.datasync.manager.getAllJobs()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);

		} catch (Exception e) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(e.getMessage());
			logger.error("Error in com.datasync.manager.stopJob() ::::" + e.getMessage());
			logger.debug("Exiting to com.datasync.manager.stopJob()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);

		}
	}

	public ResponseEntity<ApiResponse> saveJobsDifference(TempJobRequest tempJobRequest) {
		logger.debug("Entering to com.datasync.manager.saveJobsDifference()");
		logger.info("Saving or updating Temp Map");
		ApiResponse apiResponse = null;
		TempJobResponse tempJobResponse = new TempJobResponse();
		try {
			logger.debug("Temp Job response body :::: " + CommonUtil.convertToJson(tempJobRequest));
			tempJobResponse = jobsService.saveJobsDifference(tempJobRequest);
			if (tempJobResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse();
				apiResponse.setMessage("Temp Job has been saved successfully");
				logger.debug("Exiting to com.datasync.manager.saveOrUpdateMap()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Temp Job saved Failed");
				logger.debug("Exiting to com.datasync.manager.saveOrUpdateMap()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.saveJobsDifference() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.saveJobsDifference()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> saveOrUpdateBulkJob(BulkJobRequest bulkJobRequest) {
		logger.debug("Entering to com.datasync.manager.saveOrUpdateBulkJob()");
		logger.info("Saving or updating Bulk jobs");
		ApiResponse apiResponse = null;
		BulkJobResponse bulkJobResponse;
		try {
			logger.debug("Bulk Job response body :::: " + CommonUtil.convertToJson(bulkJobRequest));
			bulkJobResponse = jobsService.saveOrUpdateBulkJob(bulkJobRequest);
			if (bulkJobResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(bulkJobResponse);
				apiResponse.setMessage("The Bulk Job has been created or updated");
				logger.debug("Exiting to com.datasync.manager.saveOrUpdateBulkJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("The Bulk Job has not been created or updated");
				logger.debug("Exiting to com.datasync.manager.saveOrUpdateBulkJob()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.saveOrUpdateBulkJob() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.saveOrUpdateBulkJob()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

}
