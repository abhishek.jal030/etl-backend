package com.datasync.manager;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.http.HttpStatus;

import com.datasync.exception.DataSyncServiceException;
import com.datasync.request.ConnectionDataRequest;
import com.datasync.response.ConnectionDetailsResponse;
import com.datasync.response.ConnectionResponse;
import com.datasync.response.responsemanager.AbstractResponseManager;
import com.datasync.response.responsemanager.ApiResponse;
import com.datasync.serviceEngine.ConnectionService;
import com.datasync.util.CommonUtil;
import com.datasync.util.ResponseUtil;

@Service
public class ConnectionManager extends AbstractResponseManager {
	@Autowired
	ConnectionService connectionService;

	private static Logger logger = LogManager.getLogger(ConnectionManager.class);

	public ResponseEntity<ApiResponse> getConnection(String connectionId) {
		logger.debug("Entering to com.datasync.manager.getConnection()");
		logger.info("Get Connection based on getConnection");
		ApiResponse apiResponse = null;
		ConnectionResponse connectionConfigResponse;
		try {
			logger.debug("Connection Id :::: " + connectionId);
			connectionConfigResponse = connectionService.getConnection(connectionId);
			if (connectionConfigResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(connectionConfigResponse);
				apiResponse.setMessage("Connection Founded");
				logger.debug("Exiting to com.datasync.manager.getConnection()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Connection Not Found");
				logger.debug("Exiting to com.datasync.manager.getConnection()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.netapp.manager.getConnection() ::::" + ex.getMessage());
			ex.printStackTrace();
			logger.debug("Exiting to com.datasync.manager.getConnection()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> getAllConnections(ConnectionDataRequest connectionDataRequest,
			Integer startRecord, Integer recordCount, String sortableColumn, String sortingType) {
		logger.debug("Entering to com.datasync.manager.getAllConnections()");
		logger.info("Geting All Connection");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Connection response body :::: " + CommonUtil.convertToJson(connectionDataRequest));
			ConnectionDetailsResponse connectionDetailsResponse;
			connectionDetailsResponse = connectionService.getAllConnections(connectionDataRequest, startRecord,
					recordCount, sortableColumn, sortingType);
			if (connectionDetailsResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(connectionDetailsResponse);
				logger.debug("Exiting to com.datasync.manager.getAllConnections()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Connection Not Found");
				logger.debug("Exiting to com.datasync.manager.getAllConnections()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.getAllConnections() ::::" + ex.getMessage());
			ex.printStackTrace();
			logger.debug("Exiting to com.datasync.manager.getAllConnections()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> saveOrUpdateConnection(ConnectionDataRequest connectionDataRequest) {
		logger.debug("Entering to com.datasync.manager.saveOrUpdateConnection()");
		logger.info("Saving or updating Connection");
		ApiResponse apiResponse = null;
		ConnectionResponse connectionResponse;
		try {
			logger.debug("Connection response body :::: " + CommonUtil.convertToJson(connectionDataRequest));
			connectionResponse = connectionService.saveOrUpdateConnection(connectionDataRequest);
			if (connectionResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(connectionResponse);
				apiResponse.setMessage("The Connection has been created or updated");
				logger.debug("Exiting to com.datasync.manager.saveOrUpdateConnection()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("The Connection has not been created or updated");
				logger.debug("Exiting to com.datasync.manager.saveOrUpdateConnection()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.saveOrUpdateConnection() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.saveOrUpdateConnection()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> deleteConnections(List<String> connectionIds) {
		logger.debug("Entering to com.datasync.manager.deleteConnections()");
		logger.info("Delete Connection");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Connection Id :::: " + connectionIds);
			boolean deleted;
			deleted = connectionService.deleteConnections(connectionIds);
			if (deleted) {
				apiResponse = ResponseUtil.buildSuccessResponse();
				apiResponse.setMessage("Connections Deleted Successfully");
				logger.debug("Exiting to com.datasync.manager.deleteConnections()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);

			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Connections Not Found");
				logger.debug("Exiting to com.datasync.manager.deleteConnections()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}

		} catch (DataSyncServiceException ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.deleteConnections() ::::" + ex.getMessage());
			logger.debug("Exiting to com.datasync.manager.deleteConnections()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

}
