package com.datasync.manager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.datasync.request.ConnectionTestRequest;
import com.datasync.request.ConnectorTypeRequest;
import com.datasync.request.TableMetaDataRequest;
import com.datasync.request.TestFilterRequest;
import com.datasync.response.ConnectionTestResponse;
import com.datasync.response.ConnectorTypeResponse;
import com.datasync.response.DataPreviewResponse;
import com.datasync.response.MetadataResponse;
import com.datasync.response.MetadataResponseDetails;
import com.datasync.response.TestFilterQueryResponse;
import com.datasync.response.responsemanager.AbstractResponseManager;
import com.datasync.response.responsemanager.ApiResponse;
import com.datasync.serviceEngine.ConnectorFactory;
import com.datasync.util.CommonUtil;
import com.datasync.util.ResponseUtil;

@Service
public class ConnectorManager extends AbstractResponseManager {

	private static Logger logger = LogManager.getLogger(ConnectorManager.class);

	@Autowired
	ConnectorFactory connectorFactory;

	@Autowired
	CommonUtil CommonUtil;
	
	public ResponseEntity<ApiResponse> testConnection(ConnectionTestRequest connectionTestRequest) {
		logger.debug("Entering to com.datasync.manager.testConnection()");
		logger.info("Getting Metadata Of Entity");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Connection response body :::: " + CommonUtil.convertToJson(connectionTestRequest));
			ConnectionTestResponse connectionTestResponse;
			connectionTestResponse = connectorFactory.testConnection(connectionTestRequest);
			apiResponse = ResponseUtil.buildSuccessResponse(connectionTestResponse);
			logger.debug("Exiting to com.datasync.manager.testConnection()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);

		} catch (Exception e) {
			logger.error("Error in com.datasync.manager.testConnection() ::::" + e.getMessage());
			logger.debug("Exiting to com.datasync.manager.testConnection()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> getAllEntitiesName(String connectionId) {
		logger.debug("Entering to com.datasync.manager.getAllEntitiesName()");
		logger.info("Getting All Entity Name");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Connection Id :::: " + connectionId);
			MetadataResponseDetails metadataResponseDetails;
			metadataResponseDetails = connectorFactory.getAllEntitiesName(connectionId);
			if (metadataResponseDetails != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(metadataResponseDetails);
				logger.debug("Exiting to com.datasync.manager.getAllEntitiesName()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Entity Not Found");
				logger.debug("Exiting to com.datasync.manager.getAllEntitiesName()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.getAllEntitiesName() ::::" + ex.getMessage());
			ex.printStackTrace();
			logger.debug("Exiting to com.datasync.manager.getAllEntitiesName()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> getMetadata(String connectionId, String collectionName, String name) {
		logger.debug("Entering to com.datasync.manager.getMetadata()");
		logger.info("Getting Metadata Of Entity");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Connection Id :::: " + connectionId);
			logger.debug("Connection Name :::: " + collectionName);
			logger.debug("Type :::: " + name);
			MetadataResponse metadataResponse;
			metadataResponse = connectorFactory.getMetadata(connectionId, collectionName, name);
			apiResponse = ResponseUtil.buildSuccessResponse(metadataResponse);
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.getMetadata() ::::" + ex.getMessage());
			ex.printStackTrace();
			logger.debug("Exiting to com.datasync.manager.getMetadata()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
		logger.debug("Exiting to com.datasync.manager.getMetadata()");
		return buildEntityResponse(HttpStatus.OK, apiResponse);
	}

	public ResponseEntity<ApiResponse> getDataForPreview(String connectionId, String collectionName,
			String collectionType, String previewFilter, Integer startRecord, Integer recordCount) {
		logger.debug("Entering to com.datasync.manager.getDataForPreview()");
		logger.info("Getting Data For Preview");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Connection Id :::: " + connectionId);
			logger.debug("Connection Name :::: " + collectionName);
			logger.debug("Connection Type :::: " + collectionType);
			logger.debug("preview Filter :::: " + previewFilter);
			DataPreviewResponse dataPreviewResponse;
			dataPreviewResponse = connectorFactory.getDataForPreview(connectionId, collectionName, collectionType,
					previewFilter, startRecord, recordCount);
			if (dataPreviewResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(dataPreviewResponse);
				logger.debug("Exiting to com.datasync.manager.getDataForPreview()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Data Not Found");
				logger.debug("Exiting to com.datasync.manager.getDataForPreview()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.getDataForPreview() ::::" + ex.getMessage());
			ex.printStackTrace();
			logger.debug("Exiting to com.datasync.manager.getDataForPreview()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> refreshMetadata(String connectionId) {
		logger.debug("Entering to com.datasync.manager.refreshMetadata()");
		logger.info("Getting Metadata Of Entity");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Connection Id :::: " + connectionId);
			boolean status = connectorFactory.refreshMetadata(connectionId);
			if (status) {
				apiResponse = ResponseUtil.buildSuccessResponse();
				apiResponse.setMessage("Metadata Refreshed");
				logger.debug("Exiting to com.datasync.manager.refreshMetadata()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Metadata Refresh got failed");
				logger.debug("Exiting to com.datasync.manager.refreshMetadata()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.refreshMetadata() ::::" + ex.getMessage());
			ex.printStackTrace();
			logger.debug("Exiting to com.datasync.manager.refreshMetadata()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> refreshCollectionCounts(String connectionId) {
		logger.debug("Entering to com.datasync.manager.refreshCollectionCounts()");
		logger.info("Getting Collection COunt Of Entity");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Connection Id :::: " + connectionId);
			connectorFactory.refreshCollectionCounts(connectionId);
			apiResponse = ResponseUtil.buildSuccessResponse();
			apiResponse.setMessage("Collections Count Refreshed");
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.refreshCollectionCounts() ::::" + ex.getMessage());
			ex.printStackTrace();
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
		logger.debug("Exiting to com.datasync.manager.refreshCollectionCounts()");
		return buildEntityResponse(HttpStatus.OK, apiResponse);
	}

	public ResponseEntity<ApiResponse> getTableMetadata(String connectionId, String collectionName) {
		logger.debug("Entering to com.datasync.manager.getTableMetadata()");
		logger.info("Getting Metadata Of Entity");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Connection Id :::: " + connectionId);
			logger.debug("Connection Name :::: " + collectionName);
			MetadataResponse metadataResponse;
			metadataResponse = connectorFactory.getTableMetadata(connectionId, collectionName);
			if (metadataResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(metadataResponse);
				logger.debug("Exiting to com.datasync.manager.getTableMetadata()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				logger.debug("Exiting to com.datasync.manager.getTableMetadata()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.getTableMetadata() ::::" + ex.getMessage());
			ex.printStackTrace();
			logger.debug("Exiting to com.datasync.manager.getTableMetadata()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> getUpdatedTableMetadata(String jobId) {
		logger.debug("Entering to com.datasync.manager.getUpdatedTableMetadata()");
		logger.info("Getting Updated Metadata Of Entity");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Job Id :::: " + jobId);
			MetadataResponse metadataResponse;
			metadataResponse = connectorFactory.getUpdatedTableMetadata(jobId);
			if (metadataResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(metadataResponse);
				logger.debug("Exiting to com.datasync.manager.getUpdatedTableMetadata()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				logger.debug("Exiting to com.datasync.manager.getUpdatedTableMetadata()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception ex) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(ex.getMessage());
			logger.error("Error in com.datasync.manager.getUpdatedTableMetadata() ::::" + ex.getMessage());
			ex.printStackTrace();
			logger.debug("Exiting to com.datasync.manager.getUpdatedTableMetadata()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> createTable(String connectionId, TableMetaDataRequest tableMetaDataRequest) {
		logger.debug("Entering to com.datasync.manager.createTable()");
		logger.info("Getting Metadata Of Entity");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Connection Id :::: " + connectionId);
			logger.debug("Table Metadata Request body :::: " + CommonUtil.convertToJson(tableMetaDataRequest));
			boolean status = connectorFactory.createTable(connectionId, tableMetaDataRequest);
			if (status) {
				apiResponse = ResponseUtil.buildSuccessResponse();
				apiResponse.setMessage("Table created Successfully");
				logger.debug("Exiting to com.datasync.manager.createTable()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);

			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Table created Failed");
				logger.debug("Exiting to com.datasync.manager.createTable()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception e) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(e.getMessage());
			logger.error("Error in com.datasync.manager.getUpdatedTableMetadata() ::::" + e.getMessage());
			logger.debug("Exiting to com.datasync.manager.createTable()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> alterTable(String connectionId, TableMetaDataRequest tableMetaDataRequest) {
		logger.debug("Entering to com.datasync.manager.alterTable()");
		logger.info("Getting Metadata Of Entity");
		ApiResponse apiResponse = null;

		try {
			logger.debug("Connection Id :::: " + connectionId);
			logger.debug("Table Metadata Request body :::: " + CommonUtil.convertToJson(tableMetaDataRequest));
			boolean status = connectorFactory.alterTable(connectionId, tableMetaDataRequest);
			if (status) {
				apiResponse = ResponseUtil.buildSuccessResponse();
				apiResponse.setMessage("Table altered Successfully");
				logger.debug("Exiting to com.datasync.manager.alterTable()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);

			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Table altered Failed");
				logger.debug("Exiting to com.datasync.manager.alterTable()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception e) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(e.getMessage());
			logger.error("Error in com.datasync.manager.alterTable() ::::" + e.getMessage());
			logger.debug("Exiting to com.datasync.manager.alterTable()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}
	}

	public ResponseEntity<ApiResponse> getAllConnectorType() {
		logger.debug("Entering to com.datasync.manager.getAllConnectorType()");
		logger.info("Getting All Connector Type Name");
		ApiResponse apiResponse = null;
		try {
			ConnectorTypeResponse[] connectorTypeResponse;
			connectorTypeResponse = connectorFactory.getAllConnectorType();
			if (connectorTypeResponse != null) {
				apiResponse = ResponseUtil.buildSuccessResponse(connectorTypeResponse);
				apiResponse.setMessage("Connector Name Found");
				logger.debug("Exiting to com.datasync.manager.getAllConnectorType()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Connector Name Not Found");
				logger.debug("Exiting to com.datasync.manager.getAllConnectorType()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception e) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(e.getMessage());
			logger.error("Error in com.datasync.manager.getAllConnectorType() ::::" + e.getMessage());
			logger.debug("Exiting to com.datasync.manager.getAllConnectorType()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	public ResponseEntity<ApiResponse> createConnectorType(ConnectorTypeRequest connectorTypeRequest) {
		logger.debug("Entering to com.datasync.manager.createConnectorType()");
		logger.info("Getting All Entity Name");
		ApiResponse apiResponse = null;
		try {
			logger.debug("Table Metadata Request body :::: " + CommonUtil.convertToJson(connectorTypeRequest));
			boolean status;
			status = connectorFactory.createConnectorType(connectorTypeRequest);
			if (status) {
				apiResponse = ResponseUtil.buildSuccessResponse();
				apiResponse.setMessage("Connector Added Successfully");
				logger.debug("Exiting to com.datasync.manager.createConnectorType()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			} else {
				apiResponse = ResponseUtil.buildErrorResponse();
				apiResponse.setMessage("Connector Not Added");
				logger.debug("Exiting to com.datasync.manager.createConnectorType()");
				return buildEntityResponse(HttpStatus.OK, apiResponse);
			}
		} catch (Exception e) {
			apiResponse = ResponseUtil.buildErrorResponse();
			apiResponse.setMessage(e.getMessage());
			logger.error("Error in com.datasync.manager.createConnectorType() ::::" + e.getMessage());
			logger.debug("Exiting to com.datasync.manager.createConnectorType()");
			return buildEntityResponse(HttpStatus.OK, apiResponse);
		}

	}

	/*
	 * public ResponseEntity<ApiResponse> testFilterQuery(TestFilterRequest
	 * testFilterRequest) {
	 * logger.debug("Entering to com.datasync.manager.testFiletQuery()");
	 * ApiResponse apiResponse = null; try { TestFilterQueryResponse
	 * testFilterQueryResponse = new TestFilterQueryResponse();
	 * testFilterQueryResponse =
	 * connectorFactory.testFilterQuery(testFilterRequest); apiResponse =
	 * ResponseUtil.buildSuccessResponse(testFilterQueryResponse);
	 * apiResponse.setMessage("Query Test Successfull");
	 * logger.debug("Exiting to com.datasync.manager.createTable()"); return
	 * buildEntityResponse(HttpStatus.OK, apiResponse); } catch (Exception e) {
	 * JSONObject jo = new JSONObject(e.getMessage()); apiResponse =
	 * ResponseUtil.buildErrorResponse();
	 * apiResponse.setMessage(jo.getJSONObject("error").getJSONObject("message").get
	 * ("value").toString()); logger.debug(e.getMessage()); return
	 * buildEntityResponse(HttpStatus.OK, apiResponse); } }
	 */

}
