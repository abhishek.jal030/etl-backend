package com.datasync.response;

import java.util.List;

import com.datasync.response.responsemanager.ApiResult;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class MetadataResponse extends ApiResult {

	private String collectionName;
	private String apiUrl;
	private List<CollectionMetadataResponse> collectionMetadataResponseList;
	private List<InResponse> inResponseList;
	private Long collectionCount;
	private List<String> navigableProperties;
}
