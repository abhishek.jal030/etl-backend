package com.datasync.response;

import java.util.HashMap;
import java.util.List;

import com.datasync.response.responsemanager.ApiResult;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ApiMetaDataResponse extends ApiResult {

	private String apiName;
	private String apiUrl;
	private String apiAction;
	private String requestBodyType;
	List<CollectionMetadataResponse> collectionMetadataResponse;
}