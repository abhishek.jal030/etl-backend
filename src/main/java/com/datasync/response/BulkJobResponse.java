package com.datasync.response;

import com.datasync.response.responsemanager.ApiResult;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BulkJobResponse extends ApiResult {

	private String name;
	private String id;
	private String sourceConnectionName;
	private String destinationConnectionName;
	private String collectionNames;
	private String prefix;
	private String createdDate;
	private String updatedDate;
	private String createdBy;
	private String updatedBy;

}
