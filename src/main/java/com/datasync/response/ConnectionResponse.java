package com.datasync.response;

import java.util.HashMap;
import java.util.Map;

import com.datasync.response.responsemanager.ApiResult;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ConnectionResponse extends ApiResult {

	private String connectionId;
	private String type;
	private String name;
	private String aliasName;
	private Map<String, String> connectionConfigRequestList = new HashMap<>();
	private String createdDate;
	private String updatedDate;
	private String createdBy;
	private String updatedBy;

}
