package com.datasync.response;

import com.datasync.response.responsemanager.ApiResult;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BulkJobStatusResponse extends ApiResult {

	private String collectionName;
	private String creationStatus;
	private String message;
	private String jobName;
	private String runningStatus;
	private String totalRecord;
	private String recordProcessed;
	private String startDate;
	private String endDate;

}
