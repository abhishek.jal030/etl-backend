package com.datasync.response;

import java.util.List;

import com.datasync.response.MetadataResponse.MetadataResponseBuilder;
import com.datasync.response.responsemanager.ApiResponse;
import com.datasync.response.responsemanager.ApiResult;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class MetadataResponseDetails extends ApiResult {

	List<MetadataResponse> metadataTableResponse;
	List<MetadataResponse> metadataProcedureResponse;

}
