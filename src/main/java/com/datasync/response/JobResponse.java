package com.datasync.response;

import com.datasync.response.responsemanager.ApiResult;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class JobResponse extends ApiResult {

	private String JobName;
	private String jobId;
	private String sourceConnectionName;
	private String destinationConnectionName;
	private String sourceCollection;
	private String destinationCollection;
	private String mapping;
	private String jobFilter;
	private String incrementalParameter;
	private String incrementalParameterValue;
	private String status;
	private String lastRun;
	private String createdDate;
	private String updatedDate;
	private String createdBy;
	private String updatedBy;

}
