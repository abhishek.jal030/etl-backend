package com.datasync.response.responsemanager;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public @Data class ApiError {

	private String code;
	private String message;
	private Map<String, String> errorMessages;
	private String field;
	private Object rejectedValue;

	public ApiError(String code, String message) {
		this.code = code;
		this.message = message;
	}

	
	
	public ApiError(String code, String message, Map<String, String> errorMessages) {
		this.code = code;
		this.message = message;
		this.errorMessages = errorMessages;
	}
	public ApiError(String code, String message, String field, Object rejectedValue) {
		this.code = code;
		this.message = message;
		this.field = field;
		this.rejectedValue = rejectedValue;
	}
}
